all:
	mkdir -p bin
	cd src/compiler/syntactic && make
	javac -classpath lib/antlr-4.2-complete.jar -d bin src/compiler/*/*.java 
clean:
	cd src/compiler/syntactic && make clean
	rm -rf bin
