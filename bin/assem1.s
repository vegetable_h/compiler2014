.text
mycheck:
subu $sp, $sp, 32
sw $ra, 28($sp)
lw $t8, 12($gp)
sw $t8, 0($sp)
lw $t8, 16($gp)
sw $t8, 4($sp)
lw $t8, 0($sp)
lw $t9, 4($sp)
slt  $t8, $t8, $t9
sw $t8, 8($sp)
lw $t8, 8($sp)
beqz  $t8,L1
lw $t9, 0($sp)
sge  $t8,$t9,0
sw $t8, 16($sp)
lw $t9, 16($sp)
move $t8,$t9
sw $t8, 20($sp)
j  L2
L1:
li $t8,0
sw $t8, 20($sp)
L2:
lw $t9, 20($sp)
move $t8,$t9
sw $t8, 8($gp)
lw $ra, 28($sp)
addu $sp, $sp, 32
jr $ra
lw $t9, 24($sp)
move $t8,$t9
sw $t8, 8($gp)
lw $ra, 28($sp)
addu $sp, $sp, 32
jr $ra

malloc: 
li $v0, 9
syscall
jr $ra

printf:
sw $ra, -4($sp)
sw $s3, -8($sp)
sw $s4, -12($sp)
sw $s5, -16($sp)
sw $s6, -20($sp)
sw $t0, -24($sp)
sw $t1, -28($sp)
move $t0, $v0
move $t1, $v1
move $s4, $a0 
la $s6, printf_buf 

printf_loop: 
lb $s5, 0($s4) 
addu $s4, $s4, 1
beq $s5, '%', printf_fmt
beq $0, $s5, printf_end
sb $s5, 0($s6) 
sb $0, 1($s6) 
move $a0, $s6
li $v0, 4 
syscall
b printf_loop 

printf_fmt:
lb $s5, 0($s4)
addu $s4, $s4, 1
beq $s5, 'd', printf_int
beq $s5, 's', printf_str
beq $s5, 'c', printf_char
beq $s5, '0', printf_width

printf_shift_args: 
move $a1, $a2 
move $a2, $a3 
move $a3, $t0 
move $t0, $t1
b printf_loop 

printf_int: 
move $a0, $a1 
li $v0, 1
syscall
b printf_shift_args 

printf_str: 
move $a0, $a1 
li $v0, 4
syscall
b printf_shift_args 

printf_char: 
sb $a1, 0($s6) 
sb $0, 1($s6) 
move $a0, $s6 
li $v0, 4 
syscall
b printf_shift_args 

printf_width:
move $s3, $a1
lb $s5, 0($s4)
addu $s4, $s4, 2
subu $s5, $s5, '1'
divu $a1, $a1, 10

calc_zero_number:
beqz $a1 print_zeros
subu $s5, $s5, 1
divu $a1, $a1, 10
b calc_zero_number

print_zeros:
print_single_zero:
beqz $s5, print_number
subu $s5, $s5, 1
li $a0, 0
li $v0, 1
syscall
b print_single_zero

print_number:
move $a0, $s3
li $v0, 1
syscall
b printf_shift_args

printf_end:
lw $ra, -4($sp)
lw $s3, -8($sp)
lw $s4, -12($sp)
lw $s5, -16($sp)
lw $s6, -20($sp)
lw $t0, -24($sp)
lw $t1, -28($sp)
jr $ra
mymain:
lw $t1, 8($gp)
subu $sp, $sp, 1576
sw $ra, 1572($sp)
li $t6,100
li $t7,0
li $s0,0
li $s1,0
li $s2,0
sub  $s3,$t6,1
move $s4,$s3
move $s5,$s3
li $s6,0
mul  $s7, $t6, $t6
mul  $k0,$s7,4
move $a0, $k0
jal malloc
move $t2, $v0
move $k1,$t2
li $fp,0
L3:
mul  $s3, $t6, $t6
slt  $s7, $fp, $s3
beqz  $s7,L5
mul  $k0,$fp,4
add  $k0, $k0, $k1
li $t8,0
sw $t8, 0($k0)
add  $s3,$fp,1
move $fp,$s3
j  L3
L5:
mul  $s7, $t6, $t6
mul  $k0,$s7,4
move $a0, $k0
jal malloc
move $t3, $v0
move $s3,$t3
li $fp,0
L6:
mul  $s7, $t6, $t6
slt  $k0, $fp, $s7
beqz  $k0,L8
mul  $s7,$fp,4
add  $s7, $s7, $s3
li $t8,0
sw $t8, 0($s7)
add  $k0,$fp,1
move $fp,$k0
j  L6
L8:
mul  $s7,$t6,4
move $a0, $s7
jal malloc
move $t4, $v0
move $k0,$t4
li $fp,0
L9:
slt  $s7, $fp, $t6
beqz  $s7,L14
mul  $s7,$fp,4
add  $s7, $s7, $k0
mul  $t8,$t6,4
sw $t8, 216($sp)
lw $t8, 216($sp)
move $a0, $t8
jal malloc
move $t5, $v0
move $t8,$t5
sw $t8, 0($s7)
li $t8,0
sw $t8, 60($sp)
L10:
lw $t8, 60($sp)
slt  $t8, $t8, $t6
sw $t8, 228($sp)
lw $t8, 228($sp)
beqz  $t8,L12
mul  $t8,$fp,4
sw $t8, 232($sp)
lw $t8, 232($sp)
add  $t8, $t8, $k0
sw $t8, 232($sp)
lw $t9, 60($sp)
mul  $t8,$t9,4
sw $t8, 240($sp)
lw $t8, 240($sp)
lw $t9, 232($sp)
lw $t9, 0($t9)
add  $t8, $t8, $t9
sw $t8, 240($sp)
li $t8,1
sw $t8, 248($sp)
lw $t9, 248($sp)
neg  $t8, $t9
sw $t8, 252($sp)
lw $t9, 252($sp)
move $t8,$t9
lw $t9, 240($sp)
sw $t8, 0($t9)
lw $t9, 60($sp)
add  $t8,$t9,1
sw $t8, 260($sp)
lw $t9, 260($sp)
move $t8,$t9
sw $t8, 60($sp)
j  L10
L12:
add  $t8,$fp,1
sw $t8, 268($sp)
lw $t9, 268($sp)
move $fp,$t9
j  L9
L14:
li $t8,0
sw $t8, 272($sp)
lw $t9, 272($sp)
mul  $t8,$t9,4
sw $t8, 276($sp)
lw $t8, 276($sp)
add  $t8, $t8, $k1
sw $t8, 276($sp)
move $t8,$s0
lw $t9, 276($sp)
sw $t8, 0($t9)
li $t8,0
sw $t8, 284($sp)
lw $t9, 284($sp)
mul  $t8,$t9,4
sw $t8, 288($sp)
lw $t8, 288($sp)
add  $t8, $t8, $s3
sw $t8, 288($sp)
move $t8,$t7
lw $t9, 288($sp)
sw $t8, 0($t9)
L15:
sge  $t8, $s1, $s2
sw $t8, 320($sp)
lw $t8, 320($sp)
beqz  $t8,L67
mul  $t8,$s2,4
sw $t8, 324($sp)
lw $t8, 324($sp)
add  $t8, $t8, $k1
sw $t8, 324($sp)
lw $t9, 324($sp)
lw $t9, 0($t9)
mul  $t8,$t9,4
sw $t8, 332($sp)
lw $t8, 332($sp)
add  $t8, $t8, $k0
sw $t8, 332($sp)
mul  $t8,$s2,4
sw $t8, 340($sp)
lw $t8, 340($sp)
add  $t8, $t8, $s3
sw $t8, 340($sp)
lw $t9, 340($sp)
lw $t9, 0($t9)
mul  $t8,$t9,4
sw $t8, 348($sp)
lw $t8, 348($sp)
lw $t9, 332($sp)
lw $t9, 0($t9)
add  $t8, $t8, $t9
sw $t8, 348($sp)
lw $t9, 348($sp)
lw $t9, 0($t9)
move $t8,$t9
sw $t8, 32($sp)
mul  $t8,$s2,4
sw $t8, 356($sp)
lw $t8, 356($sp)
add  $t8, $t8, $k1
sw $t8, 356($sp)
lw $t9, 356($sp)
lw $t9, 0($t9)
sub  $t8,$t9,1
sw $t8, 368($sp)
lw $t9, 368($sp)
move $t8,$t9
sw $t8, 36($sp)
mul  $t8,$s2,4
sw $t8, 372($sp)
lw $t8, 372($sp)
add  $t8, $t8, $s3
sw $t8, 372($sp)
lw $t9, 372($sp)
lw $t9, 0($t9)
sub  $t8,$t9,2
sw $t8, 384($sp)
lw $t9, 384($sp)
move $t8,$t9
sw $t8, 40($sp)
lw $t8, 36($sp)
sw $t8, 12($gp)
sw $t6, 16($gp)
jal  mycheck
lw $t1, 8($gp)
move $t8,$t1
sw $t8, 388($sp)
li $t8,1
sw $t8, 392($sp)
lw $t8, 388($sp)
lw $t9, 392($sp)
bne  $t8, $t9, L20
lw $t8, 40($sp)
sw $t8, 12($gp)
sw $t6, 16($gp)
jal  mycheck
lw $t1, 8($gp)
move $t8,$t1
sw $t8, 400($sp)
li $t8,1
sw $t8, 404($sp)
lw $t8, 400($sp)
lw $t9, 404($sp)
bne  $t8, $t9, L20
lw $t9, 36($sp)
mul  $t8,$t9,4
sw $t8, 412($sp)
lw $t8, 412($sp)
add  $t8, $t8, $k0
sw $t8, 412($sp)
lw $t9, 40($sp)
mul  $t8,$t9,4
sw $t8, 420($sp)
lw $t8, 420($sp)
lw $t9, 412($sp)
lw $t9, 0($t9)
add  $t8, $t8, $t9
sw $t8, 420($sp)
li $t8,1
sw $t8, 428($sp)
lw $t9, 428($sp)
neg  $t8, $t9
sw $t8, 432($sp)
lw $t8, 420($sp)
lw $t8, 0($t8)
lw $t9, 432($sp)
beq  $t8, $t9, L16
lw $t8, 420($sp)
lw $t8, 0($t8)
lw $t9, 432($sp)
bne  $t8, $t9, L20
L16:
add  $t8,$s1,1
sw $t8, 444($sp)
lw $t9, 444($sp)
move $s1,$t9
mul  $t8,$s1,4
sw $t8, 448($sp)
lw $t8, 448($sp)
add  $t8, $t8, $k1
sw $t8, 448($sp)
lw $t9, 36($sp)
move $t8,$t9
lw $t9, 448($sp)
sw $t8, 0($t9)
mul  $t8,$s1,4
sw $t8, 456($sp)
lw $t8, 456($sp)
add  $t8, $t8, $s3
sw $t8, 456($sp)
lw $t9, 40($sp)
move $t8,$t9
lw $t9, 456($sp)
sw $t8, 0($t9)
lw $t9, 36($sp)
mul  $t8,$t9,4
sw $t8, 464($sp)
lw $t8, 464($sp)
add  $t8, $t8, $k0
sw $t8, 464($sp)
lw $t9, 40($sp)
mul  $t8,$t9,4
sw $t8, 472($sp)
lw $t8, 472($sp)
lw $t9, 464($sp)
lw $t9, 0($t9)
add  $t8, $t8, $t9
sw $t8, 472($sp)
lw $t9, 32($sp)
add  $t8,$t9,1
sw $t8, 484($sp)
lw $t9, 484($sp)
move $t8,$t9
lw $t9, 472($sp)
sw $t8, 0($t9)
lw $t8, 36($sp)
bne  $t8, $s5, L18
lw $t8, 40($sp)
beq  $t8, $s4, L17
lw $t8, 40($sp)
bne  $t8, $s4, L18
L17:
li $s6,1
L18:
L20:
mul  $t8,$s2,4
sw $t8, 500($sp)
lw $t8, 500($sp)
add  $t8, $t8, $k1
sw $t8, 500($sp)
lw $t9, 500($sp)
lw $t9, 0($t9)
sub  $t8,$t9,1
sw $t8, 512($sp)
lw $t9, 512($sp)
move $t8,$t9
sw $t8, 36($sp)
mul  $t8,$s2,4
sw $t8, 516($sp)
lw $t8, 516($sp)
add  $t8, $t8, $s3
sw $t8, 516($sp)
lw $t9, 516($sp)
lw $t9, 0($t9)
add  $t8,$t9,2
sw $t8, 528($sp)
lw $t9, 528($sp)
move $t8,$t9
sw $t8, 40($sp)
lw $t8, 36($sp)
sw $t8, 12($gp)
sw $t6, 16($gp)
jal  mycheck
lw $t1, 8($gp)
move $t8,$t1
sw $t8, 532($sp)
li $t8,1
sw $t8, 536($sp)
lw $t8, 532($sp)
lw $t9, 536($sp)
bne  $t8, $t9, L26
lw $t8, 40($sp)
sw $t8, 12($gp)
sw $t6, 16($gp)
jal  mycheck
lw $t1, 8($gp)
move $t8,$t1
sw $t8, 544($sp)
li $t8,1
sw $t8, 548($sp)
lw $t8, 544($sp)
lw $t9, 548($sp)
bne  $t8, $t9, L26
lw $t9, 36($sp)
mul  $t8,$t9,4
sw $t8, 556($sp)
lw $t8, 556($sp)
add  $t8, $t8, $k0
sw $t8, 556($sp)
lw $t9, 40($sp)
mul  $t8,$t9,4
sw $t8, 564($sp)
lw $t8, 564($sp)
lw $t9, 556($sp)
lw $t9, 0($t9)
add  $t8, $t8, $t9
sw $t8, 564($sp)
li $t8,1
sw $t8, 572($sp)
lw $t9, 572($sp)
neg  $t8, $t9
sw $t8, 576($sp)
lw $t8, 564($sp)
lw $t8, 0($t8)
lw $t9, 576($sp)
beq  $t8, $t9, L22
lw $t8, 564($sp)
lw $t8, 0($t8)
lw $t9, 576($sp)
bne  $t8, $t9, L26
L22:
add  $t8,$s1,1
sw $t8, 588($sp)
lw $t9, 588($sp)
move $s1,$t9
mul  $t8,$s1,4
sw $t8, 592($sp)
lw $t8, 592($sp)
add  $t8, $t8, $k1
sw $t8, 592($sp)
lw $t9, 36($sp)
move $t8,$t9
lw $t9, 592($sp)
sw $t8, 0($t9)
mul  $t8,$s1,4
sw $t8, 600($sp)
lw $t8, 600($sp)
add  $t8, $t8, $s3
sw $t8, 600($sp)
lw $t9, 40($sp)
move $t8,$t9
lw $t9, 600($sp)
sw $t8, 0($t9)
lw $t9, 36($sp)
mul  $t8,$t9,4
sw $t8, 608($sp)
lw $t8, 608($sp)
add  $t8, $t8, $k0
sw $t8, 608($sp)
lw $t9, 40($sp)
mul  $t8,$t9,4
sw $t8, 616($sp)
lw $t8, 616($sp)
lw $t9, 608($sp)
lw $t9, 0($t9)
add  $t8, $t8, $t9
sw $t8, 616($sp)
lw $t9, 32($sp)
add  $t8,$t9,1
sw $t8, 628($sp)
lw $t9, 628($sp)
move $t8,$t9
lw $t9, 616($sp)
sw $t8, 0($t9)
lw $t8, 36($sp)
bne  $t8, $s5, L24
lw $t8, 40($sp)
beq  $t8, $s4, L23
lw $t8, 40($sp)
bne  $t8, $s4, L24
L23:
li $s6,1
L24:
L26:
mul  $t8,$s2,4
sw $t8, 644($sp)
lw $t8, 644($sp)
add  $t8, $t8, $k1
sw $t8, 644($sp)
lw $t9, 644($sp)
lw $t9, 0($t9)
add  $t8,$t9,1
sw $t8, 656($sp)
lw $t9, 656($sp)
move $t8,$t9
sw $t8, 36($sp)
mul  $t8,$s2,4
sw $t8, 660($sp)
lw $t8, 660($sp)
add  $t8, $t8, $s3
sw $t8, 660($sp)
lw $t9, 660($sp)
lw $t9, 0($t9)
sub  $t8,$t9,2
sw $t8, 672($sp)
lw $t9, 672($sp)
move $t8,$t9
sw $t8, 40($sp)
lw $t8, 36($sp)
sw $t8, 12($gp)
sw $t6, 16($gp)
jal  mycheck
lw $t1, 8($gp)
move $t8,$t1
sw $t8, 676($sp)
li $t8,1
sw $t8, 680($sp)
lw $t8, 676($sp)
lw $t9, 680($sp)
bne  $t8, $t9, L32
lw $t8, 40($sp)
sw $t8, 12($gp)
sw $t6, 16($gp)
jal  mycheck
lw $t1, 8($gp)
move $t8,$t1
sw $t8, 688($sp)
li $t8,1
sw $t8, 692($sp)
lw $t8, 688($sp)
lw $t9, 692($sp)
bne  $t8, $t9, L32
lw $t9, 36($sp)
mul  $t8,$t9,4
sw $t8, 700($sp)
lw $t8, 700($sp)
add  $t8, $t8, $k0
sw $t8, 700($sp)
lw $t9, 40($sp)
mul  $t8,$t9,4
sw $t8, 708($sp)
lw $t8, 708($sp)
lw $t9, 700($sp)
lw $t9, 0($t9)
add  $t8, $t8, $t9
sw $t8, 708($sp)
li $t8,1
sw $t8, 716($sp)
lw $t9, 716($sp)
neg  $t8, $t9
sw $t8, 720($sp)
lw $t8, 708($sp)
lw $t8, 0($t8)
lw $t9, 720($sp)
beq  $t8, $t9, L28
lw $t8, 708($sp)
lw $t8, 0($t8)
lw $t9, 720($sp)
bne  $t8, $t9, L32
L28:
add  $t8,$s1,1
sw $t8, 732($sp)
lw $t9, 732($sp)
move $s1,$t9
mul  $t8,$s1,4
sw $t8, 736($sp)
lw $t8, 736($sp)
add  $t8, $t8, $k1
sw $t8, 736($sp)
lw $t9, 36($sp)
move $t8,$t9
lw $t9, 736($sp)
sw $t8, 0($t9)
mul  $t8,$s1,4
sw $t8, 744($sp)
lw $t8, 744($sp)
add  $t8, $t8, $s3
sw $t8, 744($sp)
lw $t9, 40($sp)
move $t8,$t9
lw $t9, 744($sp)
sw $t8, 0($t9)
lw $t9, 36($sp)
mul  $t8,$t9,4
sw $t8, 752($sp)
lw $t8, 752($sp)
add  $t8, $t8, $k0
sw $t8, 752($sp)
lw $t9, 40($sp)
mul  $t8,$t9,4
sw $t8, 760($sp)
lw $t8, 760($sp)
lw $t9, 752($sp)
lw $t9, 0($t9)
add  $t8, $t8, $t9
sw $t8, 760($sp)
lw $t9, 32($sp)
add  $t8,$t9,1
sw $t8, 772($sp)
lw $t9, 772($sp)
move $t8,$t9
lw $t9, 760($sp)
sw $t8, 0($t9)
lw $t8, 36($sp)
bne  $t8, $s5, L30
lw $t8, 40($sp)
beq  $t8, $s4, L29
lw $t8, 40($sp)
bne  $t8, $s4, L30
L29:
li $s6,1
L30:
L32:
mul  $t8,$s2,4
sw $t8, 788($sp)
lw $t8, 788($sp)
add  $t8, $t8, $k1
sw $t8, 788($sp)
lw $t9, 788($sp)
lw $t9, 0($t9)
add  $t8,$t9,1
sw $t8, 800($sp)
lw $t9, 800($sp)
move $t8,$t9
sw $t8, 36($sp)
mul  $t8,$s2,4
sw $t8, 804($sp)
lw $t8, 804($sp)
add  $t8, $t8, $s3
sw $t8, 804($sp)
lw $t9, 804($sp)
lw $t9, 0($t9)
add  $t8,$t9,2
sw $t8, 816($sp)
lw $t9, 816($sp)
move $t8,$t9
sw $t8, 40($sp)
lw $t8, 36($sp)
sw $t8, 12($gp)
sw $t6, 16($gp)
jal  mycheck
lw $t1, 8($gp)
move $t8,$t1
sw $t8, 820($sp)
li $t8,1
sw $t8, 824($sp)
lw $t8, 820($sp)
lw $t9, 824($sp)
bne  $t8, $t9, L38
lw $t8, 40($sp)
sw $t8, 12($gp)
sw $t6, 16($gp)
jal  mycheck
lw $t1, 8($gp)
move $t8,$t1
sw $t8, 832($sp)
li $t8,1
sw $t8, 836($sp)
lw $t8, 832($sp)
lw $t9, 836($sp)
bne  $t8, $t9, L38
lw $t9, 36($sp)
mul  $t8,$t9,4
sw $t8, 844($sp)
lw $t8, 844($sp)
add  $t8, $t8, $k0
sw $t8, 844($sp)
lw $t9, 40($sp)
mul  $t8,$t9,4
sw $t8, 852($sp)
lw $t8, 852($sp)
lw $t9, 844($sp)
lw $t9, 0($t9)
add  $t8, $t8, $t9
sw $t8, 852($sp)
li $t8,1
sw $t8, 860($sp)
lw $t9, 860($sp)
neg  $t8, $t9
sw $t8, 864($sp)
lw $t8, 852($sp)
lw $t8, 0($t8)
lw $t9, 864($sp)
beq  $t8, $t9, L34
lw $t8, 852($sp)
lw $t8, 0($t8)
lw $t9, 864($sp)
bne  $t8, $t9, L38
L34:
add  $t8,$s1,1
sw $t8, 876($sp)
lw $t9, 876($sp)
move $s1,$t9
mul  $t8,$s1,4
sw $t8, 880($sp)
lw $t8, 880($sp)
add  $t8, $t8, $k1
sw $t8, 880($sp)
lw $t9, 36($sp)
move $t8,$t9
lw $t9, 880($sp)
sw $t8, 0($t9)
mul  $t8,$s1,4
sw $t8, 888($sp)
lw $t8, 888($sp)
add  $t8, $t8, $s3
sw $t8, 888($sp)
lw $t9, 40($sp)
move $t8,$t9
lw $t9, 888($sp)
sw $t8, 0($t9)
lw $t9, 36($sp)
mul  $t8,$t9,4
sw $t8, 896($sp)
lw $t8, 896($sp)
add  $t8, $t8, $k0
sw $t8, 896($sp)
lw $t9, 40($sp)
mul  $t8,$t9,4
sw $t8, 904($sp)
lw $t8, 904($sp)
lw $t9, 896($sp)
lw $t9, 0($t9)
add  $t8, $t8, $t9
sw $t8, 904($sp)
lw $t9, 32($sp)
add  $t8,$t9,1
sw $t8, 916($sp)
lw $t9, 916($sp)
move $t8,$t9
lw $t9, 904($sp)
sw $t8, 0($t9)
lw $t8, 36($sp)
bne  $t8, $s5, L36
lw $t8, 40($sp)
beq  $t8, $s4, L35
lw $t8, 40($sp)
bne  $t8, $s4, L36
L35:
li $s6,1
L36:
L38:
mul  $t8,$s2,4
sw $t8, 932($sp)
lw $t8, 932($sp)
add  $t8, $t8, $k1
sw $t8, 932($sp)
lw $t9, 932($sp)
lw $t9, 0($t9)
sub  $t8,$t9,2
sw $t8, 944($sp)
lw $t9, 944($sp)
move $t8,$t9
sw $t8, 36($sp)
mul  $t8,$s2,4
sw $t8, 948($sp)
lw $t8, 948($sp)
add  $t8, $t8, $s3
sw $t8, 948($sp)
lw $t9, 948($sp)
lw $t9, 0($t9)
sub  $t8,$t9,1
sw $t8, 960($sp)
lw $t9, 960($sp)
move $t8,$t9
sw $t8, 40($sp)
lw $t8, 36($sp)
sw $t8, 12($gp)
sw $t6, 16($gp)
jal  mycheck
lw $t1, 8($gp)
move $t8,$t1
sw $t8, 964($sp)
li $t8,1
sw $t8, 968($sp)
lw $t8, 964($sp)
lw $t9, 968($sp)
bne  $t8, $t9, L44
lw $t8, 40($sp)
sw $t8, 12($gp)
sw $t6, 16($gp)
jal  mycheck
lw $t1, 8($gp)
move $t8,$t1
sw $t8, 976($sp)
li $t8,1
sw $t8, 980($sp)
lw $t8, 976($sp)
lw $t9, 980($sp)
bne  $t8, $t9, L44
lw $t9, 36($sp)
mul  $t8,$t9,4
sw $t8, 988($sp)
lw $t8, 988($sp)
add  $t8, $t8, $k0
sw $t8, 988($sp)
lw $t9, 40($sp)
mul  $t8,$t9,4
sw $t8, 996($sp)
lw $t8, 996($sp)
lw $t9, 988($sp)
lw $t9, 0($t9)
add  $t8, $t8, $t9
sw $t8, 996($sp)
li $t8,1
sw $t8, 1004($sp)
lw $t9, 1004($sp)
neg  $t8, $t9
sw $t8, 1008($sp)
lw $t8, 996($sp)
lw $t8, 0($t8)
lw $t9, 1008($sp)
beq  $t8, $t9, L40
lw $t8, 996($sp)
lw $t8, 0($t8)
lw $t9, 1008($sp)
bne  $t8, $t9, L44
L40:
add  $t8,$s1,1
sw $t8, 1020($sp)
lw $t9, 1020($sp)
move $s1,$t9
mul  $t8,$s1,4
sw $t8, 1024($sp)
lw $t8, 1024($sp)
add  $t8, $t8, $k1
sw $t8, 1024($sp)
lw $t9, 36($sp)
move $t8,$t9
lw $t9, 1024($sp)
sw $t8, 0($t9)
mul  $t8,$s1,4
sw $t8, 1032($sp)
lw $t8, 1032($sp)
add  $t8, $t8, $s3
sw $t8, 1032($sp)
lw $t9, 40($sp)
move $t8,$t9
lw $t9, 1032($sp)
sw $t8, 0($t9)
lw $t9, 36($sp)
mul  $t8,$t9,4
sw $t8, 1040($sp)
lw $t8, 1040($sp)
add  $t8, $t8, $k0
sw $t8, 1040($sp)
lw $t9, 40($sp)
mul  $t8,$t9,4
sw $t8, 1048($sp)
lw $t8, 1048($sp)
lw $t9, 1040($sp)
lw $t9, 0($t9)
add  $t8, $t8, $t9
sw $t8, 1048($sp)
lw $t9, 32($sp)
add  $t8,$t9,1
sw $t8, 1060($sp)
lw $t9, 1060($sp)
move $t8,$t9
lw $t9, 1048($sp)
sw $t8, 0($t9)
lw $t8, 36($sp)
bne  $t8, $s5, L42
lw $t8, 40($sp)
beq  $t8, $s4, L41
lw $t8, 40($sp)
bne  $t8, $s4, L42
L41:
li $s6,1
L42:
L44:
mul  $t8,$s2,4
sw $t8, 1076($sp)
lw $t8, 1076($sp)
add  $t8, $t8, $k1
sw $t8, 1076($sp)
lw $t9, 1076($sp)
lw $t9, 0($t9)
sub  $t8,$t9,2
sw $t8, 1088($sp)
lw $t9, 1088($sp)
move $t8,$t9
sw $t8, 36($sp)
mul  $t8,$s2,4
sw $t8, 1092($sp)
lw $t8, 1092($sp)
add  $t8, $t8, $s3
sw $t8, 1092($sp)
lw $t9, 1092($sp)
lw $t9, 0($t9)
add  $t8,$t9,1
sw $t8, 1104($sp)
lw $t9, 1104($sp)
move $t8,$t9
sw $t8, 40($sp)
lw $t8, 36($sp)
sw $t8, 12($gp)
sw $t6, 16($gp)
jal  mycheck
lw $t1, 8($gp)
move $t8,$t1
sw $t8, 1108($sp)
li $t8,1
sw $t8, 1112($sp)
lw $t8, 1108($sp)
lw $t9, 1112($sp)
bne  $t8, $t9, L50
lw $t8, 40($sp)
sw $t8, 12($gp)
sw $t6, 16($gp)
jal  mycheck
lw $t1, 8($gp)
move $t8,$t1
sw $t8, 1120($sp)
li $t8,1
sw $t8, 1124($sp)
lw $t8, 1120($sp)
lw $t9, 1124($sp)
bne  $t8, $t9, L50
lw $t9, 36($sp)
mul  $t8,$t9,4
sw $t8, 1132($sp)
lw $t8, 1132($sp)
add  $t8, $t8, $k0
sw $t8, 1132($sp)
lw $t9, 40($sp)
mul  $t8,$t9,4
sw $t8, 1140($sp)
lw $t8, 1140($sp)
lw $t9, 1132($sp)
lw $t9, 0($t9)
add  $t8, $t8, $t9
sw $t8, 1140($sp)
li $t8,1
sw $t8, 1148($sp)
lw $t9, 1148($sp)
neg  $t8, $t9
sw $t8, 1152($sp)
lw $t8, 1140($sp)
lw $t8, 0($t8)
lw $t9, 1152($sp)
beq  $t8, $t9, L46
lw $t8, 1140($sp)
lw $t8, 0($t8)
lw $t9, 1152($sp)
bne  $t8, $t9, L50
L46:
add  $t8,$s1,1
sw $t8, 1164($sp)
lw $t9, 1164($sp)
move $s1,$t9
mul  $t8,$s1,4
sw $t8, 1168($sp)
lw $t8, 1168($sp)
add  $t8, $t8, $k1
sw $t8, 1168($sp)
lw $t9, 36($sp)
move $t8,$t9
lw $t9, 1168($sp)
sw $t8, 0($t9)
mul  $t8,$s1,4
sw $t8, 1176($sp)
lw $t8, 1176($sp)
add  $t8, $t8, $s3
sw $t8, 1176($sp)
lw $t9, 40($sp)
move $t8,$t9
lw $t9, 1176($sp)
sw $t8, 0($t9)
lw $t9, 36($sp)
mul  $t8,$t9,4
sw $t8, 1184($sp)
lw $t8, 1184($sp)
add  $t8, $t8, $k0
sw $t8, 1184($sp)
lw $t9, 40($sp)
mul  $t8,$t9,4
sw $t8, 1192($sp)
lw $t8, 1192($sp)
lw $t9, 1184($sp)
lw $t9, 0($t9)
add  $t8, $t8, $t9
sw $t8, 1192($sp)
lw $t9, 32($sp)
add  $t8,$t9,1
sw $t8, 1204($sp)
lw $t9, 1204($sp)
move $t8,$t9
lw $t9, 1192($sp)
sw $t8, 0($t9)
lw $t8, 36($sp)
bne  $t8, $s5, L48
lw $t8, 40($sp)
beq  $t8, $s4, L47
lw $t8, 40($sp)
bne  $t8, $s4, L48
L47:
li $s6,1
L48:
L50:
mul  $t8,$s2,4
sw $t8, 1220($sp)
lw $t8, 1220($sp)
add  $t8, $t8, $k1
sw $t8, 1220($sp)
lw $t9, 1220($sp)
lw $t9, 0($t9)
add  $t8,$t9,2
sw $t8, 1232($sp)
lw $t9, 1232($sp)
move $t8,$t9
sw $t8, 36($sp)
mul  $t8,$s2,4
sw $t8, 1236($sp)
lw $t8, 1236($sp)
add  $t8, $t8, $s3
sw $t8, 1236($sp)
lw $t9, 1236($sp)
lw $t9, 0($t9)
sub  $t8,$t9,1
sw $t8, 1248($sp)
lw $t9, 1248($sp)
move $t8,$t9
sw $t8, 40($sp)
lw $t8, 36($sp)
sw $t8, 12($gp)
sw $t6, 16($gp)
jal  mycheck
lw $t1, 8($gp)
move $t8,$t1
sw $t8, 1252($sp)
li $t8,1
sw $t8, 1256($sp)
lw $t8, 1252($sp)
lw $t9, 1256($sp)
bne  $t8, $t9, L56
lw $t8, 40($sp)
sw $t8, 12($gp)
sw $t6, 16($gp)
jal  mycheck
lw $t1, 8($gp)
move $t8,$t1
sw $t8, 1264($sp)
li $t8,1
sw $t8, 1268($sp)
lw $t8, 1264($sp)
lw $t9, 1268($sp)
bne  $t8, $t9, L56
lw $t9, 36($sp)
mul  $t8,$t9,4
sw $t8, 1276($sp)
lw $t8, 1276($sp)
add  $t8, $t8, $k0
sw $t8, 1276($sp)
lw $t9, 40($sp)
mul  $t8,$t9,4
sw $t8, 1284($sp)
lw $t8, 1284($sp)
lw $t9, 1276($sp)
lw $t9, 0($t9)
add  $t8, $t8, $t9
sw $t8, 1284($sp)
li $t8,1
sw $t8, 1292($sp)
lw $t9, 1292($sp)
neg  $t8, $t9
sw $t8, 1296($sp)
lw $t8, 1284($sp)
lw $t8, 0($t8)
lw $t9, 1296($sp)
beq  $t8, $t9, L52
lw $t8, 1284($sp)
lw $t8, 0($t8)
lw $t9, 1296($sp)
bne  $t8, $t9, L56
L52:
add  $t8,$s1,1
sw $t8, 1308($sp)
lw $t9, 1308($sp)
move $s1,$t9
mul  $t8,$s1,4
sw $t8, 1312($sp)
lw $t8, 1312($sp)
add  $t8, $t8, $k1
sw $t8, 1312($sp)
lw $t9, 36($sp)
move $t8,$t9
lw $t9, 1312($sp)
sw $t8, 0($t9)
mul  $t8,$s1,4
sw $t8, 1320($sp)
lw $t8, 1320($sp)
add  $t8, $t8, $s3
sw $t8, 1320($sp)
lw $t9, 40($sp)
move $t8,$t9
lw $t9, 1320($sp)
sw $t8, 0($t9)
lw $t9, 36($sp)
mul  $t8,$t9,4
sw $t8, 1328($sp)
lw $t8, 1328($sp)
add  $t8, $t8, $k0
sw $t8, 1328($sp)
lw $t9, 40($sp)
mul  $t8,$t9,4
sw $t8, 1336($sp)
lw $t8, 1336($sp)
lw $t9, 1328($sp)
lw $t9, 0($t9)
add  $t8, $t8, $t9
sw $t8, 1336($sp)
lw $t9, 32($sp)
add  $t8,$t9,1
sw $t8, 1348($sp)
lw $t9, 1348($sp)
move $t8,$t9
lw $t9, 1336($sp)
sw $t8, 0($t9)
lw $t8, 36($sp)
bne  $t8, $s5, L54
lw $t8, 40($sp)
beq  $t8, $s4, L53
lw $t8, 40($sp)
bne  $t8, $s4, L54
L53:
li $s6,1
L54:
L56:
mul  $t8,$s2,4
sw $t8, 1364($sp)
lw $t8, 1364($sp)
add  $t8, $t8, $k1
sw $t8, 1364($sp)
lw $t9, 1364($sp)
lw $t9, 0($t9)
add  $t8,$t9,2
sw $t8, 1376($sp)
lw $t9, 1376($sp)
move $t8,$t9
sw $t8, 36($sp)
mul  $t8,$s2,4
sw $t8, 1380($sp)
lw $t8, 1380($sp)
add  $t8, $t8, $s3
sw $t8, 1380($sp)
lw $t9, 1380($sp)
lw $t9, 0($t9)
add  $t8,$t9,1
sw $t8, 1392($sp)
lw $t9, 1392($sp)
move $t8,$t9
sw $t8, 40($sp)
lw $t8, 36($sp)
sw $t8, 12($gp)
sw $t6, 16($gp)
jal  mycheck
lw $t1, 8($gp)
move $t8,$t1
sw $t8, 1396($sp)
li $t8,1
sw $t8, 1400($sp)
lw $t8, 1396($sp)
lw $t9, 1400($sp)
bne  $t8, $t9, L62
lw $t8, 40($sp)
sw $t8, 12($gp)
sw $t6, 16($gp)
jal  mycheck
lw $t1, 8($gp)
move $t8,$t1
sw $t8, 1408($sp)
li $t8,1
sw $t8, 1412($sp)
lw $t8, 1408($sp)
lw $t9, 1412($sp)
bne  $t8, $t9, L62
lw $t9, 36($sp)
mul  $t8,$t9,4
sw $t8, 1420($sp)
lw $t8, 1420($sp)
add  $t8, $t8, $k0
sw $t8, 1420($sp)
lw $t9, 40($sp)
mul  $t8,$t9,4
sw $t8, 1428($sp)
lw $t8, 1428($sp)
lw $t9, 1420($sp)
lw $t9, 0($t9)
add  $t8, $t8, $t9
sw $t8, 1428($sp)
li $t8,1
sw $t8, 1436($sp)
lw $t9, 1436($sp)
neg  $t8, $t9
sw $t8, 1440($sp)
lw $t8, 1428($sp)
lw $t8, 0($t8)
lw $t9, 1440($sp)
beq  $t8, $t9, L58
lw $t8, 1428($sp)
lw $t8, 0($t8)
lw $t9, 1440($sp)
bne  $t8, $t9, L62
L58:
add  $t8,$s1,1
sw $t8, 1452($sp)
lw $t9, 1452($sp)
move $s1,$t9
mul  $t8,$s1,4
sw $t8, 1456($sp)
lw $t8, 1456($sp)
add  $t8, $t8, $k1
sw $t8, 1456($sp)
lw $t9, 36($sp)
move $t8,$t9
lw $t9, 1456($sp)
sw $t8, 0($t9)
mul  $t8,$s1,4
sw $t8, 1464($sp)
lw $t8, 1464($sp)
add  $t8, $t8, $s3
sw $t8, 1464($sp)
lw $t9, 40($sp)
move $t8,$t9
lw $t9, 1464($sp)
sw $t8, 0($t9)
lw $t9, 36($sp)
mul  $t8,$t9,4
sw $t8, 1472($sp)
lw $t8, 1472($sp)
add  $t8, $t8, $k0
sw $t8, 1472($sp)
lw $t9, 40($sp)
mul  $t8,$t9,4
sw $t8, 1480($sp)
lw $t8, 1480($sp)
lw $t9, 1472($sp)
lw $t9, 0($t9)
add  $t8, $t8, $t9
sw $t8, 1480($sp)
lw $t9, 32($sp)
add  $t8,$t9,1
sw $t8, 1492($sp)
lw $t9, 1492($sp)
move $t8,$t9
lw $t9, 1480($sp)
sw $t8, 0($t9)
lw $t8, 36($sp)
bne  $t8, $s5, L60
lw $t8, 40($sp)
beq  $t8, $s4, L59
lw $t8, 40($sp)
bne  $t8, $s4, L60
L59:
li $s6,1
L60:
L62:
li $t8,1
sw $t8, 1508($sp)
lw $t9, 1508($sp)
beq  $s6, $t9, L64
lw $t9, 1508($sp)
bne  $s6, $t9, L65
L64:
j  L67
L65:
add  $t8,$s2,1
sw $t8, 1520($sp)
lw $t9, 1520($sp)
move $s2,$t9
j  L15
L67:
li $t8,1
sw $t8, 1524($sp)
lw $t9, 1524($sp)
beq  $s6, $t9, L68
lw $t9, 1524($sp)
bne  $s6, $t9, L69
L68:
mul  $t8,$s5,4
sw $t8, 1536($sp)
lw $t8, 1536($sp)
add  $t8, $t8, $k0
sw $t8, 1536($sp)
mul  $t8,$s4,4
sw $t8, 1544($sp)
lw $t8, 1544($sp)
lw $t9, 1536($sp)
lw $t9, 0($t9)
add  $t8, $t8, $t9
sw $t8, 1544($sp)
lw $t8, 1544($sp)
lw $t8, 0($t8)
move $a0, $t8
li $v0, 1
syscall
la $a0, Data2
li $v0, 4
syscall
j  L70
L69:
la $a0, Data3
li $v0, 4
syscall
L70:
li $t8,0
sw $t8, 20($gp)
sw $t1, 8($gp)
lw $ra, 1572($sp)
addu $sp, $sp, 1576
jr $ra
move $t8,$t0
sw $t8, 20($gp)
sw $t1, 8($gp)
lw $ra, 1572($sp)
addu $sp, $sp, 1576
jr $ra
main:
la $gp, GLOBAL
lw $t0, 20($gp)
jal  mymain
move $t8,$t0
sw $t8, 24($gp)
li $v0, 10
syscall
.data 0x10000000
printf_buf: .space 2
Data0: 
.asciiz "%d\n"
Data1: 
.asciiz "no solution!\n"
Data2: 
.asciiz "\n"
Data3: 
.asciiz "no solution!\n"
.align 4
GLOBAL:
