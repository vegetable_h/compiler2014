.text
mycheck:
subu $sp, $sp, 32
sw $ra, 28($sp)
lw $t8, 120068($gp)
sw $t8, 0($sp)
lw $t8, 120072($gp)
sw $t8, 4($sp)
lw $t8, 0($sp)
lw $t9, 4($sp)
slt  $t8, $t8, $t9
sw $t8, 8($sp)
lw $t8, 8($sp)
beqz  $t8,L1
lw $t9, 0($sp)
sge  $t8,$t9,0
sw $t8, 16($sp)
lw $t9, 16($sp)
move $t8,$t9
sw $t8, 20($sp)
j  L2
L1:
li $t8,0
sw $t8, 20($sp)
L2:
lw $t9, 20($sp)
move $t8,$t9
sw $t8, 120064($gp)
lw $ra, 28($sp)
addu $sp, $sp, 32
jr $ra
lw $t9, 24($sp)
move $t8,$t9
sw $t8, 120064($gp)
lw $ra, 28($sp)
addu $sp, $sp, 32
jr $ra

malloc: 
li $v0, 9
syscall
jr $ra

printf:
sw $ra, -4($sp)
sw $s3, -8($sp)
sw $s4, -12($sp)
sw $s5, -16($sp)
sw $s6, -20($sp)
sw $t0, -24($sp)
sw $t1, -28($sp)
move $t0, $v0
move $t1, $v1
move $s4, $a0 
la $s6, printf_buf 

printf_loop: 
lb $s5, 0($s4) 
addu $s4, $s4, 1
beq $s5, '%', printf_fmt
beq $0, $s5, printf_end
sb $s5, 0($s6) 
sb $0, 1($s6) 
move $a0, $s6
li $v0, 4 
syscall
b printf_loop 

printf_fmt:
lb $s5, 0($s4)
addu $s4, $s4, 1
beq $s5, 'd', printf_int
beq $s5, 's', printf_str
beq $s5, 'c', printf_char
beq $s5, '0', printf_width

printf_shift_args: 
move $a1, $a2 
move $a2, $a3 
move $a3, $t0 
move $t0, $t1
b printf_loop 

printf_int: 
move $a0, $a1 
li $v0, 1
syscall
b printf_shift_args 

printf_str: 
move $a0, $a1 
li $v0, 4
syscall
b printf_shift_args 

printf_char: 
sb $a1, 0($s6) 
sb $0, 1($s6) 
move $a0, $s6 
li $v0, 4 
syscall
b printf_shift_args 

printf_width:
move $s3, $a1
lb $s5, 0($s4)
addu $s4, $s4, 2
subu $s5, $s5, '1'
divu $a1, $a1, 10

calc_zero_number:
beqz $a1 print_zeros
subu $s5, $s5, 1
divu $a1, $a1, 10
b calc_zero_number

print_zeros:
print_single_zero:
beqz $s5, print_number
subu $s5, $s5, 1
li $a0, 0
li $v0, 1
syscall
b print_single_zero

print_number:
move $a0, $s3
li $v0, 1
syscall
b printf_shift_args

printf_end:
lw $ra, -4($sp)
lw $s3, -8($sp)
lw $s4, -12($sp)
lw $s5, -16($sp)
lw $s6, -20($sp)
lw $t0, -24($sp)
lw $t1, -28($sp)
jr $ra
myaddList:
lw $t1, 32($gp)
lw $t2, 28($gp)
lw $t3, 80052($gp)
lw $t4, 80044($gp)
lw $t5, 120064($gp)
lw $t6, 8($gp)
subu $sp, $sp, 136
sw $ra, 132($sp)
lw $t7, 120080($gp)
lw $s0, 120084($gp)
sw $t7, 120068($gp)
sw $t6, 120072($gp)
jal  mycheck
lw $t1, 32($gp)
lw $t2, 28($gp)
lw $t3, 80052($gp)
lw $t4, 80044($gp)
lw $t5, 120064($gp)
lw $t6, 8($gp)
move $s1,$t5
li $s2,1
bne  $s1, $s2, L7
sw $s0, 120068($gp)
sw $t6, 120072($gp)
jal  mycheck
lw $t1, 32($gp)
lw $t2, 28($gp)
lw $t3, 80052($gp)
lw $t4, 80044($gp)
lw $t5, 120064($gp)
lw $t6, 8($gp)
move $s3,$t5
li $s4,1
bne  $s3, $s4, L7
la $s5, 80056($gp)
mul  $s6,$t7,400
add  $s6, $s6, $s5
mul  $s7,$s0,4
add  $s7, $s7, $s6
li $k0,1
neg  $k1, $k0
lw $t8, 0($s7)
beq  $t8, $k1, L3
lw $t8, 0($s7)
bne  $t8, $k1, L7
L3:
add  $fp,$t4,1
move $t4,$fp
la $s1, 44($gp)
mul  $s2,$t4,4
add  $s2, $s2, $s1
move $t8,$t7
sw $t8, 0($s2)
la $t6, 40044($gp)
mul  $t5,$t4,4
add  $t5, $t5, $t6
move $t8,$s0
sw $t8, 0($t5)
la $s3, 80056($gp)
mul  $s4,$t7,400
add  $s4, $s4, $s3
mul  $s5,$s0,4
add  $s5, $s5, $s4
add  $s6,$t3,1
move $t8,$s6
sw $t8, 0($s5)
seq  $k0, $t2, $t7
beqz  $k0,L5
seq  $s7, $t1, $s0
bnez  $s7,L4
beqz  $s7,L5
L4:
li $t8,1
sw $t8, 80048($gp)
L5:
L7:
move $t8,$t0
sw $t8, 120076($gp)
sw $t1, 32($gp)
sw $t2, 28($gp)
sw $t3, 80052($gp)
sw $t4, 80044($gp)
lw $ra, 132($sp)
addu $sp, $sp, 136
jr $ra
main:
la $gp, GLOBAL
lw $t0, 120088($gp)
li $t8,100
sw $t8, 12($gp)
li $t8,100
sw $t8, 8($gp)
sw $t0, 120088($gp)
jal  mymain
lw $t0, 120088($gp)
move $t8,$t0
sw $t8, 120092($gp)
li $v0, 10
syscall
mymain:
lw $t1, 80048($gp)
lw $t2, 8($gp)
lw $t3, 16($gp)
lw $t4, 80044($gp)
lw $t6, 32($gp)
lw $t7, 28($gp)
lw $s0, 120056($gp)
lw $s2, 120060($gp)
subu $sp, $sp, 328
sw $ra, 324($sp)
sub  $t5,$t2,1
move $t6,$t5
move $t7,$t5
li $s0,0
L9:
slt  $s1, $s0, $t2
beqz  $s1,L14
lw $s1, 36($gp)
li $s2,0
L10:
slt  $s3, $s2, $t2
beqz  $s3,L12
la $s4, 80056($gp)
mul  $s5,$s0,400
add  $s5, $s5, $s4
mul  $s6,$s2,4
add  $s6, $s6, $s5
lw $s5, 40($gp)
li $s7,1
neg  $k0, $s7
move $t8,$k0
sw $t8, 0($s6)
addiu  $s2,$s2,1
j  L10
L12:
addiu  $s0,$s0,1
j  L9
L14:
L15:
sge  $k1, $t4, $t3
beqz  $k1,L19
la $fp, 44($gp)
mul  $t5,$t3,4
add  $t5, $t5, $fp
lw $t9, 0($t5)
move $s1,$t9
la $s3, 40044($gp)
mul  $s4,$t3,4
add  $s4, $s4, $s3
lw $t9, 0($s4)
move $s5,$t9
la $s7, 80056($gp)
mul  $s6,$s1,400
add  $s6, $s6, $s7
mul  $k0,$s5,4
add  $k0, $k0, $s6
lw $t9, 0($k0)
move $t8,$t9
sw $t8, 80052($gp)
sub  $s2,$s1,1
sub  $s0,$s5,2
sw $s2, 120080($gp)
sw $s0, 120084($gp)
sw $t0, 320($sp)
sw $t1, 80048($gp)
sw $t2, 8($gp)
sw $t3, 16($gp)
sw $t4, 80044($gp)
sw $t6, 32($gp)
sw $t7, 28($gp)
sw $s1, 36($gp)
sw $s5, 40($gp)
jal  myaddList
lw $t0, 320($sp)
lw $t1, 80048($gp)
lw $t2, 8($gp)
lw $t3, 16($gp)
lw $t4, 80044($gp)
lw $t6, 32($gp)
lw $t7, 28($gp)
lw $s1, 36($gp)
lw $s5, 40($gp)
sub  $k1,$s1,1
add  $fp,$s5,2
sw $k1, 120080($gp)
sw $fp, 120084($gp)
sw $t0, 320($sp)
sw $t1, 80048($gp)
sw $t2, 8($gp)
sw $t3, 16($gp)
sw $t4, 80044($gp)
sw $t6, 32($gp)
sw $t7, 28($gp)
sw $s1, 36($gp)
sw $s5, 40($gp)
jal  myaddList
lw $t0, 320($sp)
lw $t1, 80048($gp)
lw $t2, 8($gp)
lw $t3, 16($gp)
lw $t4, 80044($gp)
lw $t6, 32($gp)
lw $t7, 28($gp)
lw $s1, 36($gp)
lw $s5, 40($gp)
add  $t5,$s1,1
sub  $s3,$s5,2
sw $t5, 120080($gp)
sw $s3, 120084($gp)
sw $t0, 320($sp)
sw $t1, 80048($gp)
sw $t2, 8($gp)
sw $t3, 16($gp)
sw $t4, 80044($gp)
sw $t6, 32($gp)
sw $t7, 28($gp)
sw $s1, 36($gp)
sw $s5, 40($gp)
jal  myaddList
lw $t0, 320($sp)
lw $t1, 80048($gp)
lw $t2, 8($gp)
lw $t3, 16($gp)
lw $t4, 80044($gp)
lw $t6, 32($gp)
lw $t7, 28($gp)
lw $s1, 36($gp)
lw $s5, 40($gp)
add  $s4,$s1,1
add  $s7,$s5,2
sw $s4, 120080($gp)
sw $s7, 120084($gp)
sw $t0, 320($sp)
sw $t1, 80048($gp)
sw $t2, 8($gp)
sw $t3, 16($gp)
sw $t4, 80044($gp)
sw $t6, 32($gp)
sw $t7, 28($gp)
sw $s1, 36($gp)
sw $s5, 40($gp)
jal  myaddList
lw $t0, 320($sp)
lw $t1, 80048($gp)
lw $t2, 8($gp)
lw $t3, 16($gp)
lw $t4, 80044($gp)
lw $t6, 32($gp)
lw $t7, 28($gp)
lw $s1, 36($gp)
lw $s5, 40($gp)
sub  $s6,$s1,2
sub  $k0,$s5,1
sw $s6, 120080($gp)
sw $k0, 120084($gp)
sw $t0, 320($sp)
sw $t1, 80048($gp)
sw $t2, 8($gp)
sw $t3, 16($gp)
sw $t4, 80044($gp)
sw $t6, 32($gp)
sw $t7, 28($gp)
sw $s1, 36($gp)
sw $s5, 40($gp)
jal  myaddList
lw $t0, 320($sp)
lw $t1, 80048($gp)
lw $t2, 8($gp)
lw $t3, 16($gp)
lw $t4, 80044($gp)
lw $t6, 32($gp)
lw $t7, 28($gp)
lw $s1, 36($gp)
lw $s5, 40($gp)
sub  $s2,$s1,2
add  $s0,$s5,1
sw $s2, 120080($gp)
sw $s0, 120084($gp)
sw $t0, 320($sp)
sw $t1, 80048($gp)
sw $t2, 8($gp)
sw $t3, 16($gp)
sw $t4, 80044($gp)
sw $t6, 32($gp)
sw $t7, 28($gp)
sw $s1, 36($gp)
sw $s5, 40($gp)
jal  myaddList
lw $t0, 320($sp)
lw $t1, 80048($gp)
lw $t2, 8($gp)
lw $t3, 16($gp)
lw $t4, 80044($gp)
lw $t6, 32($gp)
lw $t7, 28($gp)
lw $s1, 36($gp)
lw $s5, 40($gp)
add  $k1,$s1,2
sub  $fp,$s5,1
sw $k1, 120080($gp)
sw $fp, 120084($gp)
sw $t0, 320($sp)
sw $t1, 80048($gp)
sw $t2, 8($gp)
sw $t3, 16($gp)
sw $t4, 80044($gp)
sw $t6, 32($gp)
sw $t7, 28($gp)
sw $s1, 36($gp)
sw $s5, 40($gp)
jal  myaddList
lw $t0, 320($sp)
lw $t1, 80048($gp)
lw $t2, 8($gp)
lw $t3, 16($gp)
lw $t4, 80044($gp)
lw $t6, 32($gp)
lw $t7, 28($gp)
lw $s1, 36($gp)
lw $s5, 40($gp)
add  $t5,$s1,2
add  $s3,$s5,1
sw $t5, 120080($gp)
sw $s3, 120084($gp)
sw $t0, 320($sp)
sw $t1, 80048($gp)
sw $t2, 8($gp)
sw $t3, 16($gp)
sw $t4, 80044($gp)
sw $t6, 32($gp)
sw $t7, 28($gp)
jal  myaddList
lw $t0, 320($sp)
lw $t1, 80048($gp)
lw $t2, 8($gp)
lw $t3, 16($gp)
lw $t4, 80044($gp)
lw $t6, 32($gp)
lw $t7, 28($gp)
li $s4,1
beq  $t1, $s4, L16
bne  $t1, $s4, L17
L16:
j  L19
L17:
add  $s7,$t3,1
move $t3,$s7
j  L15
L19:
li $s6,1
beq  $t1, $s6, L20
bne  $t1, $s6, L21
L20:
la $k0, 80056($gp)
mul  $s2,$t7,400
add  $s2, $s2, $k0
mul  $s0,$t6,4
add  $s0, $s0, $s2
lw $t8, 0($s0)
move $a0, $t8
li $v0, 1
syscall
la $a0, Data2
li $v0, 4
syscall
j  L22
L21:
la $a0, Data3
li $v0, 4
syscall
L22:
li $t8,0
sw $t8, 120088($gp)
sw $t1, 80048($gp)
sw $t2, 8($gp)
sw $t3, 16($gp)
sw $t4, 80044($gp)
sw $t6, 32($gp)
sw $t7, 28($gp)
sw $s1, 36($gp)
sw $s5, 40($gp)
lw $ra, 324($sp)
addu $sp, $sp, 328
jr $ra
move $t8,$t0
sw $t8, 120088($gp)
sw $t1, 80048($gp)
sw $t2, 8($gp)
sw $t3, 16($gp)
sw $t4, 80044($gp)
sw $t6, 32($gp)
sw $t7, 28($gp)
sw $s1, 36($gp)
sw $s5, 40($gp)
lw $ra, 324($sp)
addu $sp, $sp, 328
jr $ra
.data 0x10000000
printf_buf: .space 2
Data0: 
.asciiz "%d\n"
Data1: 
.asciiz "no solution!\n"
Data2: 
.asciiz "\n"
Data3: 
.asciiz "no solution!\n"
.align 4
GLOBAL:
