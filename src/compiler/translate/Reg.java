package compiler.translate;

import java.util.BitSet;


public class Reg{
	static int tot = 0;
	static MemLoc[] mem = new MemLoc[50000]; 
	static Register[] r = new Register[50000];
	static int[] tail = new int[50000];
	static int[] head = new int[50000];
	static BitSet notCopy = new BitSet(50000);
	static BitSet moved = new BitSet(50000);
	static Reg[] target = new Reg[50000];
	public int num;
	int start;
	int end;
	public Reg(){
		num = tot ++;	
		r[num] = null;
		start= end = -1;

	}
	public Reg(int n){
		num = n;
		r[num] = null;
		start = end = -1;
	}
	static void clear(){moved.clear(); notCopy.clear();}
	static void move(int n, Reg targ){
		moved.set(n);
		target[n] = targ;
	}
	static Reg getTarget(int n){return target[n];}
	static void setNotCopy(int n){notCopy.set(n);}
	static boolean moved(int n){return moved.get(n);}
	static boolean isCopy(int n){return !notCopy.get(n);}
	public void addMemLoc(MemLoc m){
		mem[num] = m;
	}
	public boolean isglb(){
		return  mem[num].global();
	}
	public boolean needStore(){
		return ((!mem[num].regisible()) || mem[num].global());
	}
	public boolean regisible(){
		return !mem[num].regisible();
	}
	public MemLoc getMem(){
		return mem[num];	
	}
	public void addRegister(Register R){
		r[num] = R;
	}
	public Register getRegister(){
		return r[num];
	}
	public void setEnd(){
		tail[num] = end;
	}
	public int getEnd(){
		return tail[num];
	}
	public void setStart(){
		head[num] = start;
	}
	public int getStart(){
		return head[num];
	}
	@Override
		public boolean equals(Object ob){
			if (!(ob instanceof Reg)) return false;	
			return (((Reg)ob).num == num);
		}
	@Override
		public int hashCode(){
			return num;
		}
	/*
	public Reg(int cur, Mem m, ASM a){
		if (m.pointer != null){
			getfrom(m.pointer, a);
			a.add("lw " + Name[cur] + ", " + m.offset + "($t6)");
		}else a.add("lw " + Name[cur] + ", "+ m.name);
		name = Name[cur]; return;
	}
	private void getfrom(VarInfo x, ASM a){
		if (x.loc.pointer != null){
			getfrom(x.loc.pointer, a);
			a.add("lw $t6, " + x.loc.offset + "($t6)");
		}else{
			a.add("lw $t6, " + x.loc.name);
		}
	}
	public Reg(ASM a){
		name = Name[11]; return;
	}
	public Reg(int cur, ASM a){
		name = Name[cur];
	}*/
	/*static void outFunc(Func f){
		for (int i = 2; i < 26; i ++)	
			if (r[i] != null && r[i].func != null)
				r[i] = null;
	} */

}
