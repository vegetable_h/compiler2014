package compiler.translate;

import compiler.semantic.Array;
import compiler.semantic.Func;
import compiler.semantic.Struct;
import compiler.semantic.Symbol;
import compiler.semantic.Type;

public class Mem{
	static int global = 0;
	private static java.util.Dictionary<Func, Integer> 
		memory = new java.util.Hashtable<Func, Integer>();
	Func func;
	int loc;
	int offset;
	boolean regisible;
	public VarInfo pointer;
	String name;
	MemLoc memloc;
	public Mem(boolean rsible){
		offset = 0;
		pointer = null;
		func = null;
		regisible = rsible;
		memloc = new MemLoc(this);
	}
	public Mem(Type type, Func cur, boolean rsible){
		regisible = rsible;
		if (cur.cur.name.equals(Symbol.symbol("glb"))){
			loc = global;
			func = cur;
			name = loc + "($gp)"; 
			global += type.size();
			pointer = null;
			offset = 0;
			if (type instanceof Array || type instanceof Struct)
				regisible = false;
			memloc = new MemLoc(this);
			return ;
		}
		offset = 0;
		pointer = null;
		func = cur;
		int tot;
		if (memory.get(cur) == null) 
			tot = 0; 
		else tot = memory.get(cur); 
		loc = tot;
		tot += type.size();	
		name = loc + "($sp)";
		memory.put(func, tot);
		if (type instanceof Array || type instanceof Struct)
			regisible = false;
		memloc = new MemLoc(this);
	}
	public Mem(Type type, Func cur, boolean rsible, VarInfo p, int o){
		regisible = rsible;
		if (cur.cur.name.equals(Symbol.symbol("glb"))){
			loc = global;
			func = cur;
			name = loc + "($gp)"; 
			global += type.size();
			pointer = p;
			offset = o;
			if (type instanceof Array || type instanceof Struct)
				regisible = false;
			memloc = new MemLoc(this);
			return ;
		}
		offset = o;
		pointer = p;
		func = cur;
		int tot;
		if (memory.get(cur) == null) 
			tot = 0; 
		else tot = memory.get(cur); 
		loc = tot;
		tot += type.size();	
		name = loc + "($sp)";
		memory.put(func, tot);
		if (type instanceof Array || type instanceof Struct)
			regisible = false;
		memloc = new MemLoc(this);
	}
	static int getSize(Func f){
		return memory.get(f);
	}
	public int getLoc(){
		return loc;
	}
	public void changeRegisible(){
		regisible = false;
		memloc.changeRegisible();
	}
	public Mem(Mem start, int _offset){
		if (start.pointer != null){
			func = start.func;
			pointer = start.pointer;
			offset = start.offset + _offset;
			regisible = false;
			memloc = new MemLoc(this);
			return ;
		}
		offset = start.offset + _offset;
		func = start.func;				
		loc = start.loc + _offset;
		regisible = start.regisible;
		if (func.cur.name.equals(Symbol.symbol("glb"))) 
			name = loc + "($gp)";
		else name = loc + "($sp)";
		memloc = new MemLoc(this);
	} 
	public Mem(int size, Func f, boolean rsible){
		offset = 0;
		func = f;
		loc = global;
		regisible = rsible;
		global += size;	
		name = loc + "($gp)";
		memloc = new MemLoc(this);
	}
	public String getCtx(){
		return "$" + loc;
	}
	@Override
		public boolean equals(Object ob){
			if (!(ob instanceof Mem)) return false; 	
			if (((Mem)ob).pointer != null) return false;
			if (pointer != null) return false;
			if (((Mem)ob).func == null)
				return func == null;
			if (func == null) return ((Mem)ob).func == null;
			if (((Mem)ob).func.cur.name != 
					func.cur.name) return false;
			return loc == ((Mem)ob).loc; 
		}
}
