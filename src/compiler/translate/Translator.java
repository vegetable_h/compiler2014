package compiler.translate;

import java.util.Vector;


import compiler.semantic.Func;
import compiler.semantic.LabelTag;
import compiler.semantic.Type;
import compiler.semantic.Loop;
import compiler.semantic.Struct;
import compiler.semantic.Union;

public class Translator{
	static int tot_label = 0;	
	//static Reg sp;
	IRWriter irw;
	private static java.util.Dictionary<Loop, Vector<Tag>>
			continueOp  = new java.util.Hashtable<Loop, Vector<Tag>>();
	private static java.util.Dictionary<Loop, Vector<Tag>>
			breakOp = new java.util.Hashtable<Loop, Vector<Tag>>();
	private static java.util.Dictionary<LabelTag, Vector<Tag>> 
			Labels = new java.util.Hashtable<LabelTag, Vector<Tag>>();
	public Translator(){
		irw = new IRWriter();	
	//	sp = new Reg("$sp");
	}
	public IRWriter ir(){
		return irw;
	}
	public void assign(VarInfo l, String op, VarInfo r, Func f){
		String cur_op = ""; 	
		if (l.type instanceof Struct || l.type instanceof Union){
			assignRecord(l, op, r, f);
		}else{ 
			if (op.length() > 1){
				for (int j = 0; j < op.length() - 1; j ++) cur_op += op.charAt(j);
				if(r.isImme)
					irw.appendCalcI(l.loc.memloc, l.loc.memloc.reg, cur_op, r.val, f);
				else irw.appendArithmetic(l.loc.memloc, l.loc.memloc.reg, cur_op, r.loc.memloc.reg, f);
			}else{
				if(r.isImme){
					irw.MOVI(l.loc.memloc, r.val, f);
				//	System.out.println(l.loc.memloc.reg.num);
				}else irw.Assign(l.loc.memloc, r.loc.memloc.reg, f); 
			}
		}
	}

	public void assignRecord(VarInfo l, String op, VarInfo r, Func f){
		Type typ = l.type;
		for (int i = 0; i < typ.size(); i += 4){ 
			Mem L, R;
			L = new Mem(l.loc, i);
			R = new Mem(r.loc, i);
			irw.Assign(L.memloc, R.memloc.reg, f);
		}
	}

	public void returnVar(Func f, VarInfo cur, VarInfo res){
		assign(res, "=", cur, f);
		irw.outFunc(f);
		irw.appendReturn(f);	
	}
	public void returnVoid(Func f){
		irw.outFunc(f);
		irw.appendReturn(f);
	}
	public void appendContinue(Loop loop, Func f){
		Vector<Tag> e = continueOp.get(loop);	
		if (e == null){
			e = new Vector<Tag>();	
		}
		e.add(irw.appendUnknownGoto(f));
		continueOp.put(loop, e);
	}
	public void appendBreak(Loop loop, Func f){
		Vector<Tag> e = breakOp.get(loop);	
		if (e == null){
			e = new Vector<Tag>();	
		}
		e.add(irw.appendUnknownGoto(f));
		breakOp.put(loop, e);
	}
	public Label addLabel(Func f){
		return irw.appendLabel("", f);
	} 

	public void appendArithmetic(VarInfo des, VarInfo s, String op, VarInfo t, LabelTag trueBranch, LabelTag falseBranch, Func f){
		if (trueBranch != null){
			Vector<Tag> e = Labels.get(trueBranch);
			if (e == null) e = new Vector<Tag>();
			Tag tag = irw.appendBNEZ(des.loc.memloc, s.loc.memloc.reg, op, t.loc.memloc.reg, f);
			e.add(tag);
			Labels.put(trueBranch, e);
		}
		if (falseBranch != null){
			Vector<Tag> e = Labels.get(falseBranch);
			if (e == null) e = new Vector<Tag>();
			Tag tag = irw.appendBEZ(des.loc.memloc, s.loc.memloc.reg, op, t.loc.memloc.reg, f);
			Labels.put(falseBranch, e);
			e.add(tag);
		}
		if (trueBranch == null && falseBranch == null)
			if (t.isImme == true)
				irw.appendCalcI(des.loc.memloc, s.loc.memloc.reg, op, t.val, f);
			else
				irw.appendArithmetic(des.loc.memloc, s.loc.memloc.reg, op, t.loc.memloc.reg, f);			
			
	} 
	public void appendMULI(VarInfo des, VarInfo s, int t, Func f){
		irw.appendMULI(des.loc.memloc, s.loc.memloc.reg, t,  f);
	}
	public void assignInt(VarInfo des, int s, Func f){
		irw.MOVI(des.loc.memloc, s, f);
	} 
	public void appendADDI(VarInfo des, VarInfo s, int t, Func f){
		irw.appendADDI(des.loc.memloc, s.loc.memloc.reg, t, f);
	}
	public void appendSUBI(VarInfo des, VarInfo s, int t, Func f){
		irw.appendSUBI(des.loc.memloc, s.loc.memloc.reg, t, f);
	}
	public void appendSingleOp(VarInfo des, String Op, VarInfo s, Func f){
		irw.appendSingleOp(des.loc.memloc, Op, s.loc.memloc.reg, f);
	}
	public void appendFunc(VarInfo res, VarInfoLabelPair vlp, Vector<VarInfo> v, Func f){
		Mem var = vlp.loc.param;	
		for (int i = 0; i < v.size(); i ++){
			appendStore(v.get(i), var, f);
			var = new Mem(var, v.get(i).type.size());
		}
		irw.appendJAL(vlp.lab, f);
		assign(res, "=", vlp.loc, f);
	} 
	public void appendCheckLoop(VarInfo tag, Loop loop, Func f){
		Tag cur = irw.appendBEZ(null, tag.loc.memloc.reg, "", null, f);
		Vector<Tag> e = breakOp.get(loop); 
		if (e == null){
			e = new Vector<Tag>();	
		}
		e.add(cur);
		breakOp.put(loop, e);
	}	
	public void appendGoto(Label lab, Func f){
		irw.appendGoto(lab, f);	
	}
	public void appendData(VarInfo res, String ctx, Func f){
		irw.appendData(res.loc.memloc, ctx, f);	
	}
	public void fillLoop(Loop loop, Label continueLabel, Label breakLabel, Func f){
		Vector<Tag> e = continueOp.get(loop); 	
		if (e != null){
			for (int i = 0; i < e.size(); i ++)	
				irw.fillGoto(continueLabel, e.get(i), f);
		}
		e = breakOp.get(loop); 	
		if (e != null){
			for (int i = 0; i < e.size(); i ++)	
				irw.fillGoto(breakLabel, e.get(i), f);
		}
	}
	public void assignMem(VarInfo des, VarInfo s, Func f){
		irw.assignMem(des.loc.memloc, s.loc.memloc, f);
	}
	public void assignLa(VarInfo des, Mem m, Func f){
		irw.assignLa(des.loc.memloc, m.memloc, f);
	}
	public void appendStore(VarInfo s, Mem des, Func f){
		for (int i = 0; i < s.type.size(); i += 4){
			Mem r = new Mem(s.loc, i);
			Mem L = new Mem(des, i);
			irw.appendMemST(L.memloc, r.memloc.reg, f);	
		} 		
	} 
	public void appendLoad(VarInfo des, Mem t, Func f){
		for (int i = 0; i < des.type.size(); i += 4){
			Mem r = new Mem(t, i); 
			Mem L = new Mem(des.loc, i);
			irw.appendLoad(L.memloc, r.memloc, f);	
		} 
	}  

	public void InFunc(Func f){
		irw.InFunc(f);
	}
	public void appendPrintf(Vector<VarInfo> v, Func f){
		Vector<Reg> para = new Vector<Reg>();
		for (int i = 1; i < v.size(); i ++){
			para.add(v.get(i).loc.memloc.reg);
		}
		irw.appendPrintf(v.get(0).data, para, f);
	}
	public void appendMalloc(VarInfo res, Vector<VarInfo> v, Func f){
		irw.appendMalloc(res.loc.memloc, v.get(0).loc.memloc.reg, f);
	}
	public Label appendLabel(String N, Func f){
		return irw.appendLabel(N, f);
	}
	public void addUnknownGoto(LabelTag branch, Func func) {
		Tag tag = irw.appendUnknownGoto(func);
		Vector<Tag> e = Labels.get(branch);
		if (e == null) e = new Vector<Tag>();
		e.add(tag);
		Labels.put(branch, e);
	}
	public void fillLabel(LabelTag branch, Label lab, Func func) {
		Vector<Tag> e= Labels.get(branch);
		if (e != null)
			for (int i = 0; i < e.size(); i ++)
				irw.fillGoto(lab, e.get(i), func);
		
	}
	public void appendBNEZ(VarInfo res, LabelTag branch, Func func) {
		Tag tag = irw.appendBNEZ(null, res.loc.memloc.reg, "", null, func);
		Vector<Tag> e = Labels.get(branch);
		if (e == null) e = new Vector<Tag>();
		e.add(tag);
		Labels.put(branch, e);
	}
	public void appendBEZ(VarInfo res, LabelTag branch, Func func) {
		Tag tag = irw.appendBEZ(null, res.loc.memloc.reg, "", null, func);
		Vector<Tag> e = Labels.get(branch);
		if (e == null) e = new Vector<Tag>();
		e.add(tag);
		Labels.put(branch, e);
	}
}

