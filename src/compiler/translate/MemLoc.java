package compiler.translate;

import compiler.semantic.Func;

public class MemLoc{
	Func func;
	Reg pointer, grandpointer;
	String name;	
	public Reg reg;
	int loc, offset;
	Mem mem;
	private static java.util.Dictionary<Reg, Reg[]> 
		hpointer = new java.util.Hashtable<Reg, Reg[]>();
	private static java.util.Dictionary<Func, Reg[]> 
		hfunc = new java.util.Hashtable<Func, Reg[]>();
	public MemLoc(Mem m){
		mem = m;
		func = m.func;
		if (m.pointer == null) grandpointer = pointer = null;
		else{
			Mem tmp = m.pointer.loc;  
			if (tmp.pointer != null){
				Mem tt = tmp.pointer.loc;
				grandpointer = getRegFunc(tt.func, tt.loc / 4);
				pointer = getRegPointer(grandpointer, tmp.offset / 4);
			}else{
				grandpointer = null;
				pointer = getRegFunc(tmp.func, tmp.loc / 4);
			}
		}
		loc = m.loc;
		offset = m.offset;
		name = m.name;
		if (pointer != null)
			reg = getRegPointer(pointer, offset / 4);
		else{
			//System.out.println(reg.num + ": "+ func.cur.name + "," + loc);
			reg = getRegFunc(func, loc / 4);
		}
		reg.addMemLoc(this);
	}
	public boolean global(){
		if (func.prev == null) return true;
		return false;
	}
	private Reg getRegPointer(Reg p, int o){
		Reg res;
		if (hpointer.get(p) != null){
			Reg[] e = hpointer.get(p);	
			if (e[o] == null){
				res = new Reg();
				e[o] = res;
				hpointer.put(p, e);
			}else res = e[o];
		}else{
			Reg[] e = new Reg[50000];	
			res = new Reg();
			e[o] = res;
			hpointer.put(p, e);
		}	
		return res;
	}
	private Reg getRegFunc(Func f, int l){
		Reg res;
	//	if (f == null) System.out.println("WTF");
		if (hfunc.get(f) != null){
			Reg[] e = hfunc.get(f);	
			if (e[l] == null){
				res = new Reg();
				e[l] = res;
				hfunc.put(f , e);
			}else res = e[l];
		}else{
			Reg[] e = new Reg[50000];
			res = new Reg();
			e[l] = res;
			hfunc.put(func, e);
		}
		return res;
	}
	public boolean regisible(){
		return mem.regisible;
	}
	public void changeRegisible(){
		mem.regisible = false;
	}
}
