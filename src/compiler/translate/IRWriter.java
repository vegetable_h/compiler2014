package compiler.translate;

import java.util.Arrays;
import java.util.BitSet;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import compiler.semantic.Func;
import compiler.semantic.Symbol;

public class IRWriter{
	Vector<Block> b;
	Vector<String> data;  
	private static java.util.Dictionary<Func, Vector<Instruction>> 
		ins = new java.util.Hashtable<Func, Vector<Instruction>>();
	ASM a;
	public IRWriter(){
		data = new Vector<String>();
		b = new Vector<Block>(); 
	}
	/*
	   public void DivideBlock(){
	   b.add(new Block(main));
	   Block cur = new Block();
	   for (int i = 0; i < ins.size(); i ++){
	   if (ins.get(i) instanceof LabelIt){
	   if (((LabelIt)ins.get(i)).lab.callee == null)
	   continue;
	   }
	   if (ins.get(i) instanceof LabelIt || i == 0 ||
	   ins.get(i - 1) instanceof GotoIns ||
	   ins.get(i - 1) instanceof Branch ||
	   ins.get(i - 1) instanceof Return){
	   b.add(cur);	
	   cur.clear();
	   cur.add(ins.get(i));
	   }else cur.add(ins.get(i));
	   } 
	   for (int i = 0; i < b.size(); i ++){
	   Block cur = b.get(i);	
	   for (int j = 0; j < cur.size(); j ++){
	   cur.get(j).checkUse(cur);	//use before define	
	   cur.get(j).checkDefine(cur); //define before use
	   cur.get(j).calcLiveness(i);
	   }
	   }
	   }*/

	public void toASM(){

		a = new ASM();
		a.add(".text");
		boolean RAswitch = true;
		for (Iterator itr = ((Hashtable<Func, Vector<Instruction>>) ins).keySet().iterator(); itr.hasNext();){
			Func f = (Func)itr.next();
			if (f.cur.name.equals(Symbol.symbol("getcount"))){
				RAswitch = false;
				break;
			}
		}	
		for (Iterator itr = ((Hashtable<Func, Vector<Instruction>>) ins).keySet().iterator(); itr.hasNext();){ 
			Func f = (Func)itr.next();	
			if (f.cur.name.equals(Symbol.symbol("glb"))){
				a.add("main:");
				a.add("la $gp, GLOBAL");
			}
			if (f.cur.name.equals(Symbol.symbol("check"))){
				RAswitch = false;
			}else RAswitch = true;
			Vector<Instruction> v= (Vector<Instruction>)ins.get(f);
			boolean go_on = true;
			Reg[] q = new Reg[Reg.tot];
			while (go_on){
			/*Reg.clear();
			for (int i = 0; i < v.size(); i ++){
				Reg cur = v.get(i).changed();
				if (cur != null){
					if (!(v.get(i) instanceof Mov)){
						Reg.setNotCopy(cur.num);
					}else 
						if (Reg.moved(cur.num)) Reg.setNotCopy(cur.num);
							else Reg.move(cur.num, ((Mov)v.get(i)).s);
				}
			}
			for (int i = 0; i < Reg.tot; i ++){
				Reg cur = new Reg(i);
				if (Reg.moved(i) && Reg.isCopy(i) && !cur.needStore()){
					for (int j = 0; j < v.size(); j ++)
						v.get(j).replaceReg(cur, Reg.getTarget(i));
					go_on = true;
					System.out.println(cur.num + ", "+ Reg.getTarget(i).num);
					break;
				}
			}*/
			Vector<Instruction> tmp;
			//go_on = true;
			
			//while (go_on){
				tmp = (Vector<Instruction>)v.clone();
				boolean changed = false;
				Instruction pre = null;
				for (int i = 0; i < tmp.size(); i ++){
					tmp.get(i).clear();
					Instruction cur = tmp.get(i);
					if (pre != null) pre.addTo(i);
					if (cur instanceof GotoIns && !((GotoIns)cur).lab.toFunc){
						//System.out.println(i + ", " + ((GotoIns)cur).lab.getNum());
						cur.addTo(((GotoIns)cur).lab.getNum());
					}
				//	if (cur instanceof Branch)
				//		System.out.println(((Branch)cur).op);
					if (cur instanceof Branch && !((Branch)cur).lab.toFunc)
						cur.addTo(((Branch)cur).lab.getNum());
					pre = cur;
				}
				changed = true;
				while (changed){
					changed = false;
					for (int i = 0; i < tmp.size(); i ++){
						for (int j = 0; j < tmp.get(i).to.size(); j ++){
							int cur = tmp.get(i).to.get(j);
							changed |= tmp.get(i).outLiveness(tmp.get(cur));
						}
						changed |= tmp.get(i).inLiveness();
					}
				} 
				for (int i = 0; i < Reg.tot; i ++){
					Reg cur = new Reg(i);			
					for (int j = 0; j < tmp.size(); j ++)	
						if (tmp.get(j).inContain(cur)){
							cur.start = j - 1; 
							if (j == 0) cur.start = 0;
							break;
						}
					for (int j = tmp.size() - 1; j >= 0; j --)
						if (tmp.get(j).inContain(cur)){
							cur.end = j; break;
						}
					if (cur.end == -1){
						cur.start = tmp.size();
					}
					cur.setEnd();
					cur.setStart();
					q[i] = cur;	
				}  
				go_on = false;
				v = new Vector<Instruction>();
				for (int i = 0; i < tmp.size(); i ++){
					//v.add(tmp.get(i));
					if (tmp.get(i).alive()){
						v.add(tmp.get(i));
						if (tmp.get(i) instanceof LabelIt){
							((LabelIt)tmp.get(i)).lab.changeNum(v.size() - 1);
						}
					}else go_on = true;
				}
				// go_on = false;
			}
			ins.put(f, v);
			Reg[] r = new Reg[20];
			Arrays.sort(q, new Comparator<Reg>() {
				@Override
				public int compare(Reg o1, Reg o2) {
					boolean flag = (o1.start == o2.start);
					if (flag == true){
						if (o1.end == o2.end)
							if (o1.num < o2.num) return -1;
									else return 1;
						if (o1.end < o2.end) return 1;
						else return -1;
					}else if (o1.start < o2.start) return -1;
					else return 1;
				}
			});
			int k = 0;
			for (int i = 0; i < v.size(); i ++){
				if (!f.cur.name.equals(Symbol.symbol("glb"))
						&& i == 0) v.get(0).toASM(a);
				while (RAswitch && k < Reg.tot && q[k].start <= q[k].end
						&& q[k].start >= i){
					int x = -1;
					if (!q[k].getMem().regisible()){
						k ++; continue;
					}
					for (int j = 0; j < 19; j ++){
						if (r[j] == null || r[j].getEnd() < i){
							x = j; break;
						}	
					}  	
					if (x != -1){
						r[x] = q[k]; 
						Register R = new Register(x + 8); 
						q[k].addRegister(R);
						//System.out.println(f.cur.name + ":" + R.name + " is for " + q[k].num
						//		 + ":" + q[k].start + " to " + q[k].end + ", " + q[k].getMem().regisible());
						
						if (q[k].needStore()){	
							Load(x + 8 - 27, q[k], a);
						}
						k ++;
					}else{
						break;
					}
				}
				if (i == 0){
					if (f.cur.name.equals(Symbol.symbol("glb")))
							if (v.get(i).alive()) v.get(i).toASM(a);
				}else if (v.get(i).alive()){
					if (v.get(i).outFunc() && RAswitch &&
							!v.get(i).isCheck()){
						for (int j = 0; j < 19; j ++){
							
								if (r[j] != null && r[j].getEnd() >= i
									&& r[j].getStart() <= i)
								store(new Register(j + 8), r[j], a);
						} 
					}
					if (v.get(i) instanceof OutFunc){
						for (int j = 0; j < 19; j ++){
							if (r[j] != null && r[j].needStore()){
								store(new Register(j + 8), r[j], a);
							}	
						} 			
					}
					//a.add(i + ":::" + v.get(i).to.size());
					v.get(i).toASM(a);	
					if (v.get(i).outFunc() && RAswitch){
						for (int j = 0; j < 19; j ++){
							if (v.get(i).isCheck() && r[j].isglb())
								Load(j + 8 - 27, r[j], a);
							if (!v.get(i).isCheck())
							if (r[j] != null && r[j].getEnd() >= i
									&& r[j].getStart() <= i){
								Load(j + 8 - 27, r[j], a);
							}		
						} 
					}
				}
			}
			if (f.cur.name.equals(Symbol.symbol("glb"))){
				a.add("li $v0, 10");
				a.add("syscall");;
			}
		}
		a.add(".data 0x10000000");
		a.add("printf_buf: .space 2");
		if (data.size() != 0){
			for (int i = 0; i < data.size(); i ++){
			//	if (!Data.isUsed(i)) continue;
				a.add("Data" + i + ": "); 
				a.add(".asciiz "+ data.get(i));
			}
		}
		a.add(".align 4");
		a.add("GLOBAL:");
	} 

	public void add(String s){
		Func f = new Func(Symbol.symbol("lib"));
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		e.add(new Lib(s));
		ins.put(f, e);
	}
	public void out(){
		a.out();
	}
	public void appendPrintf(Data data, Vector<Reg> v, Func f){
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		if (data == null) e.add(new Printf(null, v));
		else e.add(new Printf(data.ctx, v));
		ins.put(f, e);
	}
	public void appendMalloc(MemLoc res, Reg v, Func f){
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		e.add(new Malloc(v));
		e.add(new MovV0(res.reg));
		ins.put(f, e);
	}
	public void outFunc(Func f){
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		e.add(new OutFunc(f));
		ins.put(f, e);
	}
	public void InFunc(Func f){
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		e.add(new InFunc(f));
		ins.put(f, e);
	}
	public void appendLoad(MemLoc des, MemLoc from, Func f){
		Instruction i;
		Vector<Instruction> e = ins.get(f);
		i = new Lw(des.reg, from);
		if (e == null) e = new Vector<Instruction>();
		e.add(i);
		//if (!des.regisible()) appendST(des, des.reg, f);
	}
	public void appendMemST(MemLoc to, Reg from, Func f){
		Instruction i;
		if (to.pointer == null)
			i = new StoreLoc(from, to);
		else{
			//if (from == null) System.out.println("WTF");
			i = new StorePointer(from, to.pointer, to.offset);
		}
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		e.add(i);
		ins.put(f, e);
	}
	public void appendData(MemLoc des, String ctx, Func f){
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		e.add(new DataAssign(des.reg, data.size()));
		ins.put(f, e);
		data.add(ctx);	
		//System.out.println("data:" + des.reg.num);
		//if (!des.regisible()){
			//System.out.println("WTF");
			//appendST(des, des.reg, f);
		//}
	}
	public void appendReturn(Func f){
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		e.add(new Return());
		ins.put(f, e);
	}
	public void appendST(MemLoc des, Reg s, Func f){
		Instruction i;
		if (des.pointer == null)
			i = new StoreLoc(s, des);
		else{
			//if (s == null) System.out.println("WTF");
			i = new StorePointer(s, des.pointer, des.offset);
		}
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		e.add(i);
		ins.put(f, e);
	}
	public void assignMem(MemLoc des, MemLoc s, Func f){
		Instruction i;
		if (s.pointer == null)
			i = new assignMemLoc(des.reg, s);
		else i = new assignMemPointer(des.reg, s.pointer, s.offset); 
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		e.add(i);
		ins.put(f, e);
		//if (!des.regisible()) appendST(des, des.reg, f);
	}
	public void assignLa(MemLoc des, MemLoc m, Func f){
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		if (m.pointer != null)
			e.add(new CalcI("addiu", des.reg, m.pointer, m.offset));
		else e.add(new la(des.reg, m));
		ins.put(f, e);
		//if (!des.regisible()) appendST(des, des.reg, f);
	}
	public void appendArithmetic(MemLoc des,Reg l, String op, Reg r, Func f){
		Instruction i = new Instruction();
		if (op.equals("*"))
			i = new Calc("mul ", des.reg, l, r);	
		if (op.equals("/"))
			i = new Calc("divu ", des.reg, l, r);
		if (op.equals("%"))
			i = new Calc("rem ", des.reg, l, r);
		if (op.equals("+"))
			i = new Calc("add ", des.reg, l, r);
		if (op.equals("-"))
			i = new Calc("sub ", des.reg, l, r);
		if (op.equals("<<"))
			i = new Calc("sllv ", des.reg, l, r);
		if (op.equals(">>"))
			i = new Calc("srlv ", des.reg, l, r);
		if (op.equals(">"))
			i = new Calc("sgt ", des.reg, l, r);
		if (op.equals("<"))
			i = new Calc("slt ", des.reg, l, r);
		if (op.equals("<=")) 
			i = new Calc("sge ", des.reg, r, l);
		if (op.equals(">="))
			i = new Calc("sle ", des.reg, r, l);
		if (op.equals("!="))
			i = new Calc("sne ", des.reg, r, l);
		if (op.equals("=="))
			i = new Calc("seq ", des.reg, r, l);	
		if (op.equals("&"))	
			i = new Calc("and ", des.reg, l, r);
		if (op.equals("^"))
			i = new Calc("xor ", des.reg, l, r);
		if (op.equals("|"))
			i = new Calc("or ", des.reg, l, r);
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		e.add(i);
		ins.put(f, e);
		//if (!des.regisible()) appendST(des, des.reg, f);
	}	
	public void appendCalcI(MemLoc des,Reg l, String op, int r, Func f){
		Instruction i = new Instruction();
		if (op.equals("*"))
			i = new CalcI("mul ", des.reg, l, r);	
		if (op.equals("/"))
			i = new CalcI("divu ", des.reg, l, r);
		if (op.equals("%"))
			i = new CalcI("rem ", des.reg, l, r);
		if (op.equals("+"))
			i = new CalcI("add ", des.reg, l, r);
		if (op.equals("-"))
			i = new CalcI("sub ", des.reg, l, r);
		if (op.equals("<<"))
			i = new CalcI("sllv ", des.reg, l, r);
		if (op.equals(">>"))
			i = new CalcI("srlv ", des.reg, l, r);
		if (op.equals(">"))
			i = new CalcI("sgt ", des.reg, l, r);
		if (op.equals("<"))
			i = new CalcI("slt ", des.reg, l, r);
		if (op.equals("<=")) 
			i = new CalcI("sle ", des.reg, l, r);
		if (op.equals(">="))
			i = new CalcI("sge ", des.reg, l, r);
		if (op.equals("!="))
			i = new CalcI("sne ", des.reg, l, r);
		if (op.equals("=="))
			i = new CalcI("seq ", des.reg, l, r);	
		if (op.equals("&"))	
			i = new CalcI("and ", des.reg, l, r);
		if (op.equals("^"))
			i = new CalcI("xor ", des.reg, l, r);
		if (op.equals("|"))
			i = new CalcI("or ", des.reg, l, r);
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		e.add(i);
		ins.put(f, e);
		//if (!des.regisible()) appendST(des, des.reg, f);
	}	
	public Tag appendBlez(Reg rs, Func f){
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		Tag res = new Tag(e.size());	
		e.add(new Branch("blez ", rs, null));
		ins.put(f, e);
		return res;
	}
	public Tag appendBgz(Reg rs, Func f){
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		Tag res = new Tag(e.size());
		e.add(new Branch("bgz ", rs, null));
		ins.put(f, e);
		return res;
	}
	public void Assign(MemLoc des, Reg s, Func f){
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		e.add(new Mov(des.reg, s));	
		ins.put(f, e);
		//if (!des.regisible()){
		//	appendST(des, des.reg, f);
		//}
	}
	public void MOVI(MemLoc des, int s, Func f){
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		//System.out.println(des.reg.num + ", " + s);
		e.add(new MOVI(des.reg, s));	
		ins.put(f, e);
		//if (!des.regisible()) appendST(des, des.reg, f);
	}
	public void appendMULI(MemLoc des, Reg s, int t, Func f){
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		e.add(new CalcI("mul ", des.reg, s, t));	
		ins.put(f, e);
		//if (!des.regisible()) appendST(des, des.reg, f);
	}
	public void appendADDI(MemLoc des, Reg s, int t, Func f){
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		e.add(new CalcI("addiu ", des.reg, s, t));	
		ins.put(f, e);
		//if (!des.regisible()) appendST(des, des.reg, f);
	}
	public void appendSUBI(MemLoc des, Reg s, int t, Func f){
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		e.add(new CalcI("subu ", des.reg, s, t));	
		ins.put(f, e);
		//if (!des.regisible()) appendST(des, des.reg, f);
	}
	public void appendSingleOp(MemLoc des, String op, Reg t, Func f){
		Instruction i = new Instruction(); 
		if (op.equals("+"))
			i = new Mov(des.reg, t);
		if (op.equals("-"))
			i = new SingleOp("neg ", des.reg, t);
		if (op.equals("~")) 
			i = new SingleOp("not ", des.reg, t);
		if (op.equals("!")){
			i = new Sgt(des.reg, t);
		}
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		e.add(i);
		ins.put(f, e);
		//if (!des.regisible()) appendST(des, des.reg, f);
	}
	public void appendGoto(Label lab, Func f){
		Instruction g = new GotoIns("j ", lab);	
		lab.called = true; 
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		e.add(g);
		ins.put(f, e);
	}
	public Tag appendUnknownGoto(Func f){
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		Tag res = new Tag(e.size());		
		e.add(new GotoIns("j "));
		ins.put(f, e);
		return res;
	} 
	public Label appendLabel(String N, Func f){

		Vector<Instruction> e = ins.get(f);
		Label lab;
		if (e == null){
			e = new Vector<Instruction>();
		}
		lab = new Label(N, e.size());
		Instruction i = new LabelIt(lab);
		e.add(i);
		ins.put(f, e);
		return lab;
	} 
	public Tag appendBNEZ(MemLoc des, Reg l, String op, Reg r, Func f){
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		
		String cur_op = "bnez ";
		boolean flag = false;
		if (op.equals("")) cur_op = "bnez ";
		if (op.equals("*")) {cur_op = "mul "; flag = true;}
		if (op.equals("/")){cur_op = "divu "; flag = true;}
		if (op.equals("%")){cur_op = "rem "; flag = true;}
		if (op.equals("+")){cur_op = "add "; flag = true;}
		if (op.equals("-")){cur_op = "sub "; flag = true;}
		if (op.equals("<<")){cur_op = "sllv "; flag = true;}
		if (op.equals(">>")){cur_op = "srlv "; flag = true;}
		if (op.equals(">"))cur_op = "bgt ";
		if (op.equals("<"))cur_op = "blt ";
		if (op.equals("<="))cur_op = "ble ";
		if (op.equals(">="))cur_op = "bge ";
		if (op.equals("!="))cur_op = "bne ";
		if (op.equals("=="))cur_op = "beq ";
		if (op.equals("&")){cur_op = "and "; flag = true;}
		if (op.equals("^")){cur_op = "xor "; flag = true;}
		if (op.equals("|")){cur_op = "or "; flag = true;}
		
		if (!flag) e.add(new Branch(cur_op, l, r)); 
			else{
				e.add(new Calc(cur_op, des.reg, l, r));
				e.add(new Branch("bnez ", des.reg, null));
			}
		Tag res = new Tag(e.size() - 1);
		ins.put(f, e);
		return res;
	} 
	public Tag appendBEZ(MemLoc des, Reg l, String op, Reg r, Func f){
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		
		String cur_op = "beqz ";
		boolean flag = false;
		if (op.equals("")) cur_op = "beqz ";
		if (op.equals("*")) {cur_op = "mul "; flag = true;}
		if (op.equals("/")){cur_op = "divu "; flag = true;}
		if (op.equals("%")){cur_op = "rem "; flag = true;}
		if (op.equals("+")){cur_op = "add "; flag = true;}
		if (op.equals("-")){cur_op = "sub "; flag = true;}
		if (op.equals("<<")){cur_op = "sllv "; flag = true;}
		if (op.equals(">>")){cur_op = "srlv "; flag = true;}
		if (op.equals(">"))cur_op = "ble ";
		if (op.equals("<"))cur_op = "bge ";
		if (op.equals("<="))cur_op = "bgt ";
		if (op.equals(">="))cur_op = "blt ";
		if (op.equals("!="))cur_op = "beq ";
		if (op.equals("=="))cur_op = "bne ";
		if (op.equals("&")){cur_op = "and "; flag = true;}
		if (op.equals("^")){cur_op = "xor "; flag = true;}
		if (op.equals("|")){cur_op = "or "; flag = true;}
		
		if (!flag) e.add(new Branch(cur_op, l, r)); 
			else{
				e.add(new Calc(cur_op, des.reg, l, r));
				e.add(new Branch("beqz ", des.reg, null));
			}
		Tag res = new Tag(e.size() - 1);
		ins.put(f, e);
		return res;
	} 

	public void fillGoto(Label lab, Tag t, Func f){
		Vector<Instruction> e = ins.get(f);
		e.get(t.num).fillLabel(lab);
		lab.called = true;
		ins.put(f, e);
	}
	public Tag appendBNEZ(Reg key, String op, Reg reg, Func f){
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		Tag res = new Tag(e.size());
		e.add(new Branch("bnez", key, null));	
		ins.put(f, e);
		return res;
	} 
	public void appendJAL(Label lab, Func f){
		Vector<Instruction> e = ins.get(f);
		if (e == null){
			e = new Vector<Instruction>(); 
		}
		Instruction g = new GotoIns("jal ", lab);
		e.add(g);
		lab.called = true;
		ins.put(f, e);
	} 
	
	private class LivenessCheck{
		BitSet v;
		public LivenessCheck(){
			v = new BitSet(Reg.tot);
		}
		public LivenessCheck(LivenessCheck p){
			v = (BitSet)(p.v).clone();
		}
		public void clear(){v = new BitSet(Reg.tot);}
		public boolean contains(Reg cur){
			return v.get(cur.num);
		}
		public boolean or(LivenessCheck ob){
			BitSet prev = (BitSet)v.clone();
			v.or(ob.v);
			//if (!(v.equals(prev))) System.out.println(prev + "," + v);
			return !(v.equals(prev));
		}
		public void set(Reg x, boolean val){
			v.set(x.num, val);
			Reg p = x.getMem().pointer;
			while (p != null){
				v.set(p.num, true);
				p = p.getMem().pointer;
			}
		}
		public boolean set(Reg x){
			boolean res = v.get(x.num);
			if (res) return false;
			Reg p = x.getMem().pointer;
			v.set(x.num, true);
			//int y = 0;
			while (p != null){
				v.set(p.num, true);
			//	System.out.println(p.num);
				p = p.getMem().pointer;
				//y ++;
				//System.out.println(y);
			}
			//System.out.println(x.num);
			return true;
		}
		//public Reg get(int x){return v.get(x);}
	}

	private class Instruction{ 	
		Vector<Integer> to;
		LivenessCheck in, out;
		public void fillLabel(Label lab){};
		public void clear(){
			to.clear();
			in.clear(); out.clear();
		}
		public boolean outFunc() {
			return false;
		}
		public boolean outContain(Reg reg) {
			return out.contains(reg);
		}
		public boolean isCheck(){return false;}
		public boolean inContain(Reg cur) {
			return in.contains(cur);
		}
		public Reg changed(){return null;}
		public void replaceReg(Reg prev, Reg cur){}
		public void toASM(ASM a){};
		public void addTo(int ins){to.add(ins);}
		public boolean alive(){return true;}
		public boolean outLiveness(Instruction ins){
			boolean res = false;  
			res |= out.or(ins.in);
			return res;
		}
		public boolean inLiveness(){
			boolean res = false;
			res |= in.or(out);
			return res;
		}
	}

	private class GotoIns extends Instruction{
		Label lab;
		String op;
		public GotoIns(String _op){
			lab = null;	
			op = _op;
			to = new Vector<Integer>();
			in = new LivenessCheck(); out = new LivenessCheck();
		}
		public GotoIns(String _op, Label _lab){
			lab = _lab;	
			op = _op;
			to = new Vector<Integer>();
			in = new LivenessCheck(); out = new LivenessCheck();
		}
		@Override
			public void fillLabel(Label _lab){
				lab = _lab;	
			}
		@Override
			public boolean outFunc(){
				if (op.equals("jal ")) return true;
				return false;
			}
		@Override
			public boolean isCheck(){
				if (op.equals("jal ") &&
					lab.ctx.equals("mycheck")) return true;
				return false;
			}
		@Override
			public void toASM(ASM a){
				a.add(op + " " + lab.ctx);
			}
		//goto lab
	}

	private class LabelIt extends Instruction{
		Label lab;
		public LabelIt(Label _lab){
			lab = _lab;	
			to = new Vector<Integer>();
			in = new LivenessCheck(); out = new LivenessCheck();
		}
		@Override
		public boolean alive(){return lab.called;}
		@Override
			public void toASM(ASM a){
				a.add(lab.ctx + ":");
			}
		//lab:
	} 

	private class Branch extends Instruction{
		Reg l, r;
		Label lab;
		String op;
		public Branch(String o, Reg _l, Reg _r){
			l = _l; r = _r;
			lab = null;
			op = o;
			to = new Vector<Integer>();
			in = new LivenessCheck(); out = new LivenessCheck();
		}
		@Override
			public void fillLabel(Label _lab){
				lab = _lab;	
			}
		@Override
			public boolean inLiveness(){
				boolean res = false;  
				res |= in.or(out);
				res |= in.set(l);
				if (r != null) res |= in.set(r);
				return res;
			}
		@Override
			public void toASM(ASM a){
				Register tmpl = l.getRegister();
				if (tmpl == null) tmpl = Load(0, l, a); 
				if (r == null)
					a.add(op + " "+ tmpl.name + "," + lab.ctx);
				else{
					Register tmpr = r.getRegister();
					if (tmpr == null) tmpr = Load(1, r, a); 
					a.add(op + " "+ tmpl.name + ", " + tmpr.name + ", " + lab.ctx);
				}
			} 


		//op cur, lab
	}

	private class CalcI extends Instruction{
		Reg des, rs;
		int t;
		String op;
		public CalcI(String o, Reg d, Reg r, int _t){
			des = d; rs = r; t = _t; op = o;
			to = new Vector<Integer>();
			in = new LivenessCheck(); out = new LivenessCheck();
		}
		@Override
		public Reg changed(){
			return des;
		}
		@Override
		public void replaceReg(Reg prev, Reg tmp){
			if (des.equals(prev)) des = tmp;
			if (rs.equals(prev)) rs = tmp;
		}
		//op des, rs, t
		@Override
			public boolean alive(){
				if (!out.contains(des) && !des.needStore())
					return false;
				return true;
			}
		@Override
			public boolean inLiveness(){
				boolean res = false; 
				LivenessCheck tmp = new LivenessCheck(out);
				tmp.set(des, false);
				res |= in.or(tmp);
				res |= in.set(rs);
				return res;
			}
		@Override
			public void toASM(ASM a){
				Register d = des.getRegister(),
						 s = rs.getRegister();
				boolean flag = false;
				if (d == null){
					d = new Register(27);
					flag = true;
				}
				if (s == null) s = Load(1, rs, a);
				a.add(op + " " + d.name + "," + s.name + "," + t);
				if (flag || des.regisible()) store(d, des, a);
			} 

	}

	private class Mov extends Instruction{
		Reg des, s;
		public Mov(Reg d, Reg rs){
			des = d; s = rs;	
			to = new Vector<Integer>();
			in = new LivenessCheck(); out = new LivenessCheck();
		}
		@Override
		public Reg changed(){
			return des;
		}
		@Override
		public void replaceReg(Reg prev, Reg tmp){
			if (des.equals(prev)) des = tmp;
			if (s.equals(prev)) s = tmp;
		}
		@Override
			public boolean alive(){
				if (!out.contains(des) && !des.needStore())
					return false;
				return true;
			}
		@Override
			public boolean inLiveness(){
				boolean res = false;  
				LivenessCheck tmp = new LivenessCheck(out);
				tmp.set(des, false);
				res |= in.or(tmp);
				res |= in.set(s);
				
				return res;
			}
		@Override
			public void toASM(ASM a){
				Register d = des.getRegister(),
						 rs = s.getRegister();
				boolean flag = false;
				if (d == null){
					d = new Register(27);
					flag = true;
				}
				if (rs == null) rs = Load(1, s, a);
				a.add("move " + d.name + "," + rs.name);
				if (flag || des.regisible()) store(d, des, a);
			} 

		//move des, s
	}

	private class MOVI extends Instruction{
		Reg des;
		int s;
		public MOVI(Reg d, int rs){
			des = d; s = rs;	
			to = new Vector<Integer>();
			in = new LivenessCheck(); out = new LivenessCheck();
		}
		@Override
		public Reg changed(){
			return des;
		}
		@Override
		public void replaceReg(Reg prev, Reg tmp){
			if (des.equals(prev)) des = tmp;
		}
		@Override
			public boolean alive(){
				if (!out.contains(des) && !des.needStore())
					return false;
				return true;
			}
		@Override
			public boolean inLiveness(){
				boolean res = false; 
				LivenessCheck tmp = new LivenessCheck(out);
				tmp.set(des, false);
				res |= in.or(tmp);
				return res;
			}
		@Override
			public void toASM(ASM a){
				Register d = des.getRegister();
				boolean flag = false;
				if (d == null){
					d = new Register(27);
					flag = true;
				}
				a.add("li " + d.name + "," + s);
				if (flag || des.regisible()) store(d, des, a);
			} 
		//li des, s
	}

	private class SingleOp extends Instruction{
		Reg des, s;
		String Op;
		public SingleOp(String op,  Reg d, Reg rs){
			des = d; s = rs;
			Op = op;
			to = new Vector<Integer>();
			in = new LivenessCheck(); out = new LivenessCheck();
		}
		@Override
		public Reg changed(){
			return des;
		}
		@Override
		public void replaceReg(Reg prev, Reg tmp){
			if (des.equals(prev)) des = tmp;
			if (s.equals(prev)) s = tmp;
		}
		@Override
			public boolean alive(){
				if (!out.contains(des) && !des.needStore())
					return false;
				return true;
			}
		//op des, s
		@Override
			public boolean inLiveness(){
				boolean res = false;  
				LivenessCheck tmp = new LivenessCheck(out);
				tmp.set(des, false);
				res |= in.or(tmp);
				res |= in.set(s);
				return res;
			}
		@Override
			public void toASM(ASM a){
				Register d = des.getRegister(),
						 rs = s.getRegister();	
				boolean flag = false;
				if (d == null){
					d = new Register(27); 
					flag = true;
				}
				if (rs == null) rs = Load(1, s, a);
				a.add(Op + " " + d.name + ", " + rs.name);
				if (flag || des.regisible()) store(d, des, a);
			} 
	}

	private class Calc extends Instruction{
		Reg des, s, t;
		String op;
		public Calc(String o, Reg d, Reg rs, Reg rt){
			des = d; s = rs; t = rt;	
			op = o;
			to = new Vector<Integer>();
			in = new LivenessCheck(); out = new LivenessCheck();
		}
		@Override
		public Reg changed(){
			return des;
		}
		@Override
		public void replaceReg(Reg prev, Reg tmp){
			if (des.equals(prev)) des = tmp;
			if (s.equals(prev)) s = tmp;
			if (t.equals(prev)) t = tmp;
		}
		@Override
			public boolean alive(){
				if (!out.contains(des) && !des.needStore())
					return false;
				return true;
			}
		//op des, s, t
		@Override
			public boolean inLiveness(){
				boolean res = false; 
				LivenessCheck tmp = new LivenessCheck(out);
				tmp.set(des, false);
				res |= in.or(tmp);
				res |= in.set(s);
				res |= in.set(t);
				return res;
			}
		@Override
			public void toASM(ASM a){
				Register d = des.getRegister(),
						 rs = s.getRegister(), 
						 rt = t.getRegister();	
				boolean flag = false;
				if (d == null){
					d = new Register(27);
					flag = true;
				}
				if (rs == null) rs = Load(0, s, a);
				if (rt == null) rt = Load(1, t, a);
				a.add(op + " " + d.name + ", " + rs.name + ", " + rt.name);
				if (flag || des.regisible()) store(d, des, a);
			}

	}

	private class Return extends Instruction{
		public Return(){
			to = new Vector<Integer>();
			in = new LivenessCheck(); out = new LivenessCheck();
		}
		@Override
			public void toASM(ASM a){
				a.add("jr $ra");
			}
		//jr $ra
	}

	private class assignMemLoc extends Instruction{
		Reg des; 
		MemLoc t;
		public assignMemLoc(Reg d, MemLoc s){
			des = d; t = s;
			to = new Vector<Integer>();
			in = new LivenessCheck(); out = new LivenessCheck();
		}
		@Override
		public Reg changed(){
			return des;
		}
		@Override
		public void replaceReg(Reg prev, Reg tmp){
			if (des.equals(prev)) des = tmp;
		}
		@Override
			public boolean alive(){
				if (!out.contains(des) && !des.needStore())
					return false;
				return true;
			}
		@Override
		public boolean inLiveness(){
			boolean res = false;  
			LivenessCheck tmp = new LivenessCheck(out);
			tmp.set(des, false);
			res |= in.or(tmp);
			return res;
		}
		@Override
			public void toASM(ASM a){
				Register d = des.getRegister();
				boolean flag = false;
				if (d == null){
					d = new Register(27);
					flag = true;
				}
				a.add("la " + d.name + ", " + t.name);
				if (flag || des.regisible()) store(d, des, a);
			} 

		//la des, t
	}

	private class assignMemPointer extends Instruction{
		Reg des;
		Reg pointer;
		int offset;		
		public assignMemPointer (Reg d, Reg p, int o){
			des = d; pointer = p; offset = o;
			to = new Vector<Integer>();
			in = new LivenessCheck(); out = new LivenessCheck();
		}
		@Override
		public Reg changed(){
			return des;
		}
		@Override
		public void replaceReg(Reg prev, Reg tmp){
			if (des.equals(prev)) des = tmp;
			if (pointer.equals(prev)) pointer = tmp;
		}
		@Override
			public boolean alive(){
				if (!out.contains(des) && !des.needStore())
					return false;
				return true;
			}
		@Override
			public boolean inLiveness(){
				boolean res = false;  
				LivenessCheck tmp = new LivenessCheck(out);
				tmp.set(des, false);
				res |= in.or(tmp);
				res |= in.set(pointer);
				return res;
			}
		@Override
			public void toASM(ASM a){
				Register d = des.getRegister(),
						 rs = pointer.getRegister();	
				boolean flag = false;
				if (d == null){
					d = new Register(27);
					flag = true;
				}
				if (rs == null) rs = Load(1, pointer, a);
				a.add("la " + d.name + ", " + offset +"(" + rs.name + ")");
				if (flag || des.regisible()) store(d, des, a);
			} 

		//la des, offset(pointer)
	}
	
	private class Lw extends Instruction{
	   Reg des;	
	   MemLoc from;
	   public Lw(Reg d, MemLoc f){
		   des = d; from = f;	
		   to = new Vector<Integer>();
		   in = new LivenessCheck(); out = new LivenessCheck();
	   }
		@Override
		public Reg changed(){
			return des;
		}
		@Override
		public void replaceReg(Reg prev, Reg tmp){
			if (des.equals(prev)) des = tmp;
		}
	   @Override
	   public boolean inLiveness(){
		   boolean res = false;   
			LivenessCheck tmp = new LivenessCheck(out);
			tmp.set(des, false);
			res |= in.or(tmp);
		   return res;
	   }
	   @Override
		public boolean alive(){
			if (!out.contains(des) && !des.needStore())
				return false;
			return true;
		} 
	   @Override
	   public void toASM(ASM a){
		   Register d = des.getRegister();	
		   boolean flag = false;
		   if (d == null){d = new Register(27);flag = true;}
		   a.add("lw " + d.name + ", " + from.name);
		   if (flag || des.regisible()) store(d, des, a);
	   } 
//mem[to] = from;
}

private class InFunc extends Instruction{
	Func  f;
	public InFunc(Func _f){
		f = _f;
		to = new Vector<Integer>();
		in = new LivenessCheck(); out = new LivenessCheck();
	}
	@Override
		public void toASM(ASM a){
			a.add("subu $sp, $sp, " + (Mem.getSize(f) + 4)); 
			a.add("sw $ra, " + Mem.getSize(f) + "($sp)");
		}
}

private class Sgt extends Instruction{
	Reg des, t;
	public Sgt(Reg d, Reg _t){
		des = d; t = _t;
		to = new Vector<Integer>();
		in = new LivenessCheck(); out = new LivenessCheck();
	}
	@Override
	public Reg changed(){
		return des;
	}
	@Override
	public void replaceReg(Reg prev, Reg tmp){
		if (des.equals(prev)) des = tmp;
		if (t.equals(prev)) t = tmp;
	}
	@Override
		public boolean alive(){
			if (!out.contains(des) && !des.needStore())
				return false;
			return true;
		}
	@Override
		public boolean inLiveness(){
			boolean res = false;  
			LivenessCheck tmp = new LivenessCheck(out);
			tmp.set(des, false);
			res |= in.or(tmp);
			res |= in.set(t);
			return res;
		}
	@Override
		public void toASM(ASM a){
			Register d = des.getRegister(),
					 rs = t.getRegister();	
			boolean flag = false;
			if (d == null){
				d = new Register(27);
				flag = true;
			}
			if (rs == null) rs = Load(1, t, a);
			a.add("seq " + d.name + ", " + rs.name + ",0");
			if (flag || des.regisible()) store(d, des, a);
		} 
	//seq des, t, 0
}

private class OutFunc extends Instruction{
	Func f;
	public OutFunc(Func _f){
		f = _f;
		to = new Vector<Integer>();
		in = new LivenessCheck(); out = new LivenessCheck();
	}
//	@Override
//		public boolean outFunc(){return true;}
	@Override
		public void toASM(ASM a){
			a.add("lw $ra, " + Mem.getSize(f) + "($sp)");
			a.add("addu $sp, $sp, " + (Mem.getSize(f) + 4)); 
		}
}
private class Printf extends Instruction{
	String d;
	Vector<Reg> v;
	public Printf(String _d, Vector<Reg> _v){
		v = _v;
		d = _d;
		to = new Vector<Integer>();
		in = new LivenessCheck(); out = new LivenessCheck();
	}

	@Override
	public void replaceReg(Reg prev, Reg tmp){
		for (int i = 0; i < v.size(); i ++){
			if (v.get(i).equals(prev)) 
				v.set(i, tmp);
		}
	}
	@Override
		public boolean inLiveness(){
			boolean res = false;  
			res |= in.or(out);	
			for (int i = 0; i < v.size(); i ++){
				Reg cur = v.get(i);
				res |= in.set(cur);
			}
			return res;
		}
	//@Override
	//public boolean outFunc(){return true;}
	@Override
		public void toASM(ASM a){

			if (d == null){
				a.add("la $a0, Data0");
				a.add("li $a1, 10");
				a.add("li $a2, 34");
				a.add("la $a3, Data0");
				a.add("li $v0, 34");
				a.add("li $v1, 10");
				a.add("jal printf");
				Data.use(0);
				return;
			}
			if (d != null){
				int l = d.length();
				String tmp = "";
				int all = 0;
				for (int i = 1; i < l - 1; i ++){
					if (d.charAt(i) =='%'){
						if (!tmp.equals("")){
							data.add("\"" + tmp + "\"");
							Data.use(Data.tot);
							a.add("la $a0, Data" + Data.tot);
							a.add("li $v0, 4");
							a.add("syscall");
							Data.tot ++;
							tmp = "";
						} 
						Register reg = v.get(all).getRegister();
						if (reg == null) reg = Load(0, v.get(all), a); 
						all ++;
						if (d.charAt(i + 1) == 'd'){
							a.add("move $a0, " + reg.name);
							a.add("li $v0, 1");
							a.add("syscall");
							i ++;
						}else if (d.charAt(i + 1) == 'c'){
							a.add("move $a0, " + reg.name);
							a.add("li $v0, 11");
							a.add("syscall");
							i ++;
						}else{
							a.add("move $a1, " + reg.name);
							data.add("\"%04d\"");
							Data.use(Data.tot);
							a.add("la $a0, Data" + Data.tot);
							Data.tot ++;
							a.add("jal printf");
							i += 3;
						}
						continue;
					}
					tmp = tmp + d.charAt(i);
					
				}
				if (!tmp.equals("")){
					data.add("\"" + tmp + "\"");
					Data.use(Data.tot);
					a.add("la $a0, Data" + Data.tot);
					a.add("li $v0, 4");
					a.add("syscall");
					Data.tot ++;
					tmp = "";
				} 
			}
		/*	for (int i = 0; i < v.size(); i ++){
				Register cur = new Register(num);
				Register tmp = v.get(i).getRegister();
				if (tmp == null) tmp = Load(0, v.get(i), a); 
				a.add("move " + cur.name + ", " + tmp.name); 
				num ++;
				if (num > 7) num = 2;
			}
			//}
			a.add("jal printf");*/
		}
}
private class Malloc extends Instruction{
	Reg para;
	public Malloc(Reg p){
		para = p;
		to = new Vector<Integer>();
		in = new LivenessCheck(); out = new LivenessCheck();
	}
	@Override
	public void replaceReg(Reg prev, Reg tmp){
		if (para.equals(prev)) para = tmp;

	}
	@Override
		public boolean inLiveness(){
			boolean ret = false;  
			//LivenessCheck tmp = new LivenessCheck(out);
			//tmp.set(res.num, false);
			ret |= in.or(out);
			ret |= in.set(para);
			return ret;
		}
	//@Override
	//public boolean outFunc(){return true;}
	@Override
		public void toASM(ASM a){
			Register tmp = para.getRegister();
					//d = res.getRegister();	
			if (tmp == null) tmp = Load(0, para, a);
			a.add("move $a0, " + tmp.name);
			a.add("jal malloc");
			int flag = 0;
			/*if (d == null){
				d = new Register(27);
				flag = 1;
			}
			a.add("move " + d.name + ", $v0");
			if (flag == 1) store(d, res, a);*/
		}

}
private class MovV0 extends Instruction{
	Reg des;
	public MovV0(Reg p){
		des = p;
		to = new Vector<Integer>();
		in = new LivenessCheck(); out = new LivenessCheck();
	}
	@Override
	public Reg changed(){
		return des;
	}
	@Override
	public void replaceReg(Reg prev, Reg tmp){
		if (des.equals(prev)) des = tmp;
	}
	@Override
		public boolean inLiveness(){
			boolean ret = false;  
			LivenessCheck tmp = new LivenessCheck(out);
			tmp.set(des, false);
			ret |= in.or(out);
			return ret;
		}
	@Override
		public void toASM(ASM a){
			Register d = des.getRegister();	
			boolean flag = false;
			if (d == null){
				d = new Register(27);
				flag = true;
			}
			a.add("move " + d.name + ", $v0");
			if (flag || des.regisible()) store(d, des, a);
		}
	
}

private class Lib extends Instruction{
	String s;
	public Lib(String _s){
		s = _s;
		to = new Vector<Integer>();
		in = new LivenessCheck(); out = new LivenessCheck();
	}
	@Override
		public void toASM(ASM a){
			a.add(s);
		}
}

private class DataAssign extends Instruction{
	Reg des;
	int num;
	public DataAssign(Reg _d, int _n){
		des = _d; num = _n;
		to = new Vector<Integer>();
		in = new LivenessCheck(); out = new LivenessCheck();
	}
	@Override
	public Reg changed(){
		return des;
	}
	@Override
	public void replaceReg(Reg prev, Reg tmp){
		if (des.equals(prev)) des = tmp;
	}
	@Override
		public boolean alive(){
			if (!out.contains(des) && !des.needStore())
				return false;
			return true;
		}
	@Override
		public boolean inLiveness(){
			boolean res = false;  
			LivenessCheck tmp = new LivenessCheck(out);
			tmp.set(des, false);
			res |= in.or(tmp);
			return res;
		}
	@Override
		public void toASM(ASM a){
			Register d = des.getRegister();
			boolean flag = false;
			if (d == null){
				d = new Register(27);
				flag = true;
			}
			a.add("la " + d.name + ", " + "Data" + num);
			if (flag || des.regisible()) store(d, des, a);
		}


	//la des, data_num
}

private class la extends Instruction{
	Reg des;
	MemLoc m;
	public la(Reg _d, MemLoc _m){
		des= _d; m = _m;
		to = new Vector<Integer>();
		
		in = new LivenessCheck(); out = new LivenessCheck();
	}
	@Override
	public Reg changed(){
		return des;
	}
	@Override
	public void replaceReg(Reg prev, Reg tmp){
		if (des.equals(prev)) des = tmp;
	}
	@Override
		public boolean inLiveness(){
			boolean res = false;  
			LivenessCheck tmp = new LivenessCheck(out);
			tmp.set(des, false);
			res |= in.or(tmp);
			return res;
		}
	@Override
		public void toASM(ASM a){
			Register d = des.getRegister();
			boolean flag = false;
			if (d == null){
				d = new Register(27);
				flag = true;
			}
			a.add("la " + d.name + ", " + m.name);
			if (flag || des.regisible()) store(d, des, a);
		}
}
private class StorePointer extends Instruction{
	Reg s, pointer;
	int offset;
	public StorePointer(Reg _s, Reg p, int o){
		if (_s == null) System.out.println("WTF");
		s = _s;
		pointer = p; offset = o;	
		to = new Vector<Integer>();
		in = new LivenessCheck(); out = new LivenessCheck();
	}
	@Override
	public Reg changed(){
		return s;
	}
	@Override
	public void replaceReg(Reg prev, Reg tmp){
		if (pointer.equals(prev)) pointer = tmp;
		if (s.equals(prev)) s = tmp;
	}
	@Override
		public boolean inLiveness(){
			boolean res = false;  
			res |= in.or(out);
			//if (in == null) System.out.println("FFF");
			res |= in.set(s);
			res |= in.set(pointer);
			return res;
		}
	@Override
		public void toASM(ASM a){
			Register d = s.getRegister(),
					 rs = pointer.getRegister();	
			if (d == null){
				d = Load(0, s, a); 
			}
			if (rs == null) rs = Load(1, pointer, a);
			a.add("sw " + d.name + ", " + offset + "(" + rs.name + ")");
		}
}
private class StoreLoc extends Instruction{
	Reg d;
	MemLoc des;
	public StoreLoc(Reg _d, MemLoc _des){
		d = _d; des = _des;	
		to = new Vector<Integer>();
		in = new LivenessCheck(); out = new LivenessCheck();
	}
	@Override
	public Reg changed(){
		return d;
	}
	@Override
	public void replaceReg(Reg prev, Reg tmp){
		if (d.equals(prev)) d = tmp;
	}
	@Override
		public boolean inLiveness(){
			boolean res = false;
			res |= in.or(out);
			res |= in.set(d);
			return res;
		}
	@Override
		public void toASM(ASM a){
			Register tmp = d.getRegister();
			if (tmp == null){
				tmp = Load(0, d, a);
			}
			a.add("sw " + tmp.name + ", " + des.name); 
		}
	//sw d, des.name
}
public Register Load(int num, Reg des, ASM a){
	Register res = new Register(num + 27); 
	
	MemLoc d = des.getMem();	
	if (d.pointer != null){
		Register tmp = d.pointer.getRegister(); 
		if (tmp == null) tmp = Load(num, d.pointer, a);
		a.add("lw " + res.name + ", " + d.offset + "(" + tmp.name + ")");
	}else{
		
		//if (num < 0) a.add("lwAAA " + res.name + ", "+  d.name);
		//else 
		a.add("lw " + res.name + ", "+  d.name);
	}
	return res;
}
public void store(Register d, Reg des, ASM a){
	MemLoc tmp = des.getMem();	
	if (tmp.pointer != null){
		Register tt = tmp.pointer.getRegister();
		if (tt == null) tt = Load(1, tmp.pointer, a);
		a.add("sw " + d.name + ", " + tmp.offset + "(" + tt.name + ")");
	}else a.add("sw " + d.name + ", "+  tmp.name);
}

private class Block{
	Vector<Instruction> v;
	public Block(Vector<Instruction> p){
		v = new Vector<Instruction>();
		for (int i =0 ; i < p.size(); i ++)
			v.add(p.get(i));
	}
	public Block(){
		v = new Vector<Instruction>();
	}
	public void add(Instruction i){
		v.add(i);
	}
	public void clean(){
		v = new Vector<Instruction>();
	}
}
}

