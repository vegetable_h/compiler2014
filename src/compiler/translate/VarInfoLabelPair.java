package compiler.translate;

public class VarInfoLabelPair{
	public VarInfo loc;
	public Label lab;
	public VarInfoLabelPair(VarInfo l, Label b){
		loc = l; lab = b;	
	}
}
