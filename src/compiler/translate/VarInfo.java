package compiler.translate; 

import compiler.semantic.Type;

public class VarInfo{
	public Mem loc; 
	public VarInfoLabelPair varLab;
	public Type type;
	public Mem param;
	public Data data;
	public boolean isImme;
	public int val;
	public VarInfo(){
		type = null;
		//loc = new Mem();
		varLab = null;
		param = null;
		isImme = false;
	}
	public String getCtx(){
		return loc.getCtx();
	}
}
