package compiler.translate;

public class Data{
	static int tot = 0;	
	int num;
	String name, ctx;
	static boolean[] used = new boolean[10000];
	public Data(String _ctx){
		num = tot;	
		tot ++;
		name = "Data" + num;
		ctx = _ctx;
		used[num] = false;
	}
	static void use(int n){used[n] = true;}
	static boolean isUsed(int n){return used[n];}
	public String getCtx(){
		return "Data" + num;
	}
}
