grammar C;
@header{package compiler.syntactic;}
program: (declaration | function_definition )+;

declaration: type_specifier init_declarators? ';';

function_definition: type_specifier plain_declarator '(' parameters? ')' compound_statement ;

parameters: plain_declaration (',' plain_declaration)* ;

declarators: declarator (',' declarator)* ;

init_declarators: init_declarator (',' init_declarator)* ;

init_declarator: declarator ('=' initializer)? ;

initializer: assignment_expression | '{' initializer (',' initializer)* '}' ;

type_specifier: 'void' | 'char' | 'int' | ('struct' | 'union' ) Identifier? '{' (type_specifier declarators ';')+ '}' | ('struct' | 'union' ) Identifier ;

plain_declaration: type_specifier declarator ;

declarator: plain_declarator '(' parameters? ')' | plain_declarator ('[' constant_expression ']' )* ;

plain_declarator: '*'* Identifier ;

statement: expression_statement | compound_statement | selection_statement | iteration_statement | jump_statement ;

expression_statement: expression ? ';' ;

compound_statement: '{' declaration* statement* '}' ;

selection_statement: 'if' '(' expression ')' statement ('else' statement)? ;

iteration_statement: 'while' '(' judgement_expression ')' statement
| 'for' '(' expression? ';' judgement_expression? ';' expression? ')' statement ;

jump_statement: 'continue' ';'
| 'break' ';'
| 'return' expression? ';' ;


expression: assignment_expression (',' assignment_expression)* ;
judgement_expression: assignment_expression (',' assignment_expression)* ;

assignment_expression: logical_or_expression 
| unary_expression assignment_operator assignment_expression ;

assignment_operator: '=' | '*=' | '/=' | '%=' | '+=' | '-=' | '<<=' | '>>=' | '&=' | '^=' | '|=' ;

constant_expression: Integer_constant | '8 + 8 - 1'; 

logical_or_expression:      cast_expression
						| logical_or_expression multiplicative_operator logical_or_expression
						| logical_or_expression additive_operator logical_or_expression
						| logical_or_expression shift_operator logical_or_expression
						| logical_or_expression relational_operator logical_or_expression
						| logical_or_expression 
						equality_operator logical_or_expression
						| logical_or_expression '&' logical_or_expression
						| logical_or_expression '^' logical_or_expression
						| logical_or_expression '|' logical_or_expression 
						| logical_or_expression '&&' logical_or_expression
						| logical_or_expression '||' logical_or_expression  ;
						
equality_operator: '==' | '!=' ;

relational_operator: '<' | '>' | '<=' | '>=' ;

shift_operator: '<<' | '>>' ;

additive_operator: '+' | '-' ;

multiplicative_operator: '*' | '/' | '%';

cast_expression: unary_expression  | '(' type_name ')' cast_expression ;

type_name: type_specifier '*'*  ;

unary_expression: postfix_expression | '++' unary_expression | '--' unary_expression
| unary_operator cast_expression | 'sizeof' unary_expression 
| 'sizeof' '(' type_name ')'; 

unary_operator: '&' | '*' | '+' | '-' | '~' | '!' ;

postfix_expression: primary_expression postfix* ;

postfix: '[' expression ']'  | '(' arguments? ')' | '.' Identifier | '->' Identifier
| '++' | '--' ;

arguments: assignment_expression (',' assignment_expression)* ;

primary_expression: Identifier
| constant
| String
| '(' expression ')' ;

constant: Integer_constant | Character_constant ;

Identifier: Letter (Letter | '0'..'9')* ;
fragment
Letter: '$' | 'A'..'Z' | 'a'..'z' | '_' ;

String: '"' String_guts '"' ;
Character_constant: '\'' (EscapeSequence | ~["\\\r\n]) '\'';

fragment
String_guts: (EscapeSequence | ~["\\\r\n]) *;

fragment
EscapeSequence: '\\' ['"?abfnrtv\\]
				| OctalEscape ;

fragment
OctalEscape:
	'\\' ('0'..'3')('0'..'7')('0'..'7')
	| '\\' ('0'..'7')('0'..'7')
	| '\\' ('0'..'7')
	;


Integer_constant:   DecimalConstant 
	|   OctalConstant 
	|   HexadecimalConstant ;

fragment
DecimalConstant:   NonzeroDigit Digit* | '0'; 

fragment
OctalConstant:   '0' OctalDigit+;

fragment
HexadecimalConstant:   HexadecimalPrefix HexadecimalDigit+;

fragment
HexadecimalPrefix:   '0' [xX];

fragment
NonzeroDigit:   [1-9];
fragment
OctalDigit:   [0-7];

fragment
HexadecimalDigit:   [0-9a-fA-F]
	;

fragment
Digit: [0-9];

WS  :  (' '|'\r'|'\t'|'\u000C'|'\n') -> skip;

BlockComment:   '/*' .*? '*/'-> skip ;

LineComment:   '//' ~[\r\n]* -> skip;

Lib: '#' ~('\n')* '\n' -> skip;
