package compiler.main;

import compiler.semantic.Visitor;
import compiler.syntactic.*;
import compiler.translate.IRWriter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import org.antlr.v4.runtime.*; 
import org.antlr.v4.runtime.tree.*;

public class Main {
	private static void compile(String filename) throws IOException {
		InputStream in = new FileInputStream(filename);
	    ANTLRInputStream input = new ANTLRInputStream(in); 
	    
		// create a lexer that feeds off of input CharStream
		CLexer lexer = new CLexer(input); 
		// create a buffer of tokens pulled from the lexer
		CommonTokenStream tokens = new CommonTokenStream(lexer); 
		// create a parser that feeds off the tokens buffer
		CParser parser = new CParser(tokens);
		parser.removeErrorListeners(); // remove ConsoleErrorListener 
		parser.addErrorListener(new VerboseListener()); // add ours
		ParseTree tree = parser.program(); // begin parsing at init rule
		if (VerboseListener.error_cnt > 0){
			//System.out.println("ls da zhu tou");
			System.exit(1);
		}
		Visitor cVis = new Visitor(); 
		IRWriter irw = cVis.visitProgram((CParser.ProgramContext)tree);
		if (irw == null){
			//System.out.println("ls da zhu tou");
			System.exit(1);
		}
		File file = new File("../lib.s");
		BufferedReader reader = null;
		try {
	            reader = new BufferedReader(new FileReader(file));
	            String tempString = null;
	            while ((tempString = reader.readLine()) != null) {
	                irw.add(tempString);
	            }
	            reader.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            if (reader != null) {
	                try {
	                    reader.close();
	                } catch (IOException e1) {
	                }
	            }
	        }
		irw.toASM();
  		irw.out();
	}
	public static void main(String[] args) throws IOException {		
		
		for(String arg : args){
			compile(System.getProperty("user.dir") + "/" + arg);
		}
		//compile("/Users/vegetable_h/compiler2014/data.c");
		//compile("/Users/vegetable_h/compiler2014/try.c");
		//compile("/Users/vegetable_h/compiler2014/testcases/Normal/varptr-5100309127-hetianxing.c");
	}
}
