package compiler.main;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

public class VerboseListener extends BaseErrorListener { 
	static int error_cnt=0;
	@Override
	public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol,
			int line, int charPositionInLine, String msg,
			RecognitionException e){
	
			error_cnt ++;
			System.err.println("line "+line+":"+charPositionInLine+" at "+ offendingSymbol+": "+msg);
	} 
}