package compiler.semantic;

import java.util.Vector;

public class Struct extends Type{
	public Symbol name;
	public Vector<SymbolTypePair> v;
	private boolean valid;
	public Integer cur_size;
	private java.util.Dictionary<Symbol, Type> dict;
	private java.util.Dictionary<Symbol, Integer> size_dict;

	public Struct(){
		valid = true; name = null;
		defined = false; v = new Vector<SymbolTypePair>();
		dict = new java.util.Hashtable<Symbol, Type>();
		size_dict = new java.util.Hashtable<Symbol, Integer>();
		cur_size = 0;
	}
	public Struct(Symbol n){
		name = n; 
		valid = true; v = new Vector<SymbolTypePair>();
		dict = new java.util.Hashtable<Symbol, Type>();
		size_dict = new java.util.Hashtable<Symbol, Integer>();
		defined = false;
		cur_size = 0;
	}
	@Override
	public int size(){
		int res = 0;
		for (int i = 0; i < v.size(); i ++)
			res += v.get(i).type.size();
		return res;
	}
	@Override
	public Type clone(){
		Struct res = new Struct();
		res.name = name;
		res.valid = valid;
		res.defined = defined;
		res.v = v;
		res.dict = dict;
		res.size_dict = size_dict;
		res.cur_size = cur_size;
		res.dict = dict;
		return res;
	}
	public boolean isDefined(){
		if (defined == false) return false;	
		int cnt = v.size();
		for (int i =0 ; i < cnt; i ++)
			if (!v.get(i).type.isDefined()) return false;
		return true;
	}
	@Override
	public boolean convertable(Type typ){
		if (!(typ instanceof Struct))return false;	
		return (((Struct)typ).name == name);
	}
	@Override 
	public boolean equals(Object obj){
		if (!(obj instanceof Struct))return false;	
		return (((Struct)obj).name == name);
	}
	@Override
	public String toString(){
		return "Struct " + name.toString();
	}
	@Override
	public int hashCode(){
		return name.hashCode();	
	}
	@Override
	public boolean isValid(){
		return valid;
	}
	public boolean fieldContained(Symbol cur){
		if (defined == false) return false;
		if (dict.get(cur) == null)	
			return false;
		else return true;
	}
	public Type getFieldType(Symbol cur){
		if (dict.get(cur) == null)return null;	
			else return dict.get(cur);
	}
	public void addField(SymbolTypePair cur){
		//if (!cur.type.isValid()) {valid = false;return ;}
		if (dict.get(cur.symbol) != null){
			valid = false; return ;	
		}
		v.add(cur); dict.put(cur.symbol, cur.type);
		size_dict.put(cur.symbol, cur_size);
		cur_size += cur.type.size();

	}
	public int offset(Symbol cur){
		//if (size_dict.get(cur) == null) System.out.println("WTF");
	//	System.out.println(name + ", " + cur + ":" + size_dict.get(cur));
		return size_dict.get(cur);	
	}
	public void fillField(Type tmp){
		for (int i = 0; i < v.size(); i ++){
			SymbolTypePair cur = v.get(i);
			if (cur.type instanceof Pointer)
				if (((Pointer)cur.type).elementType == null){
					cur.type = new Pointer(tmp);
					dict.put(cur.symbol, cur.type);
				}
		}
	}
}
