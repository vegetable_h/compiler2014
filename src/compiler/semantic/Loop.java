package compiler.semantic;

public class Loop{
	static int tot = 0;
	int num;
	public Loop prev;
	public Loop(){
		prev = null;
		num = tot;
		tot ++;
	}
	public Loop(Loop _p){
		prev = _p;	
		num = tot;
		tot ++;
	}
	@Override
	public boolean equals(Object ob){
		if (!(ob instanceof Loop))	
			return false;
		else return (num == ((Loop)ob).num);
	}
	@Override
	public int hashCode(){
		return num;		
	} 
}
