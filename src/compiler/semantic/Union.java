package compiler.semantic;

import java.util.Vector;

public class Union extends Type{
	public Symbol name;
	public Vector<SymbolTypePair> v;
	private boolean valid;
	private java.util.Dictionary<Symbol, Type> dict;
	public Union(){
		valid = true; 
		name = null;
		defined = false;
		v = new Vector<SymbolTypePair>();
		dict = new java.util.Hashtable<Symbol, Type>();
	}
	@Override
	public Type clone(){
		Union res = new Union();
		res.name = name;
		res.valid = valid;
		res.defined = defined;
		res.v = v;
		res.dict = dict;
		return res;
	}
	public Union(Symbol n){
		name = n;valid = true;defined = false;
		v = new Vector<SymbolTypePair>();
		dict = new java.util.Hashtable<Symbol, Type>();
	}
	public boolean isDefined(){
		if (defined == false) return false;	
		int cnt = v.size();
		for (int i =0 ; i < cnt; i ++)
			if (!v.get(i).type.isDefined()) return false;
		return true;
	}
	@Override
	public String toString(){
		return "Union " + name.toString();
	}
	@Override
	public boolean convertable(Type typ){
		if (!(typ instanceof Union))return false;	
		return (((Union)typ).name == name);
	}
	@Override 
	public boolean equals(Object obj){
		if (!(obj instanceof Union))return false;	
		return (((Union)obj).name == name);
	}
	@Override
	public int hashCode(){
		return name.hashCode();	
	}
	@Override
	public boolean isValid(){
		return valid;
	}
	public boolean fieldContained(Symbol cur){
		if (defined == false)  return false;
		if (dict.get(cur) == null)	
			return false;
		else return true;
	}
	public Type getFieldType(Symbol cur){
		if (dict.get(cur) == null)return null;	
			else return dict.get(cur);
	}
	public void addField(SymbolTypePair cur){
		if (!cur.type.isValid()) {valid = false;return ;}	
		
		if (dict.get(cur.symbol) != null){
			valid = false; return ;	
		}
		v.add(cur); dict.put(cur.symbol, cur.type);
	}
	@Override
	public int size(){
		int res = 0;
		for (int i = 0; i < v.size(); i ++)
			res = Math.max(res, v.get(i).type.size());
		return res;
	}
	public void fillField(Type tmp){
		for (int i = 0; i < v.size(); i ++){
			SymbolTypePair cur = v.get(i);
			if (cur.type instanceof Pointer){
				if (((Pointer)cur.type).elementType == null)
					cur.type = new Pointer(tmp);
				dict.put(cur.symbol, cur.type);
			}
		}
	}
}
