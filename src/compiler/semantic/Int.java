package compiler.semantic;

public class Int extends Type{
	public Int(){	
	}
	public boolean isDefined(){
		return true;	
	}
	@Override
	public Type clone(){
		Int res = new Int();
		return res;
	}
	@Override
	public boolean equals(Object obj){
		if (!(obj instanceof Int)) return false; else return true;
	}
	@Override
	public String toString(){
		return "Int";
	}
	@Override
	public boolean convertable(Type tmp){
		if (tmp instanceof Char || tmp instanceof Int || tmp instanceof Pointer)	
			return true;
		else return false;
	}
}
