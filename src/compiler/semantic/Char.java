package compiler.semantic;

public class Char extends Type{
	public Char(){
	}
	public boolean isDefined(){return true;}
	@Override
	public Type clone(){
		Char res = new Char();
		return res;
	}
	@Override
	public boolean equals(Object obj){
		return (obj instanceof Char);
	}
	@Override
	public String toString(){
		return "Char";
	}
	@Override
	public boolean convertable(Type tmp){
		if (tmp instanceof Char || tmp instanceof Int ||
				tmp instanceof Pointer)	
			return true;
		else return false;
	}	
}
