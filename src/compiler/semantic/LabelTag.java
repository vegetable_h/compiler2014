package compiler.semantic;

public class LabelTag{
	public LabelTag prev;
	static int tot = 0;
	int num;
	public LabelTag(){
		prev = null;
		num = tot;
		tot ++;
	}
	public LabelTag(LabelTag _p){
		prev = _p;	
		num = tot;
		tot ++;
	}
	@Override
	public boolean equals(Object ob){
		if (!(ob instanceof LabelTag)) return false;
		return (((LabelTag)ob).num == num);
	}
	@Override
	public int hashCode(){
		return num;	
	}
}
