package compiler.semantic;

import java.util.Vector;

import compiler.translate.VarInfo;

public class Function extends Type{
	public Type returnType;
	public Vector<Type> para;	
	public Symbol name;
	public boolean defined;
	private boolean valid;
	public Function(Symbol n){
		name = n;	
	}
	public Function(Type typ){
		returnType = typ;
		para = new Vector<Type>();
		defined = false;
		valid = true;
		name = null;
	}
	@Override
	public int size(){
		return returnType.size();
	}
	@Override
	public Type clone(){
		Function res = new Function();
		res.returnType = returnType;
		res.para = para;
		res.name = name;
		res.defined = defined;
		res.valid = valid;
		return res;
	}
	public boolean convertable(Type typ){
		return returnType.convertable(typ);
	}
	@Override
	public String toString(){
		return "Function " + name.toString();
	}
	public Function(){
		returnType = null;
		para = new Vector<Type>();
		defined = false;
		valid = true;
		name = null;
	}
	@Override
	public boolean isDefined(){return defined;}
	@Override
	public boolean equals(Object obj){
		if (!(obj instanceof Function)) return false;
		if (!((Function)obj).returnType.equals(returnType)) return false;
		if (((Function)obj).name != name) return false;
		if (((Function)obj).para.size() != para.size()) return false;
		for (int i = 0; i < para.size(); i ++)
			if (!((Function)obj).para.get(i).equals(para.get(i)))
				return false;
		return true;
	}
	@Override 
	public boolean isValid(){
		if (!valid) return false;	
		if (returnType instanceof Function ||
				returnType instanceof Array)
			return false;
		return true;
	}
	@Override 
	public boolean islValue(){return false;}
	public void addPar(SymbolTypePair cur){
		para.add(cur.type);	
	}
	public boolean parameterCheck(Vector<VarInfo> v){
		if (name == Symbol.symbol("scanf") ||
				name == Symbol.symbol("printf")){
			if (v.size() == 0) return false;
			Type arrayChar = new Array(new Char(), 1);
			Type pointerChar = new Pointer(new Char());
			if (!(v.get(0).type.equals(arrayChar) || 
					v.get(0).type.equals(pointerChar) ||
					v.get(0).type.equals(new Char()))) return false;
			return true;
		}
		if (v.size() != para.size()) return false;
		for (int i = 0; i < v.size(); i ++){
			if (!v.get(i).type.convertable(para.get(i))){
				return false;	
			}
		}	
		return true;
	}
};
