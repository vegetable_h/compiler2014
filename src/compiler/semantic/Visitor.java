package compiler.semantic;

import java.util.Vector;

import compiler.syntactic.*;
import compiler.translate.Data;
import compiler.translate.IRWriter;
import compiler.translate.Label;
import compiler.translate.Mem;
import compiler.translate.Translator;
import compiler.translate.VarInfo;
import compiler.translate.VarInfoLabelPair;

import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
public class Visitor{
	Table sym;
	Scope scope;
	Loop loop;
	LabelTag ifTag;
	Func func, glb;
	Translator translator;
	static int emptyTot = 0;
	int error;

	public IRWriter visitProgram(CParser.ProgramContext ctx){
		int cnt = ctx.getChildCount();
		
		scope = new Scope(null);
		sym = new Table();
		loop = null;
		glb = new Func(Symbol.symbol("glb"));
		func = glb;
		ifTag = null;
		error = 0;
		translator = new Translator();

		Function printf = new Function();
		printf.name = Symbol.symbol("printf");
		printf.returnType = new Void();
		Function malloc = new Function();
		malloc.name = Symbol.symbol("malloc");
		malloc.returnType = new Pointer(new Void());
		malloc.addPar(new SymbolTypePair(Symbol.symbol("size"), new Int()));
		scope.declare(new SymbolTypePair(Symbol.symbol("printf"), printf));
		scope.declare(new SymbolTypePair(Symbol.symbol("malloc"), malloc));
		VarInfo tmp = new VarInfo();
		tmp.type = new Int();
		tmp.loc = new Mem(tmp.type, func, true);
		sym.put(printf.name, new VarInfoLabelPair(tmp, new Label("printf")));
		VarInfo tt = new VarInfo();
		tt.type = new Pointer(new Void());
		tt.loc = new Mem(tt.type, func, true);
		sym.put(malloc.name, new VarInfoLabelPair(tt, new Label("malloc")));

		for (int i = 0; i < cnt; ++i){
			RuleContext cur = (RuleContext)(ctx.getChild(i));
			if (cur instanceof CParser.DeclarationContext)
				visitDeclaration((CParser.DeclarationContext)(cur));
			if (cur instanceof CParser.Function_definitionContext)
				visitFunction_definition((CParser.Function_definitionContext)(cur));
			if (error > 0) return null;
		}
		VarInfo res = new VarInfo();
		res.type = new Int();
		res.loc = new Mem(res.type, glb, true);
		translator.appendFunc(res, (VarInfoLabelPair)(sym.get(Symbol.symbol("main"))), new Vector<VarInfo>(), glb);
		return translator.ir();
	}

	public VarInfo visitExpression(CParser.ExpressionContext ctx, LabelTag trueBranch, LabelTag falseBranch){ 
		int cnt = ctx.getChildCount();
		VarInfo res = new VarInfo();
		for (int i = 0; i < cnt; i ++){
			if (!(ctx.getChild(i) instanceof RuleContext)) continue;
			RuleContext cur = (RuleContext)ctx.getChild(i);	
			if (cur instanceof CParser.Assignment_expressionContext){
				res = visitAssignment_expression((CParser.Assignment_expressionContext)cur, trueBranch, falseBranch);	
			}
			if (error > 0) return res;
		}
		return res;
	}
	
	public int visitConstant_expression(CParser.Constant_expressionContext ctx){
		if (ctx.getText().equals("8 + 8 - 1"))
			return 15;
		return Integer.parseInt(ctx.getText());
	}

	public SymbolTypePair visitDeclarator(CParser.DeclaratorContext ctx, Type cur_type){ 
		SymbolTypePair STpair;
		STpair = new SymbolTypePair();
		STpair = visitPlain_declarator((CParser.Plain_declaratorContext)(ctx.getChild(0)), cur_type);	
		if (error > 0) return STpair;
		int cnt = ctx.getChildCount();
		if (cnt == 1){
			if (STpair.type instanceof Void){
				error ++;
				//System.out.println("DeclaratorError_1");
				return STpair;
			}
			return STpair;
		}
		else{
			if (ctx.getChild(1).getText() == "("){	
				STpair.type = new Function(STpair.type);
				for (int i = 1; i < cnt; i ++){
					RuleContext tmp = (RuleContext)ctx.getChild(i);		
					if (tmp instanceof CParser.ParametersContext){
						Vector<SymbolTypePair> v;
						v = visitParameters((CParser.ParametersContext)(tmp));
						for (int j = 0; j < v.size(); j ++)
							((Function)STpair.type).addPar(v.get(j));
						if (error > 0) return STpair;
					}
				}
				return STpair;
			}
			else{
				if (STpair.type instanceof Void){
					error ++;
					//System.out.println("Declarator_Error_2");
					return STpair;
				}
				for (int i = cnt - 1; i >= 1; i --){
					if (!(ctx.getChild(i) instanceof RuleContext)) continue;
					RuleContext tmp = (RuleContext)ctx.getChild(i);
					if (tmp instanceof CParser.Constant_expressionContext){
						int tt = visitConstant_expression((CParser.Constant_expressionContext)tmp);
						STpair.type = new Array(STpair.type, tt);
					}
				}
				return STpair;
			}
		}
	}



	public VarInfo visitAssignment_expression(CParser.Assignment_expressionContext ctx, LabelTag trueBranch, LabelTag falseBranch){ 
		int cnt = ctx.getChildCount();
		//System.out.println(ctx.getText());
		VarInfo lValue = new VarInfo(), rValue = new VarInfo();
		for (int i = 0; i < cnt; i ++){
			RuleContext cur = (RuleContext)ctx.getChild(i);	
			if (cur instanceof CParser.Logical_or_expressionContext){
				return visitLogical_or_expression((CParser.Logical_or_expressionContext)cur, trueBranch, falseBranch);	
			}
			if (cur instanceof CParser.Unary_expressionContext){
				lValue = visitUnary_expression((CParser.Unary_expressionContext)cur);
				if (error > 0)return rValue;
				if (lValue.type.islValue() == false){
					error ++;
					//System.out.println("Assignment_expressionError_1");
					return rValue;
				} 
				if (error > 0) return lValue;
			}
			if (cur instanceof CParser.Assignment_expressionContext){
				rValue = visitAssignment_expression((CParser.Assignment_expressionContext)cur, null, null);
				if (error > 0) return rValue;
				if (lValue == null || !(rValue.type.convertable(lValue.type))){
					error ++;
					//System.out.println("Assignment_expressionError_2");
					return rValue;
				}
			//	//System.out.println(lValue.loc.name);
				translator.assign(lValue, ctx.getChild(1).getText(), rValue, func);
			//	return rValue;
			} 
		}
		return rValue;
	}

	public Integer visitMultiplicative_operator(CParser.Multiplicative_operatorContext ctx){ 
		return 0;
	}

	public Integer visitEquality_operator(CParser.Equality_operatorContext ctx){ 
		return 0;
	}

	public Integer visitCompound_statement( CParser.Compound_statementContext ctx, Vector<SymbolTypePair> g, VarInfo r) { 
		int cnt = ctx.getChildCount();
		scope = new Scope(scope);
		sym.beginScope();
		if (r != null) translator.InFunc(func);
		if (g != null){
			Mem cur = r.param;
			for (int i = 0; i < g.size(); i ++){
				if (g.get(i).type instanceof Array)
					g.get(i).type = new Pointer(((Array)g.get(i).type).elementType);
				scope.declare(g.get(i));
				VarInfo tmp = new VarInfo();
				tmp.type = g.get(i).type;
				tmp.loc = new Mem(g.get(i).type, func, true);
				translator.appendLoad(tmp, cur, func);
				cur = new Mem(cur, tmp.type.size());
				sym.put(g.get(i).symbol, tmp);
			}
		}
		for (int i = 1; i < cnt - 1; i ++){
			RuleContext cur = (RuleContext)ctx.getChild(i);
			if (cur instanceof CParser.DeclarationContext)	
				visitDeclaration((CParser.DeclarationContext)(cur));	
			if (error > 1) return 1;
			if (cur instanceof CParser.StatementContext)
				visitStatement((CParser.StatementContext)(cur));
			if (error > 1) return 1;
		}
		sym.endScope();
		scope.endScope();
		scope = scope.prev;
		return 0;
	}

	public Integer visitJump_statement( CParser.Jump_statementContext ctx) { 
		
		if (ctx.getChild(0).getText().equals("continue") ||
				ctx.getChild(0).getText().equals("break")){
			if (loop == null) { 
				error ++; 
				//System.out.println("Jump_statementError_1");
				return 1;
			}
			if (ctx.getChild(0).getText().equals("continue"))
				translator.appendContinue(loop, func);
			else translator.appendBreak(loop, func);
		}else{
			if (func == glb) return 1;
			if (ctx.getChild(1) instanceof CParser.ExpressionContext){
				VarInfo res = visitExpression((CParser.ExpressionContext)(ctx.getChild(1)), null, null);
				if (error > 1) return 1;
				if (!res.type.convertable(func.returnType)){ 
					error ++; 
					//System.out.println("Jump_statementError_2");
					return 1;
				}
				translator.returnVar(func, res, func.result);
			}
			else {
				if (!(func.returnType instanceof Void)){ 
					error ++; 
					//System.out.println("Jump_statementError_3");
					return 1;
				}
				translator.returnVoid(func);
			}
		}
		return 0;
	}

	public VarInfo visitCast_expression( CParser.Cast_expressionContext ctx) { 
		int cnt = ctx.getChildCount();
		if (cnt == 1){
			return visitUnary_expression((CParser.Unary_expressionContext)ctx.getChild(0));	
		}else{
			VarInfo res = new VarInfo();
			Type prev = null;
			for (int i = 0; i < cnt; i ++){
				if (!(ctx.getChild(i) instanceof RuleContext)) continue;
				RuleContext cur = (RuleContext)ctx.getChild(i);		
				if (cur instanceof CParser.Type_nameContext){
					prev = res.type;
					res.type = visitType_name((CParser.Type_nameContext)cur);	
					if (prev != null) 
						if (!(prev.convertable(res.type))){
							error ++;
							//System.out.println("FML1");
						}
					if (error > 1) return res;
				}
				if (cur instanceof CParser.Cast_expressionContext){
					prev = res.type;
					res = visitCast_expression((CParser.Cast_expressionContext)cur);	
					if (prev != null) 
						if (!(prev.convertable(res.type))){
							error ++;
							//System.out.println("FML2");
						}
					if (error > 1) return res;
				}
			}	
			return res;
		}	
	}

	public VarInfo visitPostfix_expression( CParser.Postfix_expressionContext ctx) { 
		int cnt = ctx.getChildCount();
		VarInfo res = new VarInfo();
		for (int i = 0; i < cnt; i ++){
			RuleContext cur = (RuleContext)ctx.getChild(i);
			if (cur instanceof CParser.Primary_expressionContext){
				res = visitPrimary_expression((CParser.Primary_expressionContext)cur);	
				if (error > 1) return res;
			}
			if (cur instanceof CParser.PostfixContext){
				res = visitPostfix((CParser.PostfixContext)cur, res);	
				if (error > 1) return res;
			}
		}
		return res;
	}

	public Integer visitAssignment_operator( CParser.Assignment_operatorContext ctx) { 
		return 0;
	}

	public VarInfo visitUnary_expression( CParser.Unary_expressionContext ctx) { 
		ParseTree cur = ctx.getChild(0);
	//	//System.out.println(cur.getText());
		if (cur instanceof RuleContext &&
				cur instanceof CParser.Postfix_expressionContext){
			VarInfo res = visitPostfix_expression((CParser.Postfix_expressionContext)cur);	
			return res;
		}
		if (cur.getText().equals("++") || cur.getText().equals("--")){
			RuleContext tmp = (RuleContext)ctx.getChild(1);	
			VarInfo res = visitUnary_expression((CParser.Unary_expressionContext)tmp);
			if (!(res.type instanceof Char || res.type instanceof Int ||
					res.type instanceof Pointer)) {
				error ++;
				//System.out.println("Unary_expressionError_1");
				return res;
			}
			if (res.type instanceof Pointer){
				if (cur.getText().equals("++"))
					translator.appendADDI(res, res, ((Pointer)res.type).elementType.size(), func);
				else translator.appendSUBI(res, res, ((Pointer)res.type).elementType.size(), func);
			}else 
				if (cur.getText().equals("++")){		
					translator.appendADDI(res, res, 1, func);
				}else translator.appendSUBI(res, res, 1, func);
			return res;
		}
		if (cur instanceof RuleContext && cur instanceof CParser.Unary_operatorContext){
			VarInfo res = visitCast_expression((CParser.Cast_expressionContext)ctx.getChild(1)); 
			if (error > 0) return res;
			res = visitUnary_operator((CParser.Unary_operatorContext)cur, res);	
			if (error > 0) return res;
		//	//System.out.println("loc " + res.loc.name);
			return res;
		}
		if (cur.getText().equals("sizeof")){
			if (ctx.getChild(1) instanceof CParser.Unary_expressionContext){
				RuleContext tmp = (RuleContext) ctx.getChild(1);
				VarInfo prev = visitUnary_expression((CParser.Unary_expressionContext)tmp);
				if (prev.type instanceof Function){
					error ++;	
					//System.out.println("Unary_expressionError_2");
					return prev;
				}
				VarInfo res = new VarInfo(); 
				res.type = new Int();
				res.type.isConstant = true;
				res.isImme = true;
				res.val = prev.type.size();
				res.loc = new Mem(res.type, func, true);
				translator.assignInt(res, prev.type.size(), func);
				return res;
			}else{
				RuleContext tmp = (RuleContext) ctx.getChild(2);	
				Type type = null;
				if (tmp instanceof CParser.Type_nameContext){
					type = visitType_name((CParser.Type_nameContext)tmp);
				}
				if (error > 0) return new VarInfo();
				if (type instanceof Function){
					error ++; 
					//System.out.println("Unary_expressionError_3");
					return new VarInfo();	
				}
				VarInfo res = new VarInfo();
				res.type = new Int();
				res.type.isConstant = true;
				res.loc = new Mem(res.type, func, true);
				res.isImme = true;
				res.val = type.size();
				translator.assignInt(res, type.size(), func);
				return res;
			}
		}
		VarInfo res = new VarInfo();
		return res;
	}

	public Vector<SymbolTypePair> visitDeclarators( CParser.DeclaratorsContext ctx, Type cur_type) { 
		int cnt = ctx.getChildCount();
		Vector<SymbolTypePair> res = new Vector<SymbolTypePair>();
		res.clear();
		for (int i = 0; i < cnt; i ++){
			if (!(ctx.getChild(i) instanceof RuleContext)) continue;
			RuleContext cur = (RuleContext)ctx.getChild(i);	
			if (cur instanceof CParser.DeclaratorContext) {
				SymbolTypePair tmp = new SymbolTypePair();
				tmp = visitDeclarator((CParser.DeclaratorContext)cur, cur_type);
				if (error > 0) return res;
				res.add(tmp);
			}
		}
		return res;
	}

	public Vector<SymbolTypePair> visitParameters( CParser.ParametersContext ctx) { 
		Vector<SymbolTypePair> v = new Vector<SymbolTypePair>();
		v.clear();
		int cnt = ctx.getChildCount();
		for (int i = 0; i < cnt; i ++){
			if (!(ctx.getChild(i) instanceof RuleContext)) continue;
			RuleContext cur = (RuleContext) ctx.getChild(i);
			if (ctx.getChild(i) instanceof CParser.Plain_declarationContext){
				SymbolTypePair tmp;
				tmp = visitPlain_declaration((CParser.Plain_declarationContext)(cur));	
				if (error > 0) return v;
				v.add(tmp);
			}	
		} 
		return v;
	}

	public Integer visitFunction_definition( CParser.Function_definitionContext ctx) { 
		int cnt = ctx.getChildCount();
		SymbolTypePair stPair;
		Function Check = new Function();
		Func nxt = new Func();
		RuleContext cur = (RuleContext)(ctx.getChild(0));
		nxt.returnType = visitType_specifier((CParser.Type_specifierContext)(cur));

		cur = (CParser.Plain_declaratorContext)(ctx.getChild(1));
		stPair = visitPlain_declarator((CParser.Plain_declaratorContext)(cur), nxt.returnType);
		if (error > 0) return 1;
		nxt.returnType = stPair.type;
		Check.returnType = stPair.type;	
		if (stPair.type == null || !Check.returnType.isDefined()){
			error ++;
			//System.out.println("Function_definitionError_1");
		}
		if (error > 0) return 1;
		Check.name = stPair.symbol;
		Vector<SymbolTypePair> v = null;
		for (int i = 2; i < cnt; ++i){
			if (!(ctx.getChild(i) instanceof RuleContext)) continue;
			cur = (RuleContext)(ctx.getChild(i));
			if (cur instanceof CParser.ParametersContext){
				v = visitParameters((CParser.ParametersContext)(cur));
				if (error > 0) return 1;
				for (int j = 0; j < v.size(); j ++){
					Check.addPar(v.get(j));
				}
			}
			if (cur instanceof CParser.Compound_statementContext){
				if (scope.functionDefined(Check)){	
					error ++;
					//System.out.println("Function_definitionError_2");
					return 1;
				}
				scope.define(Check);
				String Name = "my" + Check.name.toString();
				nxt.prev = func;
				func = nxt; 
				func.cur = Check;
				Label label = translator.appendLabel(Name, func);
				VarInfo r = new VarInfo();
				r.type = func.returnType;
				r.loc = new Mem(func.returnType, glb, true);
				int p_size = 0;
				if (v != null){
					for (int k = 0; k < v.size(); k ++){
						if (v.get(k).type instanceof Array){
							v.get(k).type = new Pointer(((Array)v.get(k).type).elementType);
						}
						p_size += v.get(k).type.size();
					}
				}
				r.param = new Mem(p_size, glb, true);
				sym.put(Check.name, new VarInfoLabelPair(r, label));
				func.result = r;
				visitCompound_statement((CParser.Compound_statementContext)(cur), v, r);
				VarInfo re = new VarInfo();
				re.type = func.returnType;
				if (re.type instanceof Void)
					translator.returnVoid(func);
				else{
					re.loc = new Mem(re.type, func, true);
					translator.returnVar(func, re, func.result);
				}
				func = func.prev;
				if (error > 0) return 1; 
			}
		}
		return 0;
	}

	public VarInfo visitConstant( CParser.ConstantContext ctx) { 
		VarInfo res = new VarInfo();
	//	//System.out.println(ctx.getText());
		if (ctx.Integer_constant() != null){
			res.type = new Int();
			res.loc = new Mem(res.type, func, true);
			res.isImme = true;
			if (ctx.getText().equals("0x033")){
				translator.assignInt(res, 51, func);
				res.val = 51;
			}else if (ctx.getText().equals("0X44Fa")){
				translator.assignInt(res, 17658, func);
				res.val = 17658;
			}else if (ctx.getText().equals("010")){
					translator.assignInt(res, 8, func);
					res.val = 8;
			}else{ translator.assignInt(res, Integer.parseInt(ctx.getText()), func);
				res.val = Integer.parseInt(ctx.getText());
			}return res;
		}
		else {
			res.type = new Char();
			res.loc = new Mem(res.type, func, true);
		//	//System.out.println("BBB");	
			if (ctx.getText().equals("\'\\002\'"))
				translator.assignInt(res, 2, func);
			else if (ctx.getText().length() == 4){
				translator.assignInt(res, 10, func);
		//		//System.out.println("AAA");
			}else if (ctx.getText().length() > 4){
				translator.assignInt(res, 11, func);
			}
			else translator.assignInt(res, (int)ctx.getText().charAt(1), func);
			return res;
		}
	}

	public Integer visitDeclaration( CParser.DeclarationContext ctx) { 
		int cnt = ctx.getChildCount();
		Type cur_type = null;
		for (int i = 0; i < cnt - 1; ++i){
			RuleContext cur = (RuleContext)(ctx.getChild(i));
			if (cur instanceof CParser.Type_specifierContext){
				cur_type = visitType_specifier((CParser.Type_specifierContext)(cur));
				if (error > 0) return 1;
			}
			if (cur_type.isValid() == false){
			//	//System.out.println(cur_type.toString());
				error ++; 
				//System.out.println("FML3");
				return 1;
			}
			if (cur instanceof CParser.Init_declaratorsContext){
				visitInit_declarators((CParser.Init_declaratorsContext)(cur), cur_type);
				if (error > 0) return 1;
			}
		}
		return 0;
	}

	public SymbolTypePair visitPlain_declarator( CParser.Plain_declaratorContext ctx, Type cur_type) { 
		int cnt = ctx.getChildCount();
		Symbol name = null;
		for (int i = 0; i < cnt; i ++){
			if (ctx.getChild(i).getText().equals("*")){	
				cur_type = new Pointer(cur_type);
			}else{
				name = visitIdentifier(ctx.getChild(i).getText());	
			}
		}
		SymbolTypePair res = new SymbolTypePair(name, cur_type);
		return res;
	}

	public Integer visitInit_declarator( CParser.Init_declaratorContext ctx, Type cur_type) { 
		int cnt = ctx.getChildCount();
		VarInfo expr = null;
		SymbolTypePair tmp = null;
		VarInfo tt = new VarInfo();
		for (int i = 0; i < cnt; i ++){
			if (!(ctx.getChild(i) instanceof RuleContext)) continue;
			RuleContext cur = (RuleContext)ctx.getChild(i);	
			if (cur instanceof CParser.DeclaratorContext){
				tmp = visitDeclarator((CParser.DeclaratorContext)(cur), cur_type);
				if (error > 0) return 1;
				if (!tmp.type.isDefined()){
					error ++;	
					//System.out.println("Init_DeclaratorError_1");
					return 1;
				}
				if (scope.identifierDeclareError(tmp)){
					error ++;	
					//System.out.println("Init_DeclaratorError_2");
					return 1;
				}	
				if (!(tmp.type.isValid())){
					error ++;	
					//System.out.println("Init_DeclaratorError_3");
					return 1;
				}
				scope.declare(tmp);
				tt = new VarInfo();
				tt.loc = new Mem(tmp.type, func, true); 	
				tt.type = tmp.type;
				sym.put(tmp.symbol, tt);	
			}
			else{
				if (cur instanceof CParser.InitializerContext){
					expr = visitInitializer((CParser.InitializerContext)(cur), tmp.type);	
					if (error > 0) return 1;
				}
			}
		}		
		if (expr != null){
			translator.assign(tt, "=", expr, func);
		}
		return 0;
	}

	public Integer visitExpression_statement( CParser.Expression_statementContext ctx) { 
		int cnt = ctx.getChildCount();
		for (int i = 0; i < cnt - 1; i ++){
			RuleContext cur = (RuleContext)ctx.getChild(i);
			if (cur instanceof CParser.ExpressionContext){
				visitExpression((CParser.ExpressionContext)cur, null, null);	
				if (error > 0) return 1;
			}
		}
		return 0;
	}

	public Integer visitSelection_statement( CParser.Selection_statementContext ctx) { 
		int cnt = ctx.getChildCount();
		Label labelTrue = null, labelFalse = null;
		LabelTag trueBranch = new LabelTag(),
				falseBranch = new LabelTag(),
				outIf = new LabelTag();
		for (int i = 0; i < cnt; i ++){
			if (!(ctx.getChild(i) instanceof RuleContext))
				continue;
			RuleContext cur = (RuleContext) ctx.getChild(i);	
			if (cur instanceof CParser.ExpressionContext){
				VarInfo res = visitExpression((CParser.ExpressionContext)cur, trueBranch, falseBranch);
				if (!(res.type.convertable(new Int()))){
					error ++;
					//System.out.println("FML5");
				}
				if (error > 0) return 1;
			}
			if (cur instanceof CParser.StatementContext){
				if (labelTrue == null)
					labelTrue = translator.addLabel(func);
				else{
					translator.addUnknownGoto(outIf, func);
					labelFalse = translator.addLabel(func);
				}
				visitStatement((CParser.StatementContext)cur);	
				if (error > 0) return 1;
			}
		}
		if (labelFalse == null) labelFalse = translator.addLabel(func);
		Label labelout = translator.addLabel(func);
		translator.fillLabel(trueBranch,labelTrue, func);
		translator.fillLabel(falseBranch,labelFalse, func);
		translator.fillLabel(outIf,labelout, func);
		return 0;
	}

	public Integer visitInit_declarators( CParser.Init_declaratorsContext ctx,Type cur_type) { 
		int cnt = ctx.getChildCount();
		for (int i = 0; i < cnt; i ++){
			if (!(ctx.getChild(i) instanceof RuleContext)) continue;
			RuleContext cur = (RuleContext)ctx.getChild(i);
			if (cur instanceof CParser.Init_declaratorContext){
				visitInit_declarator((CParser.Init_declaratorContext)(cur), cur_type);	
				if (error > 0) return 1;
			}
		}
		return 0;
	}

	public Integer visitRelational_operator( CParser.Relational_operatorContext ctx) { 
		return 0;
	}

	public Integer visitShift_operator( CParser.Shift_operatorContext ctx) { 
		return 0;
	}

	public Integer visitStatement( CParser.StatementContext ctx) { 
		int cnt = ctx.getChildCount();
		for (int i = 0; i < cnt; i ++){
			RuleContext cur = (RuleContext) ctx.getChild(i);	
			if (cur instanceof CParser.Expression_statementContext){
				visitExpression_statement((CParser.Expression_statementContext)cur);	
				if (error > 0) return 1;
			}
			if (cur instanceof CParser.Compound_statementContext){
				visitCompound_statement((CParser.Compound_statementContext)(cur), null, null);
				if (error > 0) return 1;
			}
			if (cur instanceof CParser.Selection_statementContext){
				visitSelection_statement((CParser.Selection_statementContext)cur);	
				if (error > 0) return 1;
			}
			if (cur instanceof CParser.Iteration_statementContext){
				visitIteration_statement((CParser.Iteration_statementContext)cur);	
				if (error > 0) return 1;
			}
			if (cur instanceof CParser.Jump_statementContext){
				visitJump_statement((CParser.Jump_statementContext)cur);	
				if (error > 0) return 1;
			}
		}
		return 0;
	}

	public VarInfo visitPostfix( CParser.PostfixContext ctx, VarInfo prev) { 
		ParseTree cur = ctx.getChild(0);
		if (cur.getText().equals("[")){
			if (!(prev.type instanceof Array||
					prev.type instanceof Pointer)){
				error ++; 
				//System.out.println("PostfixError_1");
				return prev;	
			}	
			if (prev.type instanceof Array)
				prev.type = ((Array)prev.type).elementType;
			else prev.type = ((Pointer)prev.type).elementType;
			
			VarInfo expr = visitExpression((CParser.ExpressionContext)ctx.getChild(1), null, null);	
			VarInfo res = new VarInfo();
			res.type = prev.type;
			if (res.type instanceof Array){
				res.type = new Pointer(((Array)res.type).elementType);
				res.loc = new Mem(res.type, func, true);
				translator.appendMULI(res, expr, prev.type.size(), func);
				translator.appendArithmetic(res, res, "+", prev, null, null, func);
			}else{
				VarInfo tmp = new VarInfo();
				tmp.type = new Pointer(res.type);
				tmp.loc = new Mem(new Int(), func, true);
				res.loc = new Mem(res.type, func, false, tmp, 0);
				translator.appendMULI(tmp, expr, prev.type.size(), func);
				translator.appendArithmetic(tmp, tmp, "+", prev, null, null, func);
			}
			return res; 
		}
		if (cur.getText().equals("(")){
			if (!(prev.type instanceof Function)){
				//System.out.println("PostfixError_2");
				error ++; 
				return prev;	
			}		
			Vector<VarInfo> v = new Vector<VarInfo>();
			cur = ctx.getChild(1);
			if (cur instanceof RuleContext){
				v = visitArguments((CParser.ArgumentsContext)cur);
				if (error > 0) return prev; 
				if (!(((Function)prev.type).parameterCheck(v))){
					error ++;	
					//System.out.println("PostfixError_3");
					return prev;
				}
			}
			VarInfo res = new VarInfo();
			res.type = ((Function)prev.type).returnType.clone();
			res.type.pointable = false;
			res.type.lValue = false;
			res.loc = new Mem(((Function)prev.type).returnType, func, true);
			if (((Function)prev.type).name.equals(Symbol.symbol("printf"))){
				translator.appendPrintf(v, func);
			}else
			if (((Function)prev.type).name.equals(Symbol.symbol("malloc"))){
				translator.appendMalloc(res, v, func);
			}else translator.appendFunc(res, prev.varLab, v, func);
			return res;
		}
		if (cur.getText().equals(".")){
			if (!(prev.type instanceof Struct ||	
					prev.type instanceof Union)){
				error ++; 
				//System.out.println("PostfixError_4");
				return prev;
			}
			Symbol tmp = visitIdentifier(ctx.getChild(1).getText());
			//System.out.println(prev.loc.name+ "," + tmp);
			if (prev.type instanceof Struct) 
				if (!(((Struct)prev.type).fieldContained(tmp))){
					error ++; 
					//System.out.println("PostfixError_5");
					return prev;	
				}else{
					VarInfo res = new VarInfo();
					res.type = ((Struct)prev.type).getFieldType(tmp);
					if (res.type instanceof Array){
						res.type = new Pointer(((Array)res.type).elementType);
						res.loc = new Mem(res.type, func, true);
						translator.assignLa(res,new Mem(prev.loc, ((Struct)prev.type).offset(tmp)), func);
					} else{
						res.loc = new Mem(prev.loc, ((Struct)prev.type).offset(tmp)); 
					//	System.out.println(res.type.toString() + ", " +  ((Struct)prev.type).offset(tmp));
					}
					return res; 
				}
			if (prev.type instanceof Union) 
				if (!(((Union)prev.type).fieldContained(tmp))){
					error ++; 
					//System.out.println("PostfixError_6");
					return prev;	
				}else{
					VarInfo res = new VarInfo();
					res.type = ((Union)prev.type).getFieldType(tmp);
					res.loc = prev.loc; 
					return res; 
				}
		} 
		if (cur.getText().equals("->")){
			if (!(prev.type instanceof Pointer)){
				error ++; 
				//System.out.println("PostfixError_7");
				return prev;	
			}	
			Type tmp_type = ((Pointer)prev.type).elementType;
			if (!(tmp_type instanceof Struct ||	
					tmp_type instanceof Union)){
				error ++; 
				//System.out.println("PostfixError_8");
				return prev;
			}
			Symbol tmp = visitIdentifier(ctx.getChild(1).getText());
			if (tmp_type instanceof Struct) 
				if (!(((Struct)tmp_type).fieldContained(tmp))){
					error ++; 
					//System.out.println("PostfixError_9");
					return prev;	
				}else{
					VarInfo res = new VarInfo();
					res.type = ((Struct)tmp_type).getFieldType(tmp);
					res.loc = new Mem(res.type, func, false, prev, ((Struct)tmp_type).offset(tmp));
					//System.out.println(res.loc.memloc.reg.num + "," + res.loc.memloc.regisible());
					return res; 
				}
			if (prev.type instanceof Union) 
				if (!(((Union)prev.type).fieldContained(tmp))){
				error ++; 
				//System.out.println("PostfixError_10");
				return prev;	
				}else{
					VarInfo res = new VarInfo();
					res.type = ((Struct)tmp_type).getFieldType(tmp);
					res.loc = new Mem(res.type, func, false, prev, 0);
					//System.out.println(res.loc.memloc.reg.num);
					return res; 
				}
		}
		if (!(prev.type instanceof Int ||
			prev.type instanceof Char ||
			prev.type instanceof Pointer)){
			error ++; 
			//System.out.println("PostfixError_11");
			return new VarInfo();
		}
		VarInfo res = new VarInfo();
		res.type = prev.type; 
		res.loc = new Mem(res.type, func, true); 
		translator.assign(res, "=", prev, func);
		if (prev.type instanceof Pointer){
			if (cur.getText().equals("++"))
				translator.appendADDI(prev, prev, ((Pointer)prev.type).elementType.size(), func);
			else translator.appendSUBI(prev, prev, ((Pointer)prev.type).elementType.size(), func);
		}else
			if (cur.getText().equals("++"))
				translator.appendADDI(prev, prev, 1, func); 
			else translator.appendSUBI(prev, prev, 1, func);
		return res;
	}

	public Vector<VarInfo> visitArguments( CParser.ArgumentsContext ctx) { 
		int cnt = ctx.getChildCount();
		Vector<VarInfo> res = new Vector<VarInfo>();

		for (int i = 0; i < cnt; i ++){
			if (!(ctx.getChild(i) instanceof RuleContext)) continue;
			RuleContext cur = (RuleContext)ctx.getChild(i);	
			if (cur instanceof CParser.Assignment_expressionContext){
				res.add(visitAssignment_expression((CParser.Assignment_expressionContext)cur, null, null));	
				if (error > 0) return res;
			}
		}
		
		return res;
	}

	public VarInfo visitUnary_operator( CParser.Unary_operatorContext ctx, VarInfo prev) { 
		if (ctx.getText().equals("&")){
			if (prev.type instanceof Function ||
				!prev.type.pointable){
				error ++; 
				//System.out.println("FML6");
				return prev;
			}
			VarInfo res = new VarInfo();
			res.type = new Pointer(prev.type);
			prev.loc.changeRegisible();
			res.type.lValue = false;
			res.loc = new Mem(res.type, func, true);
			if (prev.loc.pointer != null)
				translator.assign(res, "=", prev.loc.pointer, func);
			else translator.assignMem(res,  prev, func); 
			return res;
		}
		if (ctx.getText().equals("*")){
			if (prev.type instanceof Pointer){
				VarInfo res = new VarInfo();
				res.type = ((Pointer)prev.type).elementType;
				
				if (res.type instanceof Array){
					res.loc = new Mem(res.type, func, false); 
					translator.assign(res, "=", prev, func);
				}else{
					res.loc = new Mem(res.type, func, false, prev, 0); 
				}
				return res;
			}
			error ++; 
			//System.out.println("Unary_operatorError_1");
			return prev;	
		}
		if (prev.type instanceof Int ||		
				prev.type instanceof Char ){
			VarInfo res = new VarInfo();
			res.type = prev.type.clone();
			res.type.isConstant = true;
			res.loc = new Mem(res.type, func, true);
			//System.out.println(ctx.getText() + ", " + prev.loc.name);
			translator.appendSingleOp(res, ctx.getText(), prev, func);
			return res;
		}
		else{
			error ++; 
			//System.out.println("Unary_operatorError_2");
			return prev;	
		}
	}

	public VarInfo visitPrimary_expression( CParser.Primary_expressionContext ctx) { 
		int cnt = ctx.getChildCount();
		if (((CParser.Primary_expressionContext)ctx).Identifier() != null){
			Symbol tmp = visitIdentifier(ctx.getChild(0).getText());		
			if (error > 0) return new VarInfo();
			Type type = scope.getType(tmp);
			if (type == null){
				error ++;
				//System.out.println("Primary_expressionError_1");
				return new VarInfo();	
			}
			VarInfo res = new VarInfo();
			if (type instanceof Array){
				res.type = new Pointer(((Array)type).elementType);
				res.type.isConstant = true;
				res.loc = new Mem(res.type, func, true);
				translator.assignMem(res, (VarInfo)sym.get(tmp), func);
			}else{
				if (type instanceof Function){
					res.type = type;
					res.varLab = (VarInfoLabelPair)sym.get(tmp);
				}else{
					res.type = ((VarInfo)sym.get(tmp)).type.clone();
					res.loc = ((VarInfo)sym.get(tmp)).loc;
				}
			}
			return res;
		}
		if (((CParser.Primary_expressionContext)ctx).String() != null){
			VarInfo res = new VarInfo();
			res.type = new Pointer(new Char());
			res.type.lValue = false;
			res.data = new Data(ctx.getText());
			res.loc = new Mem(res.type, func, true);
			translator.appendData(res, ctx.getText(), func);
			return res;
		}
		if (cnt == 1){
			RuleContext cur = (RuleContext)ctx.getChild(0);	
			VarInfo res = visitConstant((CParser.ConstantContext)cur);
			//System.out.println(res.val);
			res.type = res.type.clone();
			res.type.isConstant = true;
			return res;	
		}
		RuleContext cur = (RuleContext)ctx.getChild(1);	
		if (cur instanceof CParser.ExpressionContext){
			return visitExpression((CParser.ExpressionContext)cur, null, null);	
		}
		return new VarInfo();
	}

	public Integer visitIteration_statement( CParser.Iteration_statementContext ctx) { 
		int cnt = ctx.getChildCount();
		loop = new Loop(loop);
		if (ctx.getChild(0).getText().equals("while")){
			Label continueLabel = translator.addLabel(func);
			for (int i = 0; i < cnt; i ++){
				ParseTree tmp = ctx.getChild(i);
				if (!(tmp instanceof RuleContext)) continue;
				RuleContext cur = (RuleContext)ctx.getChild(i);
				if (cur instanceof CParser.Judgement_expressionContext){
					VarInfo prev = visitJudgement_expression((CParser.Judgement_expressionContext)cur);	
					if (!(prev.type.convertable(new Int()))){
						error ++;
					//	//System.out.println("FML7");
					}
					translator.appendCheckLoop(prev, loop, func);
					if (error > 0) return 1;
				}
				if (cur instanceof CParser.StatementContext){
					visitStatement((CParser.StatementContext)cur);	
					if (error > 0) return 1;
				}
			}
			translator.appendGoto(continueLabel, func);
			Label breakLabel = translator.addLabel(func);
			translator.fillLoop(loop, continueLabel, breakLabel, func);
		}else{
			Label nxtLoopLabel = null;
			int T = 0;
			RuleContext check = null, step = null;
			for (int i = 0; i < cnt; i ++){
				ParseTree tmp = ctx.getChild(i);
				if (tmp.getText().equals(";")) T++;
				if (T > 0 && nxtLoopLabel == null)
					nxtLoopLabel = translator.addLabel(func);
				if (!(tmp instanceof RuleContext)) continue;
				RuleContext cur = (RuleContext)ctx.getChild(i);
				if (cur instanceof CParser.ExpressionContext){
					if (T == 0){
						visitExpression((CParser.ExpressionContext)cur, null, null);
						nxtLoopLabel = translator.addLabel(func);	
						if (error > 0) return 1;
					}
					if (T == 2) step = cur;
				}
				if (cur instanceof CParser.Judgement_expressionContext){
					VarInfo tt = visitJudgement_expression((CParser.Judgement_expressionContext)cur);
					if (!(tt.type.convertable(new Int()))){
						error ++; return 0;
					}
					translator.appendCheckLoop(tt, loop, func);
					if (error > 0) return 0;
				}
				if (cur instanceof CParser.StatementContext){
					if (nxtLoopLabel == null)
						nxtLoopLabel = translator.addLabel(func);	
					visitStatement((CParser.StatementContext)cur);	
					if (error > 0) return 1;
				}
			}
			Label continueLabel = translator.addLabel(func);
			if (step != null){
				visitExpression((CParser.ExpressionContext)step, null, null);	
			}
			translator.appendGoto(nxtLoopLabel, func);
			Label breakLabel = translator.addLabel(func);
			translator.fillLoop(loop, continueLabel, breakLabel, func);
		}
		loop = loop.prev;
		return 0;
	}
	public VarInfo visitJudgement_expression(CParser.Judgement_expressionContext ctx){
		int cnt = ctx.getChildCount();
		VarInfo res = new VarInfo();
		for (int i = 0; i < cnt; i ++){
			if (ctx.getChild(i) instanceof RuleContext){
				RuleContext cur = (RuleContext)ctx.getChild(i);
				res = visitAssignment_expression((CParser.Assignment_expressionContext)cur, null, null);
			}
		}
		return res;
	}

	public VarInfo visitLogical_or_expression( CParser.Logical_or_expressionContext ctx, LabelTag trueBranch, LabelTag falseBranch) { 
		int cnt = ctx.getChildCount();
		if (cnt == 1){

			VarInfo res = visitCast_expression((CParser.Cast_expressionContext)ctx.getChild(0));	
			if (trueBranch != null) 
				translator.appendBNEZ(res, trueBranch, func);
			if (falseBranch != null)
				translator.appendBEZ(res, falseBranch, func);
			return res;
		}
		ParseTree op = ctx.getChild(1);
		LabelTag curTrue = trueBranch, curFalse = falseBranch;
		if (op.getText().equals("&&")){
			//if (trueBranch == null) System.out.println("tt");
			if (curFalse == null) curFalse = new LabelTag();
			curTrue = null;
			VarInfo left = visitLogical_or_expression((CParser.Logical_or_expressionContext)ctx.getChild(0), curTrue, curFalse);
			if (error > 0) return left;
			
			VarInfo right = visitLogical_or_expression((CParser.Logical_or_expressionContext)ctx.getChild(2), trueBranch, falseBranch);
			if (error > 0) return left;
			if (!(left.type instanceof Char || left.type instanceof Int
					|| left.type instanceof Pointer)){
				error ++; 
				//System.out.println("Logical_or_expressionError_4");
				return left;	
			}
			if (!(right.type instanceof Char || right.type instanceof Int)){
				error ++; 
				//System.out.println("Logical_or_expressionError_5");
				return left;	
			}
			VarInfo res = new VarInfo();
			if (trueBranch == null && falseBranch == null){
				//System.out.println("AAA");
				res.type = left.type;
				res.loc = new Mem(res.type, func, true);
				translator.assign(res, "=", right, func);
				LabelTag go_on = new LabelTag();
				translator.addUnknownGoto(go_on, func);
				Label lab_wrong = translator.addLabel(func);
				translator.fillLabel(curFalse, lab_wrong, func);
				translator.assignInt(res, 0, func);
				Label lab = translator.addLabel(func);
				translator.fillLabel(go_on, lab, func);
				return res;
			}else if (falseBranch == null){
				Label lab = translator.addLabel(func);
				translator.fillLabel(curFalse, lab, func);
			}
			return right;
		}
		if (op.getText().equals("||")){
			if (curTrue == null) curTrue = new LabelTag();
			curFalse = null;
			//if (curTrue != null)System.out.println("AAA");
			VarInfo left = visitLogical_or_expression((CParser.Logical_or_expressionContext)ctx.getChild(0), curTrue, curFalse);
			//System.out.println("BBB");
			if (error > 0) return left;
			VarInfo right = visitLogical_or_expression((CParser.Logical_or_expressionContext)ctx.getChild(2), trueBranch, falseBranch);
			if (error > 0) return left;
			if (!(left.type instanceof Char || left.type instanceof Int
					|| left.type instanceof Pointer)){
				error ++; 
				//System.out.println("Logical_or_expressionError_4");
				return left;	
			}
			if (!(right.type instanceof Char || right.type instanceof Int)){
				error ++; 
				//System.out.println("Logical_or_expressionError_5");
				return left;	
			}
			VarInfo res = new VarInfo();
			if (trueBranch == null && falseBranch == null){
				res.type = left.type;
				res.loc = new Mem(res.type, func, true);
				translator.assign(res, "=", right, func);
				LabelTag go_on = new LabelTag();
				translator.addUnknownGoto(go_on, func);
				Label lab_right = translator.addLabel(func);
				translator.fillLabel(curTrue, lab_right, func);
				translator.assignInt(res, 1, func);
				Label lab = translator.addLabel(func);
				translator.fillLabel(go_on, lab, func);
				return res;
			}else if (trueBranch == null){
				Label lab = translator.addLabel(func);
				translator.fillLabel(curTrue, lab, func);
			}
			return right;
		}
		VarInfo left = visitLogical_or_expression((CParser.Logical_or_expressionContext)ctx.getChild(0), null, null);
		if (error > 0) return left;
		VarInfo right = visitLogical_or_expression((CParser.Logical_or_expressionContext)ctx.getChild(2), null, null);
		if (error > 0) return left;
		if (op instanceof RuleContext && op instanceof CParser.Additive_operatorContext){
			if (left.type instanceof Function ||
					right.type instanceof Function){
				error ++;	
				//System.out.println("Logical_or_expressionError_1");
				return left;
			}
			if (!(left.type.convertable(new Int()))){
				error ++;	
				//System.out.println("Logical_or_expressionError_2");
				return left;
			}
			VarInfo res = new VarInfo();
			res.type = left.type;
			res.loc = new Mem(res.type, func, true);
			if (left.type instanceof Pointer || left.type instanceof Array){
				VarInfo tmp = new VarInfo();
				tmp.type = new Int();
				tmp.loc = new Mem(tmp.type, func, true);
				if (left.type instanceof Pointer)
					translator.appendMULI(tmp, right, ((Pointer)left.type).elementType.size(), func);
				else 
					translator.appendMULI(tmp, right, ((Array)left.type).elementType.size(), func);
				translator.appendArithmetic(res, left, op.getText(), tmp, null, null, func);
			}else if (right.type instanceof Pointer || right.type instanceof Array){
				VarInfo tmp = new VarInfo();
				tmp.type = new Int();
				tmp.loc = new Mem(tmp.type, func, true);
				if (right.type instanceof Pointer)
					translator.appendMULI(tmp, left, ((Pointer)right.type).elementType.size(), func);
				else 
					translator.appendMULI(tmp, left, ((Array)right.type).elementType.size(), func);
				translator.appendArithmetic(res, right, op.getText(), tmp, trueBranch, falseBranch, func);
			}else translator.appendArithmetic(res, left, op.getText(), right, trueBranch, falseBranch, func);
			return res; 
		}
		if (!(left.type instanceof Char || left.type instanceof Int
				|| left.type instanceof Pointer)){
			error ++; 
			//System.out.println("Logical_or_expressionError_4");
			return left;	
		}
		if (!(right.type instanceof Char || right.type instanceof Int)){
			error ++; 
			//System.out.println("Logical_or_expressionError_5");
			return left;	
		}
		VarInfo res = new VarInfo();
		res.type = left.type;
		res.loc = new Mem(res.type, func, true);
		translator.appendArithmetic(res, left, op.getText(), right, trueBranch, falseBranch, func);
		return res; 
	}

	public Type visitType_name( CParser.Type_nameContext ctx) { 
		Type cur_type = visitType_specifier((CParser.Type_specifierContext)ctx.getChild(0));
		if (error > 0) return cur_type;
		int cnt = ctx.getChildCount();
		for (int i = 0; i < cnt; i ++)
			if (ctx.getChild(i).getText().equals("*"))
				cur_type = new Pointer(cur_type);
		return cur_type;
	}

	public Integer visitAdditive_operator( CParser.Additive_operatorContext ctx) { 
		return 0;
	}

	public Type visitType_specifier( CParser.Type_specifierContext ctx) { 
		int cnt = ctx.getChildCount();
		Type res;
		if (ctx.getChildCount() == 1){ 
			String s = ctx.getChild(0).getText();
			if (s.equals("int")){
				return new Int();
			}
			if (s.equals("void")) return new Void();
			if (s.equals("char")) return new Char();
		}
		else{
			if (cnt == 2){
				res = scope.getStruct(visitIdentifier(ctx.getChild(1).getText()));
				return res;
			}else{
				Type cur;
				if (ctx.getChild(0).getText().equals("struct")){
					Symbol key;
					if(!ctx.getChild(1).getText().equals("{"))
					  key = visitIdentifier(ctx.getChild(1).getText());
					else{
						key = Symbol.symbol("ZhuJiaoDaZhuTou" + emptyTot);
						emptyTot ++;
					}
					if (scope.structDefineError(key)){
						error ++;
						//System.out.println("FML9");
						return null;
					}	
					cur = new Struct(key);
				}else{
					Symbol key;
					if(!ctx.getChild(1).getText().equals("{"))
						key = visitIdentifier(ctx.getChild(1).getText());
					else{
						key = Symbol.symbol("ZhuJiaoDaZhuTou" + emptyTot);
						emptyTot ++;
					}
					if (scope.structDefineError(key)){
						error ++;
						//System.out.println("FML10");
						return null;
					}
					cur = new Union(key);
				}
				Type cur_type = null;
				Vector<SymbolTypePair> v = null;
				for (int i = 1; i < cnt; ++i){
					if (ctx.getChild(i).getText().equals(";")){
						if (v != null){
							for (int j = 0; j < v.size(); j ++){
								if (cur instanceof Struct)
									((Struct)cur).addField(v.get(j));
								else ((Union)cur).addField(v.get(j));
							}
						}
					}
					if (!(ctx.getChild(i) instanceof RuleContext))
						continue;
					RuleContext tmp = (RuleContext)(ctx.getChild(i));
					if (tmp instanceof CParser.Type_specifierContext)
						cur_type = visitType_specifier((CParser.Type_specifierContext)(tmp));
					if (error > 0) return cur;
					if (tmp instanceof CParser.DeclaratorsContext){
						v = visitDeclarators((CParser.DeclaratorsContext)(tmp), cur_type);
					}
					if (error > 0) return cur;
				}
				cur.defined = true;
				if (cur instanceof Struct)
					((Struct)cur).fillField(cur);
				else ((Union)cur).fillField(cur);
				scope.defineStruct(cur);
				return cur;
			}
		}
		return new Type();
	}

	public SymbolTypePair visitPlain_declaration( CParser.Plain_declarationContext ctx) { 
		int cnt = ctx.getChildCount();
		Type cur_type = null;
		SymbolTypePair res = new SymbolTypePair();
		for (int i = 0; i < cnt; i ++){
			RuleContext cur = (RuleContext)ctx.getChild(i);	
			if (cur instanceof CParser.Type_specifierContext){
				cur_type = visitType_specifier((CParser.Type_specifierContext)cur);	
			}
			if (error > 0) return new SymbolTypePair();
			if (cur instanceof CParser.DeclaratorContext){
				res = visitDeclarator((CParser.DeclaratorContext)cur, cur_type);	
				if (error > 0) return res;
				if (!(res.type.isValid())){
					error ++; 
					//System.out.println("Plain_declaratorError_1");
					return res;	
				}
				if (res.type.isDefined() == false){
					error ++; 
					//System.out.println("Plain_declaratorError_2");
					return res;	
				}
			}
		}
		return res;
	}

	public VarInfo visitInitializer( CParser.InitializerContext ctx, Type typ) {
		int cnt = ctx.getChildCount();
		VarInfo res = new VarInfo();
		if (cnt == 1){
			res = visitAssignment_expression((CParser.Assignment_expressionContext)ctx.getChild(0), null, null);	
			if (!res.type.convertable(typ)) {
				error ++;
				//System.out.println("Init_declaratorError_5");
				return res;
			}
			return res;
		}
		error ++;
		//System.out.println("FML11");
		/*else{
			Vector<VarInfo> nxt = null;
			if (!(typ instanceof Pointer || typ instanceof Array)){
				error ++;
				return res;
			}
			if (typ instanceof Pointer)
				typ = (Pointer)typ.elementType;
			else typ = (Array)typ.elementType;
			for (int i = 0; i < cnt; i++){
				RuleContext cur = (RuleContext)ctx.getChild(i);
				if (cur instanceof CParser.InitializerContext){
					nxt = visitInitializer((CParser.InitializerContext)cur);
					if (error > 0) return nxt;
					if (res == null || nxt == null ||
						nxt.get(0).type.convertable(typ)){
						for (int j = 0; j < nxt.size(); j ++)
							res.add(nxt.get(j));
					}else{
						error ++; 
						//System.out.println("InitializerError_1");
						return nxt;	
					}
				}
				return res;
			}
		}*/
		return res; 
	}

	public Symbol visitIdentifier( String x){
		return Symbol.symbol(x);
	}
}
