package compiler.semantic;

public class Pointer extends Type{
	public Type elementType;
	public Pointer(Type _e){
		this.elementType = _e;	
	}
	public Pointer(){
		elementType = null;
	}
	@Override
	public Type clone(){
		Pointer res = new Pointer();
		res.elementType = elementType;
		return res;
	}
	@Override
	public String toString(){
		return "Pointer" + "->"+ elementType.toString();
	}
	@Override
	public boolean isDefined(){
		return true;	
	}
	@Override
	public boolean convertable(Type typ){
		return (typ instanceof Pointer || typ instanceof Int ||
				typ instanceof Char || typ instanceof Array);
	}
	@Override
	public boolean equals(Object obj){
		if (!(obj instanceof Pointer )) return false;	
		return (((Pointer)obj).elementType.equals(elementType));
	}
	@Override
	public boolean isValid(){
		return true;
		//return elementType.isValid();	
	}
};
