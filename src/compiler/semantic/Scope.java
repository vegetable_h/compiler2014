package compiler.semantic;

import java.util.Vector;

class TypePointer{
	Type type;
	TypePointer prev;
	TypePointer(Type typ){
		type = typ;	
		prev = null;
	}
	TypePointer(Type typ, TypePointer p){
		type = typ;	
		prev = p;
	}
};
public class Scope{
	public Scope prev;
	
	private static java.util.Dictionary<Symbol, TypePointer> 
			dict_of_variables = new java.util.Hashtable<Symbol, TypePointer>();
	private Vector<Symbol> cur_scope_record;
	private java.util.Dictionary<Symbol, Type>
			dict_of_cur_var;
	private static java.util.Dictionary<Symbol, TypePointer>
			dict_of_struct = new java.util.Hashtable<Symbol, TypePointer>();
	private Vector<Symbol> cur_scope_struct;
	private static java.util.Dictionary<Symbol, Vector<Type>>  
			dict_of_funcs = new java.util.Hashtable<Symbol, Vector<Type>>();

	public Scope(){
		prev = null;
		cur_scope_struct = new Vector<Symbol>();
		cur_scope_record = new Vector<Symbol>();
		dict_of_cur_var =  new java.util.Hashtable<Symbol, Type>();
	}
	
	public Scope(Scope _p){
		prev = _p;	
		cur_scope_struct = new Vector<Symbol>();
		cur_scope_record = new Vector<Symbol>();
		dict_of_cur_var =  new java.util.Hashtable<Symbol, Type>();
	}
	public Type getType(Symbol var){
		TypePointer e = dict_of_variables.get(var) ;
		if (e == null)	
			return null;
		else return e.type;
	}
	public void endScope(){
		for (int i = cur_scope_record.size() - 1; i >= 0; i --){
			Symbol key = cur_scope_record.get(i);	
			TypePointer e = dict_of_variables.get(key);
			if (e.prev == null){
				dict_of_variables.remove(key);	
			}else dict_of_variables.put(key, e.prev);
		}	
		for (int i = 0; i < cur_scope_struct.size(); i ++){
			Symbol key = cur_scope_struct.get(i);
			TypePointer e = dict_of_struct.get(key);
			Type cur = e.type;
			if (e.prev == null)
				dict_of_struct.remove(key);
			else dict_of_struct.put(key, e.prev);
			cur.defined = false;
		}
	}
	public Type getStruct(Symbol key){
		TypePointer e = dict_of_struct.get(key);
		if (e == null) return null;
		return e.type;
	}
	public boolean structDefineError(Symbol key){	
		return (cur_scope_struct.contains(key)); 
	}
	public void defineStruct(Type typ){
		Symbol key;
		if (typ instanceof Struct) key = ((Struct)typ).name;
			else key = ((Union)typ).name;
		cur_scope_struct.add(key);
		TypePointer e = dict_of_struct.get(key);
		if (e == null) dict_of_struct.put(key, new TypePointer(typ));
			else dict_of_struct.put(key, new TypePointer(typ, e)); 
	}
	public boolean functionDefined(Type typ){
		Type f = dict_of_cur_var.get(((Function)typ).name); 
		if (f != null) return false;
		return typ.isDefined();
	}
	public boolean identifierDeclareError(SymbolTypePair stPair){
		Type e = dict_of_cur_var.get(stPair.symbol);	
		if (e == null) return false;
		if (e instanceof Function){
			Vector<Type> f = dict_of_funcs.get(((Function)stPair.type).name);		
			if (f == null) return false;
			int cnt = f.size();
			for (int i = 0; i < cnt; i ++)
				if (f.get(i) == stPair.type && !((Function)stPair.type).returnType.equals( 
						((Function)f.get(i)).returnType))
					return true;
			return false;
		}else return true;
	}
	public void declare(SymbolTypePair stPair){
		dict_of_cur_var.put(stPair.symbol, stPair.type);
		cur_scope_record.add(stPair.symbol);			
		TypePointer e = dict_of_variables.get(stPair.symbol);
		if (e == null){
			dict_of_variables.put(stPair.symbol, new TypePointer(stPair.type));	
		}else{
			dict_of_variables.put(stPair.symbol, new TypePointer(stPair.type, e));	
		}
		if (stPair.type instanceof Function){
			Vector<Type> f = dict_of_funcs.get(((Function)stPair.type).name);		
			if (f != null && f.contains(stPair.type)) return;
			dict_of_funcs.remove(((Function)stPair.type).name);
			if (f == null) f = new Vector<Type>();
			f.add(stPair.type);
			dict_of_funcs.put(stPair.symbol, f);
		}	
	}
	public void define(Type typ){
		typ.defined = true;	
		Vector<Type> f = dict_of_funcs.get(((Function)typ).name);		
		if (f != null && f.contains(typ)) return ;
		if (f != null) dict_of_funcs.remove(((Function)typ).name);
		f = new Vector<Type>();
		f.add(typ);
		dict_of_funcs.put(((Function)typ).name, f);
		dict_of_cur_var.put(((Function)typ).name, typ);
		cur_scope_record.add(((Function)typ).name);			
		TypePointer e = dict_of_variables.get(((Function)typ).name);
		if (e == null){
			dict_of_variables.put(((Function)typ).name, new TypePointer(typ));	
		}else{
			dict_of_variables.put(((Function)typ).name, new TypePointer(typ, e));	
		}
	}

}
