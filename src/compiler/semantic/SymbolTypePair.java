package compiler.semantic;

public class SymbolTypePair{
	public Symbol symbol;
	public Type type;
	public SymbolTypePair(Symbol s, Type t){
		symbol = s; type = t;
	}
	public SymbolTypePair(){}
}
