package compiler.semantic;
public class Type{
	public boolean isConstant;
	public boolean defined;
	public boolean pointable;
	public boolean lValue;
	public boolean isDefined(){return true;}
	public Type(){
		isConstant = false;
		defined = true;
		pointable = true;
		lValue = true;
	}
	public boolean convertable(Type comp){ 
		if (comp.getClass() != getClass()) return false;
		return true;
	}
	public boolean islValue(){
		if ((!isConstant) && lValue) return true; else return false;
	}
	public boolean isValid(){return true;}
	public Type clone(){
		return new Type();
	}
	public String toString(){String s = "type"; return s;}
	public int size(){return 4;}
	@Override
	public boolean equals(Object obj){
		if (!(obj instanceof Type)) return false;
			else return false;
	}
};
