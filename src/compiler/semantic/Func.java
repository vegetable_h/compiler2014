package compiler.semantic;

import compiler.translate.VarInfo;

public class Func{
	public Func prev;
	public Function cur;
	public Type returnType;
	public VarInfo result;
	public Func(){}
	public Func(Symbol s){
		prev = null;	
		cur = new Function(s); 
	}
	@Override
	public boolean equals(Object ob){
		if (!(ob instanceof Func)) return false;
		return ((Func)ob).cur.name == cur.name;
	}
	@Override 
	public int hashCode(){
		return cur.name.hashCode();
	}
}
