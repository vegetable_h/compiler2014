package compiler.semantic;

public class Void extends Type{
	public Void(){	
	}
	@Override
	public String toString(){
		return "Void";
	}
	public boolean isDefined(){
		return true;
	}
	public boolean convertable(Object obj){
		return (obj instanceof Void);
	}
	@Override
	public Type clone(){
		return new Void();
	}
	@Override
	public boolean equals(Object obj){
		return (obj instanceof Void);
	}
}
