package compiler.semantic;

public class Array extends Type{
	public Type elementType;
	int tot;
	public Array(Type typ, int t){
		elementType = typ;	
		tot = t;
	}
	@Override 
	public int size(){
		return elementType.size() * tot;
	}
	public Array(){
		elementType = null;
	}
	@Override
	public Type clone(){
		Array res = new Array();
		res.elementType =elementType;
		return res;
	}
	@Override
	public String toString(){
		return "Array of " + elementType.toString();
	}
	@Override
	public boolean isDefined(){
		return elementType.isDefined();	
	}
	@Override 
	public boolean equals(Object obj){
		if (!(obj instanceof Array)) return false;
		return (((Array)obj).elementType.equals(elementType));
	}
	@Override
	public boolean islValue(){return false;}
	@Override
	public boolean convertable(Type tmp){
		if (!(tmp instanceof Char || tmp instanceof Int)
				&&
				!(tmp instanceof Pointer || tmp instanceof Array))	
			return false;
		return true;
	}
}
