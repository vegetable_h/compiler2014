#!/bin/bash
set -x
#Note that chinese name of directory may not be supported.
make clean
make all

#prepare outputs directory
mkdir -p outputs && rm outputs/*
dir="testcases/Normal"
echo CCHK = $CCHK
source finaltermvars.sh
for filec in $(ls $dir/*.c); do
	filea=${filec%.c}.ans
	cp $filec bin/data.c
	cd bin
	$CCHK data.c 1>assem.s
	spim -stat_file spimstat -file assem.s 1> spimout
	diff spimout $filea
	num=-1
	num=$(cat spimstat | awk ' { print $1 } ')
	echo NUMBER : $num
	sleep 1
	cd ..
done

