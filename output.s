.text
main:
la $gp, GLOBAL
jal mymain
lw $t0, 12($gp)
lw $t1, 8($gp)
move $t0, $t1
sw $t0, 12($gp)
li $v0, 10
syscall
mymain:
subu $sp, $sp, 11372
sw $ra, 11368($sp)
lw $t0, 4($sp)
li $t0, 10000
sw $t0, 4($sp)
lw $t0, 0($sp)
lw $t1, 4($sp)
move $t0, $t1
sw $t0, 0($sp)
lw $t0, 12($sp)
li $t0, 0
sw $t0, 12($sp)
lw $t0, 8($sp)
lw $t1, 12($sp)
move $t0, $t1
sw $t0, 8($sp)
lw $t0, 20($sp)
li $t0, 2800
sw $t0, 20($sp)
lw $t0, 16($sp)
lw $t1, 20($sp)
move $t0, $t1
sw $t0, 16($sp)
lw $t0, 28($sp)
li $t0, 0
sw $t0, 28($sp)
lw $t0, 24($sp)
lw $t1, 28($sp)
move $t0, $t1
sw $t0, 24($sp)
lw $t0, 36($sp)
li $t0, 0
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
move $t0, $t1
sw $t0, 32($sp)
lw $t0, 11248($sp)
li $t0, 0
sw $t0, 11248($sp)
lw $t0, 11244($sp)
lw $t1, 11248($sp)
move $t0, $t1
sw $t0, 11244($sp)
L2:
lw $t0, 11252($sp)
lw $t1, 8($sp)
lw $t2, 16($sp)
sub  $t0, $t1, $t2
sw $t0, 11252($sp)
lw $t0, 11252($sp)
beqz  $t0,L4
lw $t0, 11256($sp)
la $t0, 40($sp)
sw $t0, 11256($sp)
lw $t0, 11260($sp)
lw $t1, 8($sp)
move $t0, $t1
sw $t0, 11260($sp)
lw $t0, 8($sp)
lw $t1, 8($sp)
addiu  $t0, $t1, 1
sw $t0, 8($sp)
lw $t0, 11268($sp)
lw $t1, 11260($sp)
mul  $t0, $t1, 4
sw $t0, 11268($sp)
lw $t0, 11268($sp)
lw $t1, 11268($sp)
lw $t2, 11256($sp)
add  $t0, $t1, $t2
sw $t0, 11268($sp)
lw $t0, 11272($sp)
li $t0, 5
sw $t0, 11272($sp)
lw $t0, 11276($sp)
lw $t1, 0($sp)
lw $t2, 11272($sp)
divu  $t0, $t1, $t2
sw $t0, 11276($sp)
lw $t6, 11268($sp)
lw $t0, 0($t6)
lw $t1, 11276($sp)
move $t0, $t1
lw $t3, 11268($sp)
sw $t0, 0($t3)
L3:
j L2
L4:
L5:
lw $t0, 11280($sp)
li $t0, 0
sw $t0, 11280($sp)
lw $t0, 24($sp)
lw $t1, 11280($sp)
move $t0, $t1
sw $t0, 24($sp)
lw $t0, 11284($sp)
li $t0, 2
sw $t0, 11284($sp)
lw $t0, 11288($sp)
lw $t1, 16($sp)
lw $t2, 11284($sp)
mul  $t0, $t1, $t2
sw $t0, 11288($sp)
lw $t0, 11244($sp)
lw $t1, 11288($sp)
move $t0, $t1
sw $t0, 11244($sp)
lw $t0, 11288($sp)
beqz  $t0,L10
lw $t0, 8($sp)
lw $t1, 16($sp)
move $t0, $t1
sw $t0, 8($sp)
L6:
lw $t0, 11292($sp)
la $t0, 40($sp)
sw $t0, 11292($sp)
lw $t0, 11300($sp)
lw $t1, 8($sp)
mul  $t0, $t1, 4
sw $t0, 11300($sp)
lw $t0, 11300($sp)
lw $t1, 11300($sp)
lw $t2, 11292($sp)
add  $t0, $t1, $t2
sw $t0, 11300($sp)
lw $t0, 11304($sp)
lw $t6, 11300($sp)
lw $t1, 0($t6)
lw $t2, 0($sp)
mul  $t0, $t1, $t2
sw $t0, 11304($sp)
lw $t0, 24($sp)
lw $t1, 24($sp)
lw $t2, 11304($sp)
add  $t0, $t1, $t2
sw $t0, 24($sp)
lw $t0, 11308($sp)
la $t0, 40($sp)
sw $t0, 11308($sp)
lw $t0, 11316($sp)
lw $t1, 8($sp)
mul  $t0, $t1, 4
sw $t0, 11316($sp)
lw $t0, 11316($sp)
lw $t1, 11316($sp)
lw $t2, 11308($sp)
add  $t0, $t1, $t2
sw $t0, 11316($sp)
lw $t0, 11244($sp)
lw $t1, 11244($sp)
subu  $t0, $t1, 1
sw $t0, 11244($sp)
lw $t0, 11320($sp)
lw $t1, 24($sp)
lw $t2, 11244($sp)
rem  $t0, $t1, $t2
sw $t0, 11320($sp)
lw $t6, 11316($sp)
lw $t0, 0($t6)
lw $t1, 11320($sp)
move $t0, $t1
lw $t3, 11316($sp)
sw $t0, 0($t3)
lw $t0, 11324($sp)
lw $t1, 11244($sp)
move $t0, $t1
sw $t0, 11324($sp)
lw $t0, 11244($sp)
lw $t1, 11244($sp)
subu  $t0, $t1, 1
sw $t0, 11244($sp)
lw $t0, 24($sp)
lw $t1, 24($sp)
lw $t2, 11324($sp)
divu  $t0, $t1, $t2
sw $t0, 24($sp)
lw $t0, 8($sp)
lw $t1, 8($sp)
subu  $t0, $t1, 1
sw $t0, 8($sp)
lw $t0, 8($sp)
beqz  $t0,L8
L7:
lw $t0, 24($sp)
lw $t1, 24($sp)
lw $t2, 8($sp)
mul  $t0, $t1, $t2
sw $t0, 24($sp)
j L6
L8:
L9:
lw $t0, 11328($sp)
li $t0, 14
sw $t0, 11328($sp)
lw $t0, 16($sp)
lw $t1, 16($sp)
lw $t2, 11328($sp)
sub  $t0, $t1, $t2
sw $t0, 16($sp)
lw $t0, 11332($sp)
la $t0, Data0
sw $t0, 11332($sp)
lw $t0, 11336($sp)
lw $t1, 24($sp)
lw $t2, 0($sp)
divu  $t0, $t1, $t2
sw $t0, 11336($sp)
lw $t0, 11340($sp)
lw $t1, 32($sp)
lw $t2, 11336($sp)
add  $t0, $t1, $t2
sw $t0, 11340($sp)
la $a0, Data0
lw $t1, 11340($sp)
move $a1, $t1
jal printf
lw $t0, 11348($sp)
lw $t1, 24($sp)
lw $t2, 0($sp)
rem  $t0, $t1, $t2
sw $t0, 11348($sp)
lw $t0, 32($sp)
lw $t1, 11348($sp)
move $t0, $t1
sw $t0, 32($sp)
j L5
L10:
lw $t0, 11352($sp)
la $t0, Data1
sw $t0, 11352($sp)
la $a0, Data1
jal printf
lw $t0, 11360($sp)
li $t0, 0
sw $t0, 11360($sp)
lw $t0, 8($gp)
lw $t1, 11360($sp)
move $t0, $t1
sw $t0, 8($gp)
lw $ra, 11368($sp)
addu $sp, $sp, 11372
jr $ra
lw $t0, 8($gp)
lw $t1, 11364($sp)
move $t0, $t1
sw $t0, 8($gp)
lw $ra, 11368($sp)
addu $sp, $sp, 11372
jr $ra

malloc: 
li $v0, 9
syscall
jr $ra

printf:
sw $ra, -4($sp)
sw $s3, -8($sp)
sw $s4, -12($sp)
sw $s5, -16($sp)
sw $s6, -20($sp)
sw $t0, -24($sp)
sw $t1, -28($sp)
move $t0, $v0
move $t1, $v1
move $s4, $a0 
la $s6, printf_buf 

printf_loop: 
lb $s5, 0($s4) 
addu $s4, $s4, 1
beq $s5, '%', printf_fmt
beq $0, $s5, printf_end
sb $s5, 0($s6) 
sb $0, 1($s6) 
move $a0, $s6
li $v0, 4 
syscall
b printf_loop 

printf_fmt:
lb $s5, 0($s4)
addu $s4, $s4, 1
beq $s5, 'd', printf_int
beq $s5, 's', printf_str
beq $s5, 'c', printf_char
beq $s5, '0', printf_width

printf_shift_args: 
move $a1, $a2 
move $a2, $a3 
move $a3, $t0 
move $t0, $t1
b printf_loop 

printf_int: 
move $a0, $a1 
li $v0, 1
syscall
b printf_shift_args 

printf_str: 
move $a0, $a1 
li $v0, 4
syscall
b printf_shift_args 

printf_char: 
sb $a1, 0($s6) 
sb $0, 1($s6) 
move $a0, $s6 
li $v0, 4 
syscall
b printf_shift_args 

printf_width:
move $s3, $a1
lb $s5, 0($s4)
addu $s4, $s4, 2
subu $s5, $s5, '1'
divu $a1, $a1, 10

calc_zero_number:
beqz $a1 print_zeros
subu $s5, $s5, 1
divu $a1, $a1, 10
b calc_zero_number

print_zeros:
print_single_zero:
beqz $s5, print_number
subu $s5, $s5, 1
li $a0, 0
li $v0, 1
syscall
b print_single_zero

print_number:
move $a0, $s3
li $v0, 1
syscall
b printf_shift_args

printf_end:
lw $ra, -4($sp)
lw $s3, -8($sp)
lw $s4, -12($sp)
lw $s5, -16($sp)
lw $s6, -20($sp)
lw $t0, -24($sp)
lw $t1, -28($sp)
jr $ra
.data 0x10000000
printf_buf: .space 2
Data0: 
.asciiz "%04d"
Data1: 
.asciiz "\n"
.align 4
GLOBAL:
