.text
lw $v0, 12($gp)
li $v0, 0
move $v0, $v0
subi $sp 20
lw $v1, 0($sp)
move $v1, $v0
lw $a0, 12($sp)
li $a0, 1
lw $a1, 16($sp)
lw $a2, 8($sp)
add  $a1, $a2, $a0
lw $a3, 4($sp)
move $a3, $a1
addi $sp 20
goto $ra
subi $sp 56
lw $t0, 0($sp)
move $t0, $v0
lw $t1, 8($sp)
li $t1, 0
lw $t2, 4($sp)
move $t2, $t1
lw $t3, 16($sp)
li $t3, 1
lw $t4, 12($sp)
move $t4, $t3
L1:
lw $t5, 20($sp)
la $t5, 4($sp)
move $t5, $v0
jal addSmall
lw $t6, 24($sp)
move $t6, $v0
li $t6, 1
lw $t7, 28($sp)
add  $t7, $t2, $t6
move $t2, $t7
L2:
lw $s0, 32($sp)
move $s0, $t4
addi  $t4, $t4, 1
lw $s1, 36($sp)
li $s1, 10
lw $s2, 40($sp)
sgt  $s2, $s1, $t4
not  $s2, $s2
bnez  $s2,L3
j L1
L3:
lw $s3, 52($sp)
lw $s4, 48($sp)
add  $s3, $s4, $t2
lw $s5, 44($sp)
move $s5, $s3
addi $sp 56
goto $ra
subi $sp 64
lw $s6, 8($sp)
la $s6, 8($gp)
lw $s7, 4($sp)
move $s7, $s6
lw $t8, 12($sp)
li $t8, 1
lw $t9, 0($sp)
move $t9, $t8
L4:
sw $v0, 20($sp)
lw $v0, 20($sp)
li $v0, 0
sw $v1, 16($sp)
lw $v1, 16($sp)
move $v1, $v0
sw $a0, 24($sp)
lw $a0, 24($sp)
la $a0, 16($sp)
sw $a1, 20($gp)
lw $a1, 20($gp)
move $a0, $a1
jal addMiddle
sw $a2, 28($sp)
lw $a2, 28($sp)
move $a2, $a1
sw $a3, 36($sp)
lw $a3, 36($sp)
sw $t0, 32($sp)
lw $t0, 32($sp)
add  $a3, $t0, $v1
sw $t1, 28($sp)
lw $t1, 28($sp)
move $t1, $a3
sw $t2, 40($sp)
lw $t2, 40($sp)
li $t2, 1
sw $t3, 44($sp)
lw $t3, 44($sp)
add  $t3, $a1, $t2
move $a1, $t3
sw $a0, 24($sp)
lw $a0, Data0
sw $a1, 20($gp)
lw $a1, $t9
sw $a2, 28($sp)
lw $a2, $a1
jal printf
L5:
sw $t4, 48($sp)
lw $t4, 48($sp)
move $t4, $t9
addi  $t9, $t9, 1
sw $t5, 52($sp)
lw $t5, 52($sp)
li $t5, 10
sw $t6, 56($sp)
lw $t6, 56($sp)
sgt  $t6, $t5, $t9
not  $t6, $t6
bnez  $t6,L6
j L4
L6:
sw $t7, 60($sp)
lw $t7, 60($sp)
move $a1, $t7
addi $sp 64
goto $ra

malloc: 
li $v0, 9
sw $v0, -4($sp)
syscall
lw $v0, -4($sp)
jr $ra

printf:
sw $ra, -4($sp)
sw $s3, -8($sp)
sw $s4, -12($sp)
sw $s5, -16($sp)
sw $s6, -20($sp)
sw $t0, -24($sp)
sw $t1, -28($sp)
move $t0, $v0
move $t1, $v1
move $s4, $a0 
la $s6, printf_buf 

printf_loop: 
lb $s5, 0($s4) 
addu $s4, $s4, 1
beq $s5, '%', printf_fmt
beq $0, $s5, printf_end
sb $s5, 0($s6) 
sb $0, 1($s6) 
move $a0, $s6
li $v0, 4 
syscall
b printf_loop 

printf_fmt:
lb $s5, 0($s4)
addu $s4, $s4, 1
beq $s5, 'd', printf_int
beq $s5, 's', printf_str
beq $s5, 'c', printf_char
beq $s5, '0', printf_width

printf_shift_args: 
move $a1, $a2 
move $a2, $a3 
move $a3, $t0 
move $t0, $t1
b printf_loop 

printf_int: 
move $a0, $a1 
li $v0, 1
syscall
b printf_shift_args 

printf_str: 
move $a0, $a1 
li $v0, 4
syscall
b printf_shift_args 

printf_char: 
sb $a1, 0($s6) 
sb $0, 1($s6) 
move $a0, $s6 
li $v0, 4 
syscall
b printf_shift_args 

printf_width:
move $s3, $a1
lb $s5, 0($s4)
addu $s4, $s4, 2
subu $s5, $s5, '1'
divu $a1, $a1, 10

calc_zero_number:
beqz $a1 print_zeros
subu $s5, $s5, 1
divu $a1, $a1, 10
b calc_zero_number

print_zeros:
print_single_zero:
beqz $s5, print_number
subu $s5, $s5, 1
li $a0, 0
li $v0, 1
syscall
b print_single_zero

print_number:
move $a0, $s3
li $v0, 1
syscall
b printf_shift_args

printf_end:
lw $ra, -4($sp)
lw $s3, -8($sp)
lw $s4, -12($sp)
lw $s5, -16($sp)
lw $s6, -20($sp)
lw $t0, -24($sp)
lw $t1, -28($sp)
jr $ra
.data
printf_buf: .space 2
Data0: 
.asciiz "%d %d\n"
