
malloc: 
li $v0, 9
syscall
jr $ra

printf:
sw $ra, -4($sp)
sw $s3, -8($sp)
sw $s4, -12($sp)
sw $s5, -16($sp)
sw $s6, -20($sp)
sw $t0, -24($sp)
sw $t1, -28($sp)
move $t0, $v0
move $t1, $v1
move $s4, $a0 
la $s6, printf_buf 

printf_loop: 
lb $s5, 0($s4) 
addu $s4, $s4, 1
beq $s5, '%', printf_fmt
beq $0, $s5, printf_end
sb $s5, 0($s6) 
sb $0, 1($s6) 
move $a0, $s6
li $v0, 4 
syscall
b printf_loop 

printf_fmt:
lb $s5, 0($s4)
addu $s4, $s4, 1
beq $s5, 'd', printf_int
beq $s5, 's', printf_str
beq $s5, 'c', printf_char
beq $s5, '0', printf_width

printf_shift_args: 
move $a1, $a2 
move $a2, $a3 
move $a3, $t0 
move $t0, $t1
b printf_loop 

printf_int: 
move $a0, $a1 
li $v0, 1
syscall
b printf_shift_args 

printf_str: 
move $a0, $a1 
li $v0, 4
syscall
b printf_shift_args 

printf_char: 
sb $a1, 0($s6) 
sb $0, 1($s6) 
move $a0, $s6 
li $v0, 4 
syscall
b printf_shift_args 

printf_width:
move $s3, $a1
lb $s5, 0($s4)
addu $s4, $s4, 2
subu $s5, $s5, '1'
divu $a1, $a1, 10

calc_zero_number:
beqz $a1 print_zeros
subu $s5, $s5, 1
divu $a1, $a1, 10
b calc_zero_number

print_zeros:
print_single_zero:
beqz $s5, print_number
subu $s5, $s5, 1
li $a0, 0
li $v0, 1
syscall
b print_single_zero

print_number:
move $a0, $s3
li $v0, 1
syscall
b printf_shift_args

printf_end:
lw $ra, -4($sp)
lw $s3, -8($sp)
lw $s4, -12($sp)
lw $s5, -16($sp)
lw $s6, -20($sp)
lw $t0, -24($sp)
lw $t1, -28($sp)
jr $ra
