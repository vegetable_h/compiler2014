#!/bin/bash

echo 'please modify the $names variable to be your own id'
echo 'please make sure build-answers.sh is run in the normaldata directory'
names="vegetable_h" #modify to be your own id
#names=$(cat names)
echo $names
sleep 2
timeo=10s


function _SC() #safe call
{
	echo "[RUNNING] : $*"
	eval $*
	if [ $? -ne 0 ]; then
		echo "[ERROR] : command $* failed, please check, exiting..."
		exit
	fi
}

outdir=$(pwd)

mkdir -p students

if [ ! -d compiler2014-testcases ]; then
	_SC git clone git@bitbucket.org:acmcompiler/compiler2014-testcases.git
else
	_SC cd compiler2014-testcases
	_SC git pull
	_SC cd ..
fi

normaldir=$(pwd)/compiler2014-testcases/Normal

for name in ${names[@]}; do
	cd $outdir
	score=0
	echo now testing ${name}...
	_SC cd students
	if [ ! -d $name ]; then
		_SC mkdir $name "&&" cd $name
		_SC git clone "git@bitbucket.org:${name}/compiler2014.git"
		_SC cd ..
	fi
	_SC cd $name/compiler2014
	_SC git checkout -f master
	_SC git pull 
	_SC git fetch --tags
	_SC git checkout -f finalterm #pay attention to your tag! someone don't have the midterm tag
	_SC make clean #some one don't have make clean
	_SC make
	_SC cat ./finaltermvars.sh

	unset -v CCHK
	set -x
	source ./finaltermvars.sh #can't ensure SC here 
	set +x
	echo CCHK=$CCHK

	wrong=0
	_SC cd bin 
	for filec in $(ls $normaldir/*.c); do
		filea=${filec%.c}.ans
		cp $filec data.c
		timeout $timeo $CCHK data.c 1>assem.s
		spim -stat_file spimstat -file assem.s 1>spimout
		diff spimout $filea
		num=-1
		num=$(cat spimstat | awk ' { print $1 } ')
		echo NUMBER : $num
		sleep 1
	done
	_SC cd ..
	_SC git checkout -f master
	cd $outdir
done
